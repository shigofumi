#define _XOPEN_SOURCE 600
#include <stdlib.h>
#include <stdio.h>
#include <isds.h>
#include <string.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <locale.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <confuse.h>
#include <errno.h>
#include <libgen.h>
#include <limits.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <strings.h>
#include <time.h>
#include <magic.h>
#include <libxml/parser.h> /* For xmlParserVersion() */

#include "shigofumi.h"
#include "completion.h"
#include "data.h"
#include "io.h"
#include "ui.h"
#include "utils.h"

#define CONFIG_FILE ".shigofumirc"
#define CONFIG_SERVER "base_url"
#define CONFIG_USERNAME "username"
#define CONFIG_PASSWORD "password"
#define CONFIG_CERT_FORMAT "certificate_format"
#define CONFIG_CERT_PATH "certificate_path"
#define CONFIG_KEY_ENGINE "key_engine"
#define CONFIG_KEY_FORMAT "key_format"
#define CONFIG_KEY_PATH "key_path"
#define CONFIG_KEY_PASSWORD "key_password"
#define CONFIG_OTP_METHOD "otp_method"
#define CONFIG_OTP_CODE "otp_code"
#define CONFIG_DEBUGLEVEL "debug_level"
#define CONFIG_VERIFYSERVER "verify_server"
#define CONFIG_CAFILE "ca_file"
#define CONFIG_CADIRECTORY "ca_directory"
#define CONFIG_CLEAN_TEMPORARY_FILES "clean_temporary_files"
#define CONFIG_CONFIRM_SEND "confirm_send"
#define CONFIG_CRLFILE "crl_file"
#define CONFIG_TIMEOUT "timeout"
#define CONFIG_LOGFACILITIES "log_facilities"
#define CONFIG_LOGFILE "log_file"
#define CONFIG_LOGLEVEL "log_level"
#define CONFIG_MARKMESSAGEREAD "mark_message_read"
#define CONFIG_NORMALIZEMIMETYPE "normalize_mime_type"
#define CONFIG_OPENCOMMAND "open_command"
#define CONFIG_OVERWRITEFILES "overwrite_files"
#define CZPDEPOSIT_URL "https://www.czechpoint.cz/uschovna/"

#define TIMEOUT 10000
#define LOG_LEVEL 20

/* Configuration */
cfg_opt_t configuration_syntax[] = {
    CFG_STR(CONFIG_SERVER, NULL, CFGF_NONE),
    CFG_STR(CONFIG_USERNAME, NULL, CFGF_NODEFAULT),
    CFG_STR(CONFIG_PASSWORD, NULL, CFGF_NODEFAULT),
    CFG_STR(CONFIG_CERT_FORMAT, NULL, CFGF_NODEFAULT),
    CFG_STR(CONFIG_CERT_PATH, NULL, CFGF_NODEFAULT),
    CFG_STR(CONFIG_KEY_ENGINE, NULL, CFGF_NODEFAULT),
    CFG_STR(CONFIG_KEY_FORMAT, NULL, CFGF_NODEFAULT),
    CFG_STR(CONFIG_KEY_PATH, NULL, CFGF_NODEFAULT),
    CFG_STR(CONFIG_KEY_PASSWORD, NULL, CFGF_NODEFAULT),
    CFG_STR(CONFIG_OTP_METHOD, NULL, CFGF_NODEFAULT),
    CFG_STR(CONFIG_OTP_CODE, NULL, CFGF_NODEFAULT),
    /*CFG_STR(CONFIG_DEBUGLEVEL, NULL, CFGF_NODEFAULT),*/
    CFG_BOOL(CONFIG_VERIFYSERVER, cfg_true, CFGF_NONE),
    CFG_STR(CONFIG_CAFILE, NULL, CFGF_NODEFAULT),
    CFG_STR(CONFIG_CADIRECTORY, NULL, CFGF_NODEFAULT),
    CFG_BOOL(CONFIG_CLEAN_TEMPORARY_FILES, cfg_true, CFGF_NONE),
    CFG_STR(CONFIG_CRLFILE, NULL, CFGF_NODEFAULT),
    CFG_INT(CONFIG_TIMEOUT, TIMEOUT, CFGF_NONE),
    CFG_STR_LIST(CONFIG_LOGFACILITIES, "{none}", CFGF_NONE),
    CFG_STR(CONFIG_LOGFILE, NULL, CFGF_NODEFAULT),
    CFG_INT(CONFIG_LOGLEVEL, LOG_LEVEL, CFGF_NONE),
    CFG_BOOL(CONFIG_CONFIRM_SEND, cfg_true, CFGF_NONE),
    CFG_BOOL(CONFIG_MARKMESSAGEREAD, cfg_false, CFGF_NONE),
    CFG_BOOL(CONFIG_NORMALIZEMIMETYPE, cfg_true, CFGF_NONE),
    CFG_STR_LIST(CONFIG_OPENCOMMAND, "{xdg-open, %f}", CFGF_NONE),
    CFG_BOOL(CONFIG_OVERWRITEFILES, cfg_true, CFGF_NONE),
    CFG_END()
};
cfg_t *configuration;

/* Logger */
int logger_fd = -1;

/* UI */
_Bool batch_mode = 0;
char *prompt = NULL;
struct command (*commands)[] = NULL;
FILE *output = NULL;

/* Data */
struct isds_ctx *cisds = NULL;
struct isds_list *boxes = NULL;
struct isds_message *message = NULL;
_Bool messages_are_outgoing = 0;
struct isds_list *messages = NULL;
unsigned long int total_messages = 0;
struct isds_ctx *czechpoint = NULL;
struct isds_list *temporary_files = NULL;

/* Temporary log-in settings */
char *server = NULL;
char *username = NULL;
char *password = NULL;
char *key_password = NULL;
char *otp_method = NULL;
char *otp_code = NULL;
char *pki_engine = NULL;
char *pki_certificate_path = NULL;
char *pki_certificate_format = NULL;
char *pki_key_path = NULL;
char *pki_key_format = NULL;

static void discard_credentials(void) {
    zfree(server);
    zfree(username);
    zfree(password);
    zfree(key_password);
    zfree(otp_method);
    zfree(otp_code);
    zfree(pki_engine);
    zfree(pki_certificate_path);
    zfree(pki_certificate_format);
    zfree(pki_key_path);
    zfree(pki_key_format);
}


/* Remove temporary file */
void shi_unlink_temporary_file(void **data) {
    if (!data || !*data) return;

    const char *file = (const char *)*data;
    unlink_file(file);
    zfree(*data);
}


/* Finish ISDS operation, report error, if the operation returned different
 * code than @positive_code. */
static void finish_isds_operation_with_code(struct isds_ctx *ctx,
        isds_error err, isds_error positive_code)  {
    shi_progressbar_finish();
    if (err != positive_code) {
        if (isds_long_message(ctx)) 
            fprintf(stderr, _("Error occurred: %s: %s\n"), isds_strerror(err),
                    isds_long_message(ctx));
        else
            fprintf(stderr, _("Error occurred: %s\n"), isds_strerror(err));
    }
}


/* Finish ISDS operation, report error, if the operation did not returned
 * IE_SUCCESS. */
static void finish_isds_operation(struct isds_ctx *ctx, isds_error err)  {
    finish_isds_operation_with_code(ctx, err, IE_SUCCESS);
}


/* Do the cleanup and exit */
static void shi_exit(int exit_code) {
    /* Data */
    discard_credentials();
    isds_list_free(&boxes);
    isds_message_free(&message);
    isds_list_free(&messages);
    if (temporary_files) {
        oprintf(_("Removing temporary files...\n"));
        isds_list_free(&temporary_files);
    }

    if (cisds) {
        isds_error err;
        oprintf(_("Logging out...\n"));
        err = isds_logout(cisds);
        finish_isds_operation(cisds, err);
        if (err) exit_code = EXIT_FAILURE;
        isds_ctx_free(&cisds);
    }
    isds_ctx_free(&czechpoint);
    isds_cleanup();
    
    /* Configuration */
    cfg_free(configuration);

    /* UI */
    free(prompt);
    free(commands);

    exit(exit_code);
}

/* Set prompt. if @format is NULL, switch to default prompt */
static void set_prompt(const char *format, ...) {
    char *buffer = NULL;
    va_list ap;
    if (format) {
        va_start(ap, format);
        shi_vasprintf(&buffer, format, ap);
        va_end(ap);

        if (buffer) {
            shi_asprintf(&prompt, _("%s> "), buffer);
            if (prompt) {
                free(buffer);
                return;
            }
        }

        free(buffer);
        free(prompt);
        prompt = strdup(_("> "));
        return;
    }

    zfree(prompt);
}


/* Convert name of PKI format into ISDS format type.
 * Return -1 in case of invalid name */
static int string2pki_format(const char *name, isds_pki_format *format) {
   if (!name || !format) { return -1; } 
   if (!strcasecmp(name, "PEM")) {
       *format = PKI_FORMAT_PEM;
   } else if (!strcasecmp(name, "DER")) {
       *format = PKI_FORMAT_DER;
   } else if (!strcasecmp(name, "ENG")) {
       *format = PKI_FORMAT_ENG;
   } else {
       return -1;
   }
   return 0;
}


/* Check for configutation option CONFIG_CERT_FORMAT.
 * Return 0 in case of valid or undefined value. */
static int shi_validate_certificate_format(cfg_t *configuration, cfg_opt_t *option) {
    isds_pki_format format;
    const char *value = cfg_opt_getnstr(option, 0);

    if (NULL != value && string2pki_format(value, &format)) {
        cfg_error(configuration, _("Invalid certificate format: %s"), value);
        return -1;
    }

    return 0;
}

/* Check for configutation option CONFIG_KEY_FORMAT.
 * Return 0 in case of valid or undefined value. */
static int shi_validate_key_format(cfg_t *configuration, cfg_opt_t *option) {
    isds_pki_format format;
    const char *value = cfg_opt_getnstr(option, 0);

    if (NULL != value && string2pki_format(value, &format)) {
        cfg_error(configuration, _("Invalid private key format: %s"), value);
        return -1;
    }

    return 0;
}

static int shi_load_configuration(const char *config_file) {
    char *config_name = NULL;
    int ret;
    int error = 0;

    /* Get config file */
    if (config_file) {
        config_name = (char *) config_file;
    } else {
        if (-1 == shi_asprintf(&config_name, "%s/%s", getenv("HOME"),
                    CONFIG_FILE)) {
            fprintf(stderr, _("Could not build configuration file name\n"));
            return -1;
        }
    }
    
    /* Parse configuration */
    configuration = cfg_init(configuration_syntax, CFGF_NONE);
    cfg_set_validate_func(configuration,
            CONFIG_CERT_FORMAT, shi_validate_certificate_format);
    cfg_set_validate_func(configuration,
            CONFIG_KEY_FORMAT, shi_validate_key_format);

    ret = cfg_parse(configuration, config_name);
    if (ret) {
        error = -1;
        if (ret == CFG_FILE_ERROR) {
            fprintf(stderr,
                    _("Error while opening configuration file `%s': %s\n"),
                    config_name, strerror(errno));
            if (NULL == config_file) {
                /* Consider missing default configuration file as non-fatal. */
                oprintf(_("Using default configuration\n"));
                error = 0;
            }
        } else {
            fprintf(stderr, _("Error while parsing configuration file `%s'\n"),
                    config_name);
        }
    }

    if (config_name != config_file) free(config_name);
    return error;
}


void logger(isds_log_facility facility, isds_log_level level,
        const char *message, int length, void *data) {
    int fd;
    ssize_t written, left = length;

    if (!data) return;
    fd = *((int *) data);
    /*printf("\033[32mLOG(%02d,%02d): ", facility, level);
    printf("%.*s", length, message);
    printf("\033[m");*/

    while (left) {
        written = write(fd, message + length - left, left);
        if (written == -1) {
            fprintf(stderr,
                    _("Could not save log message into log file: %s\n"
                        "Log message discarded!\n"),
                    strerror(errno));
            /*close(fd);
            fd = -1;*/
            return;
        }
        left-=written;
    }
}


/* Redirect ISDS log to file if @file is not NULL. */
static int do_log_to_file(const char *file) {
    if (file && *file) {
        logger_fd = open_file_for_writing(file, 0, 1);
        if (logger_fd == -1) {
            fprintf(stderr, _("Could not redirect ISDS log to file `%s'\n"),
                    file);
            return -1;
        }
        isds_set_log_callback(logger, &logger_fd);
    }
    return 0;
}


/* Add log facility based on its name. */
static int add_log_facility(isds_log_facility *facilities, const char *name) {
    if (!facilities) return -1;

    if (!strcmp(name, "none")) *facilities |= ILF_NONE;
    else if (!strcmp(name, "http")) *facilities |= ILF_HTTP;
    else if (!strcmp(name, "soap")) *facilities |= ILF_SOAP;
    else if (!strcmp(name, "isds")) *facilities |= ILF_ISDS;
    else if (!strcmp(name, "file")) *facilities |= ILF_FILE;
    else if (!strcmp(name, "sec")) *facilities |= ILF_SEC;
    else if (!strcmp(name, "xml")) *facilities |= ILF_XML;
    else if (!strcmp(name, "all")) *facilities |= ILF_ALL;
    else {
        fprintf(stderr, _("%s: Unknown log facility\n"), name);
        return -1;
    }

    return 0;
}


/* Save log facility into confuse configuration */
static void save_log_facility(int level) {
    cfg_setlist(configuration, CONFIG_LOGFACILITIES, 0);

    if (level == ILF_ALL) {
        cfg_addlist(configuration, CONFIG_LOGFACILITIES, 1, "all");
        return;
    }
    if (level == ILF_NONE) {
        cfg_addlist(configuration, CONFIG_LOGFACILITIES, 1, "none");
        return;
    }
    if (level & ILF_HTTP)
        cfg_addlist(configuration, CONFIG_LOGFACILITIES, 1, "http");
    if (level & ILF_SOAP)
        cfg_addlist(configuration, CONFIG_LOGFACILITIES, 1, "soap");
    if (level & ILF_ISDS)
        cfg_addlist(configuration, CONFIG_LOGFACILITIES, 1, "isds");
    if (level & ILF_FILE)
        cfg_addlist(configuration, CONFIG_LOGFACILITIES, 1, "file");
    if (level & ILF_SEC)
        cfg_addlist(configuration, CONFIG_LOGFACILITIES, 1, "sec");
    if (level & ILF_XML)
        cfg_addlist(configuration, CONFIG_LOGFACILITIES, 1, "xml");
}


/* Clamp long int to unsigned int */
static unsigned int normalize_timeout(long int raw) {
    if (raw < 0) {
        oprintf(_("Configured network timeout is less then 0. "
                    "Clamped to 0.\n"));
        return 0;
    }
    if (raw > UINT_MAX ) {
        oprintf(_("Configured network timeout is greater then %1$u. "
                    "Clamped to %1$u.\n"), UINT_MAX);
        return UINT_MAX;
    }
    return (unsigned int) raw;
}


/* Clamp long int to <0;100> */
static unsigned int normalize_log_level(long int raw) {
    if (raw < 0) {
        oprintf(_("Configured log level is less then 0. Clamped to 0.\n"));
        return 0;
    }
    if (raw > ILL_ALL) {
        oprintf(_("Configured log level is greater then %1$u. "
                    "Clamped to %1$u.\n"), ILL_ALL);
        return ILL_ALL;
    }
    if (raw > UINT_MAX ) {
        oprintf(_("Configured log level is greater then %1$u. "
                    "Clamped to %1$u.\n"), UINT_MAX);
        return UINT_MAX;
    }
    return (unsigned int) raw;
}


static int shi_init(const char *config_file) {
    isds_error err;
    char *value;
    unsigned int timeout, log_level;
    isds_log_facility log_facility = ILF_NONE;

    oprintf(_("This is Shigofumi, an ISDS client. "
                "Have a nice e-government.\n"));

    /* Do not permute arguments in getopt() */
    if (setenv("POSIXLY_CORRECT", "", 1)) {
        fprintf(stderr,
                _("Could not set POSIXLY_CORRECT environment variable\n"));
        return -1;
    }

    /* Load configuration */
    if (shi_load_configuration(config_file))
        return -1;
    timeout = normalize_timeout(cfg_getint(configuration, CONFIG_TIMEOUT));
    log_level = normalize_log_level(cfg_getint(configuration, CONFIG_LOGLEVEL));

    /* Init readline */
    rl_readline_name = "shigofumi";
    rl_filename_quote_characters = "\\ >";
    rl_filename_quoting_function = shi_quote_filename;
    rl_filename_dequoting_function = shi_dequote_filename;
    rl_char_is_quoted_p = shi_char_is_quoted;

    /* Initialize ISDS */ 
    err = isds_init();
    if (err) {
        fprintf(stderr, _("Could not initialize libisds library: %s\n"),
                isds_strerror(err));
        return -1;
    }

    /* Set ISDS logging */
    value = cfg_getstr(configuration, CONFIG_LOGFILE);
    if (do_log_to_file(value))
        return -1;
    for (int i = 0; i < cfg_size(configuration, CONFIG_LOGFACILITIES); i++) {
        if (add_log_facility(&log_facility,
                cfg_getnstr(configuration, CONFIG_LOGFACILITIES, i)))
            return -1;

    }
    isds_set_logging(log_facility, log_level);

    /* Set ISDS context up */
    cisds = isds_ctx_create();
    if (!cisds) {
        fprintf(stderr, _("Could not create ISDS context\n"));
        return -1;
    }
    err = isds_set_timeout(cisds, timeout);
    if (err) {
        fprintf(stderr, _("Could not set ISDS network timeout: %s\n"),
             isds_strerror(err));
    }
    err = isds_set_progress_callback(cisds, shi_progressbar, NULL);
    if (err) {
        fprintf(stderr, _("Could not register network progress bar: %s: %s\n"),
                isds_strerror(err), isds_long_message(cisds));
    }
    err = isds_set_opt(cisds, IOPT_NORMALIZE_MIME_TYPE,
            cfg_getbool(configuration, CONFIG_NORMALIZEMIMETYPE));
    if (err) {
        fprintf(stderr,
                cfg_getbool(configuration, CONFIG_NORMALIZEMIMETYPE) ? 
                    _("Could not enable MIME type normalization: %s: %s\n") :
                    _("Could not disable MIME type normalization: %s: %s\n"),
                isds_strerror(err), isds_long_message(cisds));
    }
    if (!cfg_getbool(configuration, CONFIG_VERIFYSERVER)) {
        oprintf(_("Warning: Shigofumi disabled server identity verification "
                    "on user request!\n"));
        err = isds_set_opt(cisds, IOPT_TLS_VERIFY_SERVER, 0);
        if (err) {
            fprintf(stderr,
                    _("Could not disable server identity verification: "
                        "%s: %s\n"),
                    isds_strerror(err), isds_long_message(cisds));
        }
    }
    if ((value = cfg_getstr(configuration, CONFIG_CAFILE))) {
        err = isds_set_opt(cisds, IOPT_TLS_CA_FILE, value);
        if (err) {
            fprintf(stderr,
                    _("Could not set file with CA certificates: %s: %s: %s\n"),
                    value, isds_strerror(err), isds_long_message(cisds));
        }
    }
    if ((value = cfg_getstr(configuration, CONFIG_CADIRECTORY))) {
        err = isds_set_opt(cisds, IOPT_TLS_CA_DIRECTORY, value);
        if (err) {
            fprintf(stderr,
                    _("Could not set directory with CA certificates: "
                    "%s: %s: %s\n"),
                    value, isds_strerror(err), isds_long_message(cisds));
        }
    }
    if ((value = cfg_getstr(configuration, CONFIG_CRLFILE))) {
        err = isds_set_opt(cisds, IOPT_TLS_CRL_FILE, value);
        if (err) {
            fprintf(stderr, _("Could not set file with CRL: %s: %s: %s\n"),
                    value, isds_strerror(err), isds_long_message(cisds));
        }
    }
    

    /* Set Czech POINT context up */
    czechpoint = isds_ctx_create();
    if (!czechpoint) {
        fprintf(stderr, _("Could not create Czech POINT context\n"));
        return -1;
    }
    err = isds_set_timeout(czechpoint, timeout);
    if (err) {
        fprintf(stderr, _("Could not set Czech POINT network timeout: %s\n"),
             isds_strerror(err));
    }
    err = isds_set_progress_callback(czechpoint, shi_progressbar, NULL);
    if (err) {
        fprintf(stderr, "Could not register network progress bar: %s: %s\n",
                isds_strerror(err), isds_long_message(cisds));
    }

    return 0;
}


static int shi_quit(int argc, const char **argv) {
    shi_exit(EXIT_SUCCESS);
    return 0;
}



static void shi_help_usage(const char *command) {
    oprintf(_(
        "Usage: %s [COMMAND]\n"
        "Show COMMAND manual or list of currently available commands.\n"
        ),
            command);
}


static int shi_help(int argc, const char **argv) {
    size_t command_width = 14;
    int i;

    if (!commands) {
        fprintf(stderr, _("No command is available\n"));
        return -1;
    }

    if (argc == 2 && argv[1] && *argv[1]) {
        /* Show usage for given command */
        for (i = 0; (*commands)[i].name; i++) {
            if (!strcmp((*commands)[i].name, argv[1])) {
                if ((*commands)[i].usage)
                    (*commands)[i].usage((*commands)[i].name);
                else if ((*commands)[i].description) {
                    ohprint((*commands)[i].name, command_width);
                    oprintf("  %s\n", _((*commands)[i].description));
                }
                else
                    fprintf(stderr,
                            _("%s: %s: Command description not defined\n"),
                            argv[0], argv[1]);
                return 0;
            }
        }
        fprintf(stderr, _("%s: %s: No such command exists\n"), argv[0], argv[1]);
        return -1;
    }

    /* Or list all commands */
    oprintf(_("Following commands are available:\n"));
    for (i = 0; (*commands)[i].name; i++) {
        ohprint((*commands)[i].name, command_width);
        oprintf("  %s\n", _((*commands)[i].description));
    }

    return 0;
}


static void show_version(void) {
    char *libisds_version = isds_version();

    oprintf(_("This is Shigofumi version %s.\n"), PACKAGE_VERSION);
    oprintf(_("\n"
                "Used libraries\n"
                "confuse: %s\n"
                "libisds: %s\n"
                ),
            confuse_version, libisds_version);
#if HAVE_DECL_MAGIC_VERSION
    oprintf(_("libmagic: %d\n"), magic_version());
#endif
    oprintf(_("libxml2: %s\n"
                "Readline: %s\n"
                ),
            xmlParserVersion, rl_library_version);
    free(libisds_version);
}


static int shi_version(int argc, const char **argv) {
    show_version();
    oprintf(_(
"\n"
"-----\n"
"It's a shigofumi. A letter delivered from the afterlife.    (Fumika)\n"
"A message can not be delivered to dead person.  (ISDS specification)\n"
"Virtual and real world. They can be compatible.     (Program author)\n"
    ));
    return 0;
}


static int shi_copying(int argc, const char **argv) {
    oprintf(_(
    "This is Shigofumi, an ISDS client.\n"
    "Copyright (C) 2010, 2011, 2012, 2013, 2014, 2015, 2017, 2018  Petr Pisar\n"
    "Copyright (C) 2020, 2021  Petr Pisar\n"
    "\n"
    "This program is free software: you can redistribute it and/or modify\n"
    "it under the terms of the GNU General Public License as published by\n"
    "the Free Software Foundation, either version 3 of the License, or\n"
    "(at your option) any later version.\n"
    "\n"
    "This program is distributed in the hope that it will be useful,\n"
    "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
    "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
    "GNU General Public License for more details.\n"
    "\n"
    "You should have received a copy of the GNU General Public License\n"
    "along with this program.  If not, see <http://www.gnu.org/licenses/>.\n"
    ));
    return 0;
}


static int shi_cache(int argc, const char **argv) {
    const struct isds_list *item;
    size_t i;

    if (boxes) {
        for (item = boxes, i = 0; item; item = item->next, i++);
        oprintf(_(
                    "Cached box list: %zu\n"),
                i);
    }

    if (messages) {
        oprintf(_(
                    "Cached message list:\n"
                    "\tDirection: %s\n"
                    "\tMessages: %'lu\n"),
                (messages_are_outgoing) ? _("Outgoing") : _("Incoming"),
                total_messages);
    }

    if (message) {
        oprintf(_("Cached message: %s\n"),
                (message->envelope && message->envelope->dmID) ?
                message->envelope->dmID : _("<Unknown ID>"));
    }

    return 0;
}


static void shi_chdir_usage(const char *command) {
    oprintf(_(
                "Usage: %s [DIRECTORY]\n"
                "Change working directory to DIRECTORY.\n"
                "If no DIRECTORY is supplied, HOME directory will be used.\n"),
            command);
}


static int shi_chdir(int argc, const char **argv) {
    const char *directory = NULL;

    if (!argv || argc > 2) {
        shi_chdir_usage((argv) ? argv[0] : NULL);
        return -1;
    }

    if (argc == 2 && argv[1] && *argv[1])
        directory = argv[1];
    else {
        directory = getenv("HOME");
        if (!directory) {
            oprintf("Environment variable HOME does not exist\n");
            return -1;
        }
    }
    if (chdir(directory)) {
        oprintf(_("Could not change working directory: %s: %s\n"), directory,
                strerror(errno));
        return -1;
    }
    
    return 0;
}


static int shi_pwd(int argc, const char **argv) {
    char *buffer = NULL, *newbuffer;
    size_t length = 0;

    while (length += 1024) {
        newbuffer = realloc(buffer, length);
        if (!newbuffer) {
            fprintf(stderr, _("Error: Not enough memory\n"));
            free(buffer);
            return -1;
        }
        buffer = newbuffer;

        if (getcwd(buffer, length)) {
            oprintf("%s\n", buffer);
            free(buffer);
            return 0;
        }
    }

    fprintf(stderr, _("Error: Current directory string is too long\n"));
    free(buffer);
    return -1;
}


/* Deallocate *@destination and duplicate @new_value if non-NULL. In case of
 * error, it prints error message and returns -1. Otherwise it returns 0. */
static int replace_string(char **destination, const char *new_value) {
    if (destination == NULL) return -1;
    zfree(*destination);
    if (new_value != NULL) {
        *destination = strdup(new_value);
        if (*destination == NULL) {
            fprintf(stderr, _("Not enough memory\n"));
            return -1;
        }
    }
    return 0;
}


/* Convert name of OTP authentication method into ISDS method type.
 * Return -1 in case of invalid name */
static int string2otp_method(const char *name, isds_otp_method *method) {
   if (!name || !method) { return -1; } 
   if (!strcasecmp(name, "HOTP")) {
       *method = OTP_HMAC;
   } else if (!strcasecmp(name, "TOTP")) {
       *method = OTP_TIME;
   } else {
       return -1;
   }
   return 0;
}


/* Log-in to ISDS.
 * Return -1 in case of failure, 0 in case of success, +1 in case of partial
 * success (e.g. TOTP preauthentication to obtain new code succeeded, but user
 * is still not logged in because second phase is necessary). */
static int do_login(void) {
    isds_error err;
    struct isds_pki_credentials pki;
    struct isds_otp otp;

    /* Build OTP structure */
    if (NULL != otp_method) {
        if (string2otp_method(otp_method, &otp.method)) {
            fprintf(stderr, _("Error: Invalid one-time password "
                        "authentication method `%s'\n"), otp_method);
            return -1;
        }
    }
    
    /* Announce base URL */
    oprintf(_("ISDS base URL: %s\n"),
            (server == NULL) ? _("<default>") : server);

    if (batch_mode) {
        oprintf(_("Unattended mode detected. "
                    "Make sure credentials have been preset.\n"));
    } else {
        oprintf(_("You are going to insert credentials for your account.\n"
                    "Leave blank line to choose default value.\n"));
    }
    select_completion(COMPL_NONE);

    /* Ask for user name if not predefined */
    if (NULL == username) {
        shi_ask_for_string(&username, _("Input ISDS user name: "),
                cfg_getstr(configuration, CONFIG_USERNAME), batch_mode);
    }

    /* Ask for password */
    shi_ask_for_password(&password, _("Input ISDS password: "),
            cfg_getstr(configuration, CONFIG_PASSWORD), batch_mode);

    /* Ask for key password if PKI authentication requested */
    if (NULL != pki_key_path) {
        shi_ask_for_password(&key_password, _("Input private key password: "),
                cfg_getstr(configuration, CONFIG_KEY_PASSWORD), batch_mode);
    }

    /* Ask for OTP code if OTP authentication requested */
    if (NULL != otp_method) {
        shi_ask_for_password(&otp_code,
                (otp.method == OTP_TIME) ?
                    _("Input one-time code (empty to send new one): ") :
                    _("Input one-time code: "),
                cfg_getstr(configuration, CONFIG_OTP_CODE), batch_mode);
        otp.otp_code = otp_code;
    }

    select_completion(COMPL_COMMAND);
    set_prompt(NULL);

    /* Build PKI structure */
    if (NULL != pki_certificate_path || NULL != pki_key_path) {
        pki.engine = pki_engine;
        if (NULL == pki_certificate_format) {
            fprintf(stderr, _("Error: No certficate format supplied\n"));
            return -1;
        }
        if (string2pki_format(pki_certificate_format, &pki.certificate_format)) {
            fprintf(stderr, _("Error: Invalid certificate format `%s'\n"),
                pki_certificate_format);
            return -1;
        }
        if (NULL == pki_key_format) {
            fprintf(stderr, _("Error: No private key format supplied\n"));
            return -1;
        }
        if (string2pki_format(pki_key_format, &pki.key_format)) {
            fprintf(stderr, _("Error: Invalid private key format `%s'\n"),
                pki_key_format);
            return -1;
        }
        pki.certificate = pki_certificate_path;
        pki.key = pki_key_path;
        pki.passphrase = key_password;
    }

    if (NULL != otp_method && OTP_TIME == otp.method && NULL == otp_code)
        printf(_("Requesting one-time code from server for "
                    "a login...\n"));
    else
        printf(_("Logging in...\n"));
    err = isds_login(cisds, server, username, password,
            (NULL != pki_certificate_path || NULL != pki_key_path) ?
                &pki : NULL,
            (NULL != otp_method) ? &otp : NULL);
    if (NULL != otp_method && OTP_TIME == otp.method && NULL == otp_code)
        finish_isds_operation_with_code(cisds, err, IE_PARTIAL_SUCCESS);
    else
        finish_isds_operation(cisds, err);

    if (IE_PARTIAL_SUCCESS == err) { 
        printf(_("OTP code has been sent by ISDS successfully.\n"
                    "Once you receive the code, retry log-in with "
                    "the code.\n"));
        return 1;
    } else if (err) {
        printf(_("Log-in failed\n"));
        return -1;
    }

    oprintf(_("Logged in.\n"));
    return 0;
}


static struct isds_DbOwnerInfo *do_box(void) {
    isds_error err;
    struct isds_DbOwnerInfo *box = NULL;

    printf(_("Getting box details you are logged in...\n"));
    err = isds_GetOwnerInfoFromLogin(cisds, &box);
    finish_isds_operation(cisds, err);

    return box;
}


static int shi_box(int argc, const char **argv) {
    struct isds_DbOwnerInfo *box = NULL;

    box = do_box();
    if (!box) return -1;

    format_DbOwnerInfo(box);

    isds_DbOwnerInfo_free(&box);
    return 0;
}


/* Get info about box with @id.
 * @id is UTF-8 encoded
 * Return NULL in case of error, otherwise box description that caller must
 * free. */
static struct isds_DbOwnerInfo *stat_box(const char *id) {
    isds_error err;
    struct isds_DbOwnerInfo criteria;
    struct isds_list *boxes = NULL, *item;
    struct isds_DbOwnerInfo *box = NULL;
    char *id_locale = NULL;

    if (!id || !*id) return NULL;

    id_locale = utf82locale(id);
    memset(&criteria, 0, sizeof(criteria));
    criteria.dbID = (char *) id;

    printf(_("Getting details about box with ID `%s'...\n"), id_locale);
    err = isds_FindDataBox(cisds, &criteria, &boxes);
    finish_isds_operation(cisds, err);
    if (err) goto leave;

    for(item = boxes; item; item = item->next) {
        if (!item->data) continue;

        if (item->next) {
            fprintf(stderr, _("Error: More boxes match ID `%s'\n"), id_locale);
            goto leave;
        }

        box = (struct isds_DbOwnerInfo *) item->data;
        item->data = NULL;
        break;
    }

leave:
    free(id_locale);
    isds_list_free(&boxes);

    return box;
}


/* Obtain box ID value either from locale-encoded argument or from current box
 * the context is logged in.
 * @arg is locale-encoded box ID or NULL
 * @box_id outputs pointer to reallocated UTF-8 encoded box ID. Pass NULL if
 * you are not interreted in it. Will be NULLed on error.
 * @box_id_locale outputs pointer to reallocated locale-encoded box ID. Pass
 * NULL if you are not interrested in it. Will be NULLed on error.
 * @return 0 on sucess, -1 on error. */
static int get_current_box_id(const char *arg,
        char **box_id, char **box_id_locale) {
    struct isds_DbOwnerInfo *box = NULL;

    if (NULL != box_id) zfree(*box_id);
    if (NULL != box_id_locale) zfree(*box_id_locale);

    if (NULL == arg || '\0' == *arg) {
        /* Get current box ID */
        box = do_box();
        if (NULL == box || NULL == box->dbID || !*box->dbID) {
            isds_DbOwnerInfo_free(&box);
            fprintf(stderr, _("Could not get current box ID\n"));
            goto error;
        }
        if (NULL != box_id) {
            *box_id = strdup(box->dbID);
            if (NULL == *box_id) {
                fprintf(stderr, _("Not enough memory\n"));
                goto error;
            }
        }
        if (NULL != box_id_locale) {
            *box_id_locale = utf82locale(box->dbID);
            if (NULL == *box_id_locale) {
                fprintf(stderr, _("Could not convert box ID to locale\n"));
                goto error;
            }
        }
    } else {
        /* Box ID supplied as argument */
        if (NULL != box_id_locale) {
            *box_id_locale = strdup(arg);
            if (NULL == *box_id_locale) {
                fprintf(stderr, _("Not enough memory\n"));
                goto error;
            }
        }
        if (NULL != box_id) {
            *box_id = locale2utf8(arg);
            if (NULL == *box_id) {
                fprintf(stderr, _("Could not convert box ID `%s' to UTF-8\n"),
                        arg);
                goto error;
            }
        }
    }

    return 0;

error:
    if (NULL != box_id) zfree(*box_id);
    if (NULL != box_id_locale) zfree(*box_id_locale);
    return -1;
}


static void shi_commercialcredit_usage(const char *command) {
    oprintf(_(
"Usage: %s [OPTION...] [BOX_ID]\n"
"Retrieve details and history of a credit available for sending commercial\n"
"messages from a box with ID BOX_ID. Default is box you are logged in.\n"
"Options require a date argument:\n"
"  -f     list history from date inclusive (locale or full ISO 8601 date)\n"
"  -t     list history to date inclusive (locale or full ISO 8601 date)\n"
        ), command);
}


/* Retrieve details about credit for sending commercial messages */
static int shi_commercialcredit(int argc, const char **argv) {
    isds_error err;
    struct tm *from_date = NULL, *to_date = NULL;
    long int current_credit;
    char *notification_email = NULL;
    struct isds_list *history = NULL, *item;
    int opt;
    char *box_id = NULL, *box_id_locale = NULL;
    int ordinar;
    int retval = 0;

    optind = 0;
    while ((opt = getopt(argc, (char * const *)argv, "f:ht:")) != -1) {
        switch (opt) {
            case 'f':
                free(from_date);
                from_date = datestring2tm(optarg);
                if (NULL == from_date) {
                    fprintf(stderr, _("Error: Could not parse date: %s\n"),
                            optarg);
                    retval = -1;
                    goto leave;
                }
                break;
            case 'h':
                shi_commercialcredit_usage((argv)?argv[0]:NULL);
                goto leave;
            case 't':
                free(to_date);
                to_date = datestring2tm(optarg);
                if (NULL == to_date) {
                    fprintf(stderr, _("Error: Could not parse date: %s\n"),
                            optarg);
                    retval = -1;
                    goto leave;
                }
                break;
            default:
                shi_commercialcredit_usage((argv)?argv[0]:NULL);
                retval = -1;
                goto leave;
        }
    }
    if (optind + 1 < argc) {
        fprintf(stderr, _("Bad invocation\n"));
        shi_commercialcredit_usage((argv)?argv[0]:NULL);
        retval = -1;
        goto leave;
    }

    if ((retval = get_current_box_id(argv[optind], &box_id, &box_id_locale))) {
        goto leave;
    }

    printf(_("Querying `%s' box credit details...\n"),
            box_id_locale);
    err = isds_get_commercial_credit(cisds, box_id, from_date, to_date,
            &current_credit, &notification_email, &history);
    finish_isds_operation(cisds, err);

    if (!err) {
        oprintf(_("Details of credit for sending commercial messages "
                    "from box `%s':\n"), box_id_locale);
        print_header_currency(_("Current credit"), &current_credit);
        print_header_utf8(_("Notification e-mail"), notification_email);

        if (NULL != history) {
            oprintf(_("History of credit change events:\n"));
            print_header_tm(_("From"), from_date);
            print_header_tm(_("To"), to_date);
            for (item = history, ordinar = 0; item; item=item->next) {
                if (!item->data) continue;
                ordinar++;
                oprintf(_("\n* Event #%d:\n"), ordinar);
                format_credit_event(item->data);
            }
            if (ordinar == 0)
                oprintf(_("No event exists.\n"));
        }
    } else {
        oprintf(_("Could not get details about a credit available for sending "
                    "commercial messages\n"
                    "from box `%s'.\n"),
                box_id_locale);
        retval = -1;
    }

leave:
    isds_list_free(&history);
    free(notification_email);
    free(from_date);
    free(to_date);
    free(box_id);
    free(box_id_locale);
    return retval;
}


static void shi_commercialreceiving_usage(const char *command) {
    oprintf(_(
"Usage: %s [-0|-1] [BOX_ID]\n"
"Manipulate commercial receiving box status.\n"
"  -O      switch off receiving of commercial messages\n"
"  -1      switch on receiving of commercial messages\n"
"  BOX_ID  affects box with ID BOX_ID; default is box you are logged in\n"
"If no option is given, show current commercial receiving status.\n"),
            command);
}


/* Manipulate commercial receiving box status */
static int shi_commercialreceiving(int argc, const char **argv) {
    isds_error err;
    int opt;
    int action = -1;
    char *box_id = NULL, *box_id_locale = NULL;
    int retval = 0;

    optind = 0;
    while ((opt = getopt(argc, (char * const *)argv, "01")) != -1) {
        switch (opt) {
            case '0':
                action = 0;
                break;
            case '1':
                action = 1;
                break;
            default:
                shi_commercialreceiving_usage((argv)?argv[0]:NULL);
                return -1;
        }
    }
    if (optind + 1 < argc) {
        fprintf(stderr, _("Bad invocation\n"));
        shi_commercialreceiving_usage((argv)?argv[0]:NULL);
        return -1;
    }

    if ((retval = get_current_box_id(argv[optind], &box_id, &box_id_locale))) {
        goto leave;
    }

    if (action == -1) {
        struct isds_DbOwnerInfo *box = stat_box(box_id);
        if (!box) {
            fprintf(stderr, _("Could not get details about box ID `%s'\n"),
                    box_id_locale);
            retval = -1;
            goto leave;
        }

        oprintf(_("Commercial receiving status of box `%s': "), box_id_locale);
        if (!box->dbOpenAddressing)
            oprintf(_("Unknown\n"));
        else if (*box->dbOpenAddressing)
            oprintf(_("Positive\n"));
        else
            oprintf(_("Negative\n"));
        isds_DbOwnerInfo_free(&box);
    } else {
        char *refnumber = NULL;
        printf((action) ?
                _("Switching `%s' box commercial receiving on...\n"):
                _("Switching `%s' box commercial receiving off...\n"),
                box_id_locale);
        err = isds_switch_commercial_receiving(cisds, box_id, action,
                NULL, &refnumber);
        finish_isds_operation(cisds, err);

        if (!err) {
            char *refnumber_locale = utf82locale(refnumber);
            oprintf(_("Commercial receiving status successfully changed. "
                        "Assigned reference number: %s\n"),
                    refnumber_locale);
            free(refnumber_locale);
        } else {
            oprintf(_("Commercial receiving status has not been changed.\n"));
            retval = -1;
        }
        free(refnumber);
    }

leave:
    free(box_id);
    free(box_id_locale);
    return retval;
}


static void shi_commercialsending_usage(const char *command) {
    oprintf(_(
"Usage: %s [BOX_ID]\n"
"Retrieve permissions to send commercial messages from a box.\n"
"  BOX_ID  query permissions for box with ID BOX_ID; default is box you\n"
"          are logged in\n"),
            command);
}


/* Retrieve permissions to send commercial messages */
static int shi_commercialsending(int argc, const char **argv) {
    isds_error err;
    struct isds_DbOwnerInfo *box = NULL;
    struct isds_list *permissions = NULL, *item;
    int opt;
    char *box_id = NULL, *box_id_locale = NULL;
    int ordinar;
    int retval = 0;

    optind = 0;
    while ((opt = getopt(argc, (char * const *)argv, "h")) != -1) {
        switch (opt) {
            case 'h':
                shi_commercialsending_usage((argv)?argv[0]:NULL);
                return 0;
            default:
                shi_commercialsending_usage((argv)?argv[0]:NULL);
                return -1;
        }
    }
    if (optind + 1 < argc) {
        fprintf(stderr, _("Bad invocation\n"));
        shi_commercialsending_usage((argv)?argv[0]:NULL);
        return -1;
    }

    if ((retval = get_current_box_id(argv[optind], &box_id, &box_id_locale))) {
        return -1;
    }

    printf(_("Querying `%s' box commercial sending permissions...\n"),
            box_id_locale);
    err = isds_get_commercial_permissions(cisds, box_id, &permissions);
    finish_isds_operation(cisds, err);

    if (!err) {
        oprintf(_("Permissions to send commercial messages from box `%s':\n"),
                box_id_locale);
        for (item = permissions, ordinar = 0; item; item=item->next) {
            if (!item->data) continue;
            ordinar++;
            oprintf(_("\n* Permission #%d:\n"), ordinar);
            format_commercial_permission(item->data);
        }
        if (ordinar == 0)
            oprintf(_("No permission exists.\n"));
    } else {
        oprintf(_("Could not list permissions to send commercial messages "
                    "from box `%s'.\n"), box_id_locale);
        retval = -1;
    }

    isds_list_free(&permissions);
    free(box_id);
    free(box_id_locale);
    isds_DbOwnerInfo_free(&box);
    return retval;
}


static int shi_user(int argc, const char **argv) {
    isds_error err;
    struct isds_DbUserInfo *user = NULL;

    printf(_("Getting user details you are logged as...\n"));
    err = isds_GetUserInfoFromLogin(cisds, &user);
    finish_isds_operation(cisds, err);
    if (err) return -1;

    format_DbUserInfo(user);

    isds_DbUserInfo_free(&user);
    return 0;
}


static void shi_users_usage(const char *command) {
    oprintf(_(
                "Usage: %s BOX_ID\n"
                "Get list of users having access to box with BOX_ID.\n"),
            command);
}


static int shi_users(int argc, const char **argv) {
    isds_error err;
    struct isds_list *users = NULL, *item;
    int ordinar;

    if (!argv || !argv[1] || !*argv[1]) {
        shi_users_usage((argv)?argv[0]:NULL);
        return -1;
    }

    printf(_("Getting users of box with ID `%s'...\n"), argv[1]);
    err = isds_GetDataBoxUsers(cisds, argv[1], &users);
    finish_isds_operation(cisds, err);
    if (err) return -1;

    for (item = users, ordinar = 0; item; item=item->next) {
        if (!item->data) continue;
        ordinar++;
        oprintf(_("\n* User #%d:\n"), ordinar);
        format_DbUserInfo(item->data);
    }
    if (ordinar == 0)
        oprintf(_("Empty list of users returned.\n"));

    isds_list_free(&users);
    return 0;
}


static int show_password_expiration(void) {
    isds_error err;
    struct timeval *expiration = NULL;

    err = isds_get_password_expiration(cisds, &expiration);
    finish_isds_operation(cisds, err);
    if (err) {
        fprintf(stderr, "Could not get password expiration time\n");
        return -1;
    }

    print_header_timeval(_("Your password expires at"), expiration);
    free(expiration);
    return 0;
}


/* Change password in ISDS */
static int do_passwd(void) {
    char *old_password = NULL;
    char *new_password = NULL;
    char *new_password2 = NULL;
    struct isds_otp otp;
    char *refnumber = NULL;
    isds_error err = IE_ERROR;
    int retval = 0;

    if (replace_string(&otp_method, cfg_getstr(configuration, CONFIG_OTP_METHOD)))
        return -1;
    /* Build OTP structure */
    if (NULL != otp_method) {
        if (string2otp_method(otp_method, &otp.method)) {
            fprintf(stderr, _("Error: Invalid one-time password "
                        "authentication method `%s'\n"), otp_method);
            return -1;
        }
    }

    select_completion(COMPL_NONE);

    oprintf(_(
"You are going to change your password. If you don't want to change your\n"
"password, insert empty string or EOF.\n"
"\n"
"You will be asked for your current (old) password and then for new password.\n"
"ISDS forces some criteria new password must fulfill. Current rules are:\n"
"\tLength: minimal 8, maximal 32 characters\n"
"\tMust contain at least: 1 upper case letter, 1 lower case letter, 1 digit\n"
"\tAllowed alphabet: [a-z][A-Z][0-9][!#$%%&()*+,-.:=?@[]_{}|~]\n"
"\tMust differ from last 255 passwords\n"
"\tMust not contain user ID\n"
"\tMust not contain sequence of three or more same characters\n"
"\tMust not start with `qwert', `asdgf', or `12345'\n"
"Finally, you must repeat your new password to avoid mistakes.\n"
"After password change will be confirmed, you must log in again as password\n"
"is transmitted to server on each request.\n"
"\n"));

    old_password = ask_for_password(_("Old password: "));
    if (!old_password || *old_password == '\0') {
        fprintf(stderr, _("No password supplied\n"));
        goto error;
    }

    /* Ask for OTP code if OTP authentication requested */
    zfree(otp_code);
    if (NULL != otp_method) {
        shi_ask_for_password(&otp_code,
                (otp.method == OTP_TIME) ?
                    _("One-time code (empty to send new one): ") :
                    _("One-time code: "),
                cfg_getstr(configuration, CONFIG_OTP_CODE), batch_mode);
        otp.otp_code = otp_code;
    
    }

    if (NULL == otp_method || NULL != otp_code) {
        new_password = ask_for_password(_("New password: "));
        if (!new_password || *new_password == '\0') {
            fprintf(stderr, _("No password supplied\n"));
            goto error;
        }

        new_password2 = ask_for_password(_("Repeat new password: "));
        if (!new_password2 || *new_password2 == '\0') {
            fprintf(stderr, _("No password supplied\n"));
            goto error;
        }

        if (strcmp(new_password, new_password2)) {
            fprintf(stderr, _("New passwords differ\n"));
            goto error;
        }
    }

    if (NULL != otp_method && OTP_TIME == otp.method && NULL == otp_code)
        printf(_("Requesting one-time code from server for "
                    "a password change...\n"));
    else
        printf(_("Changing password...\n"));
    err = isds_change_password(cisds, old_password, new_password,
            (NULL != otp_method) ? &otp : NULL, &refnumber);
    if (NULL != otp_method && OTP_TIME == otp.method && NULL == otp_code)
        finish_isds_operation_with_code(cisds, err, IE_PARTIAL_SUCCESS);
    else
        finish_isds_operation(cisds, err);

    if (NULL != refnumber) {
            char *refnumber_locale = utf82locale(refnumber);
            free(refnumber);
            oprintf(_("Assigned reference number: %s\n"), refnumber_locale);
            free(refnumber_locale);
    }
    if (IE_PARTIAL_SUCCESS == err) { 
        oprintf(_("OTP code has been sent by ISDS successfully.\n"
                    "Once you receive the code, retry changing password with "
                    "the code.\n"));
        goto leave;
    } else if (err) {
        printf(_("Password change failed\n"));
        goto error;
    } else {
        oprintf(_("Password HAS been successfully changed.\n"));
        goto leave;
    }

error:
    retval = -1;
    oprintf(_("Password has NOT been changed!\n"));

leave:
    free(old_password);
    free(new_password);
    free(new_password2);
    zfree(otp_code);

    set_prompt(NULL);
    select_completion(COMPL_COMMAND);

    oprintf(_("\n"
            "Remember, ISDS password has limited life time.\n"));
    return retval;
}


static void shi_passwd_usage(const char *command) {
    oprintf(_(
"Usage: %s [-S]\n"
"Manipulate user password or change it if no option given.\n"
"\n"
"Options:\n"
"  -S     show password expiration time\n"
                ), command);
}


static int shi_passwd(int argc, const char **argv) {
    int opt;

    optind = 0;
    while ((opt = getopt(argc, (char * const *)argv, "S")) != -1) {
        switch (opt) {
            case 'S':
                return show_password_expiration();
            default:
                shi_passwd_usage(argv[0]);
                return -1;
        }
    }
    if (optind != argc || argc > 1) {
        fprintf(stderr, _("Bad invocation\n"));
        shi_passwd_usage((argv)?argv[0]:NULL);
        return -1;
    }

    return do_passwd();
}


static void shi_login_usage(const char *command) {
    oprintf(_(
"Usage: %s [OPTIONS] [USER_NAME]\n"
"Attemp to log into ISDS server.\n"
"\n"
"Options:\n"
"  -b URL           ISDS server base URL\n"
"  -c IDENTIFIER    user certificate\n"
"  -C FORMAT        user certificate format\n"
"  -k IDENTIFIER    user private key\n"
"  -K FORMAT        user private key format\n"
"  -e IDENTIFIER    cryptographic engine\n"
"  -o METHOD        use one-time password authentication method\n"
"\n"
"Recognized certificate and key FORMATS are:\n"
"  PEM    Base64 encoded serialization in local file\n"
"  DER    binary serialization in local file\n"
"  ENG    material is stored in cryptographic engine\n"
"Identifiers of cryptographic engine, certificate, and private key are\n"
"specific for underlying cryptographic library.\n"
"\n"
"Recognized one-time password methods are:\n"
"  HOTP   HMAC-based OTP method\n"
"  TOTP   time-based OTP method\n"
"\n"
"Values of omitted options are taken from configuration file.\n"
                ), command);
}


static int shi_login(int argc, const char **argv) {
    int opt;
    int status;

    discard_credentials();

    /* Load stored configuration */
    if (replace_string(&server, cfg_getstr(configuration, CONFIG_SERVER)))
        return -1;
    if (replace_string(&otp_method, cfg_getstr(configuration, CONFIG_OTP_METHOD)))
        return -1;
    if (replace_string(&pki_engine,
                cfg_getstr(configuration, CONFIG_KEY_ENGINE)))
        return -1;
    if (replace_string(&pki_certificate_path,
                cfg_getstr(configuration, CONFIG_CERT_PATH)))
        return -1;
    if (replace_string(&pki_certificate_format,
                cfg_getstr(configuration, CONFIG_CERT_FORMAT)))
        return -1;
    if (replace_string(&pki_key_path,
                cfg_getstr(configuration, CONFIG_KEY_PATH)))
        return -1;
    if (replace_string(&pki_key_format,
                cfg_getstr(configuration, CONFIG_KEY_FORMAT)))
        return -1;

    /* Override configuration with positional arguments */
    optind = 0;
    while ((opt = getopt(argc, (char * const *)argv, "b:c:C:k:K:e:o:")) != -1) {
        switch (opt) {
            case 'b':
                if (replace_string(&server, optarg)) return -1;
                break;
            case 'c':
                if (replace_string(&pki_certificate_path, optarg)) return -1;
                break;
            case 'C':
                if (replace_string(&pki_certificate_format, optarg)) return -1;
                break;
            case 'k':
                if (replace_string(&pki_key_path, optarg)) return -1;
                break;
            case 'K':
                if (replace_string(&pki_key_format, optarg)) return -1;
                break;
            case 'e':
                if (replace_string(&pki_engine, optarg)) return -1;
                break;
            case 'o':
                if (replace_string(&otp_method, optarg)) return -1;
                break;
            default:
                shi_login_usage(argv[0]);
                return -1;
        }
    }
    if (optind < argc - 1) {
        fprintf(stderr, _("Bad invocation\n"));
        shi_login_usage(argv[0]);
        return -1;
    }
    if (optind == argc - 1) {
        username = strdup(argv[optind]);
    }

    /* Proceed log-in */
    status = do_login();
    if (status < 0) return -1;

    /* If log-in passed, store configuration */
    cfg_setstr(configuration, CONFIG_SERVER, server);
    cfg_setstr(configuration, CONFIG_USERNAME, username);
    cfg_setstr(configuration, CONFIG_PASSWORD, password);
    cfg_setstr(configuration, CONFIG_KEY_PASSWORD, key_password);
    cfg_setstr(configuration, CONFIG_OTP_METHOD, otp_method);
    cfg_setstr(configuration, CONFIG_KEY_ENGINE, pki_engine);
    cfg_setstr(configuration, CONFIG_CERT_PATH, pki_certificate_path);
    cfg_setstr(configuration, CONFIG_CERT_FORMAT, pki_certificate_format);
    cfg_setstr(configuration, CONFIG_KEY_PATH, pki_key_path);
    cfg_setstr(configuration, CONFIG_KEY_FORMAT, pki_key_format);

    /* Get some details only if fully logged in */
    if (0 == status) {
        show_password_expiration();
    }
    return 0;
}


static void shi_debug_usage(const char *command) {
    oprintf(_(
"Usage: %s -l LEVEL [-f FACILITY...] [{-e | -o FILE}]\n"
"Debug FACILITIES on LEVEL.\n"
"\n"
"-l LEVEL       set log level, valid interval <%d,%d>, default is %d\n"
"               %d is no logging, %d critical, %d errors,\n"
"               %d warnings, %d info, %d debug, %d all\n"
"-f FACILITY    debug only given facility, repeat this option to debug\n"
"               more facilities; valid values: none, http, soap, isds,\n"
"               file, sec, xml, all; default is none\n"
"-e             write debug log into stderr\n"
"-o FILE        append debug log to FILE\n"
        ),
            command,
            ILL_NONE, ILL_ALL, ILL_NONE,
                ILL_NONE, ILL_CRIT, ILL_ERR, ILL_WARNING,
                ILL_INFO, ILL_DEBUG, ILL_ALL);
}

static int shi_debug(int argc, const char **argv) {
    int opt;
    int log_level = ILL_NONE;
    isds_log_facility log_facility = ILF_NONE;
    char *file = NULL;
    _Bool close_log = 0;

    optind = 0;
    while ((opt = getopt(argc, (char * const *)argv, "l:f:eo:")) != -1) {
        switch (opt) {
            case 'l':
                log_level = normalize_log_level(atoi(optarg));
                break;
            case 'f':
                if (add_log_facility(&log_facility, optarg)) return -1;
                break;
            case 'e':
                close_log = 1;
            case 'o':
                file = optarg;
                break;
            default:
                shi_debug_usage(argv[0]);
                return -1;
        }
    }
    if (optind == 1 || optind != argc) {
        fprintf(stderr, _("Bad invocation\n"));
        shi_debug_usage(argv[0]);
        return -1;
    }

    /* Redirect log */
    if (close_log) {
        isds_set_log_callback(NULL, NULL);

        if (logger_fd != -1) {
            if (-1 == close(logger_fd)) {
                fprintf(stderr, _("Closing log file failed: %s\n"),
                        strerror(errno));
                return -1;
            }
        }
        cfg_setstr(configuration, CONFIG_LOGFILE, NULL);
    }
    if (do_log_to_file(file))
        return -1;
    if (file) cfg_setstr(configuration, CONFIG_LOGFILE, file);
        
    /* Set log levels */
    isds_set_logging(log_facility, log_level);
    cfg_setint(configuration, CONFIG_LOGLEVEL, log_level);
    save_log_facility(log_facility);

    return 0;
}


static void show_setting_str(const char *variable, _Bool cenzore) {
    if (!variable) return;

    const char *value = cfg_getstr(configuration, variable);
    
    if (value) {
        if (cenzore)
            oprintf(_("%s = <set>\n"), variable);
        else
            oprintf(_("%s = `%s'\n"), variable, value);
    } else {
        oprintf(_("%s = <unset>\n"), variable);
    }
}


static void show_setting_boolean(const char *variable) {
    if (!variable) return;

    _Bool value = cfg_getbool(configuration, variable);
    
    if (value) {
        oprintf(_("%s = <true>\n"), variable);
    } else {
        oprintf(_("%s = <false>\n"), variable);
    }
}


static void show_setting_int(const char *variable) {
    if (!variable) return;

    long int value = cfg_getint(configuration, variable);
    
    oprintf(_("%s = %ld\n"), variable, value);
}


static void show_setting_strlist(const char *variable) {
    if (!variable) return;

    int length = cfg_size(configuration, variable);
    
    if (length <= 0) {
        oprintf(_("%s = <unset>\n"), variable);
    } else {
        oprintf(_("%s = {"), variable);
        for (int i = 0; i < length; i++) {
            const char *value = cfg_getnstr(configuration, variable, i);
            if (i < length - 1)
                oprintf(_("`%s', "), value);
            else
                oprintf(_("`%s'}\n"), value);
        }
    }
}


static int shi_settings(int argc, const char **argv) {
    /*int opt;
    int log_level = ILL_NONE;
    isds_log_facility log_facility = ILF_NONE;
    char *file = NULL;
    _Bool close_log = 0;*/

    /*
    optind = 0;
    while ((opt = getopt(argc, (char * const *)argv, "l:f:eo:")) != -1) {
        switch (opt) {
            case 'l':
                log_level = normalize_log_level(atoi(optarg));
                break;
            case 'f':
                if (add_log_facility(&log_facility, optarg)) return -1;
                break;
            case 'e':
                close_log = 1;
            case 'o':
                file = optarg;
                break;
            default:
                shi_debug_usage(argv[0]);
                return -1;
        }
    }
    if (optind == 1 || optind != argc) {
        printf(_("Bad invocation\n"));
        shi_debug_usage(argv[0]);
        return -1;
    }
    */

    oprintf(_("Current settings:\n"));

    show_setting_str(CONFIG_SERVER, 0);
    show_setting_str(CONFIG_USERNAME, 0);
    show_setting_str(CONFIG_PASSWORD, 1),
    show_setting_str(CONFIG_CERT_FORMAT, 0),
    show_setting_str(CONFIG_CERT_PATH, 0),
    show_setting_str(CONFIG_KEY_ENGINE, 0),
    show_setting_str(CONFIG_KEY_FORMAT, 0),
    show_setting_str(CONFIG_KEY_PATH, 0),
    show_setting_str(CONFIG_KEY_PASSWORD, 1),
    show_setting_str(CONFIG_OTP_METHOD, 0),
    show_setting_str(CONFIG_OTP_CODE, 1),
    show_setting_boolean(CONFIG_VERIFYSERVER);
    show_setting_str(CONFIG_CAFILE, 0);
    show_setting_str(CONFIG_CADIRECTORY, 0);
    show_setting_boolean(CONFIG_CLEAN_TEMPORARY_FILES);
    show_setting_str(CONFIG_CRLFILE, 0);
    show_setting_int(CONFIG_TIMEOUT);
    show_setting_strlist(CONFIG_LOGFACILITIES);
    show_setting_str(CONFIG_LOGFILE, 0);
    show_setting_int(CONFIG_LOGLEVEL);
    show_setting_boolean(CONFIG_CONFIRM_SEND);
    show_setting_boolean(CONFIG_MARKMESSAGEREAD);
    show_setting_boolean(CONFIG_NORMALIZEMIMETYPE);
    show_setting_strlist(CONFIG_OPENCOMMAND);
    show_setting_boolean(CONFIG_OVERWRITEFILES);

    return 0;
}


static void shi_find_box_usage(const char *command) {
    oprintf(_(
"Usage: %s {OPTION... | BOX_ID}\n"
"Get information about box with BOX_ID or boxes meeting other criteria.\n"
"Each search option requires an argument:\n"
"  -t     box type; accepted values:\n"
"             FO            Private individual\n"
"             PFO           Self-employed individual\n"
"             PFO_ADVOK     Lawyer\n"
"             PFO_AUDITOR   Statutory auditor\n"
"             PFO_DANPOR    Tax advisor\n"
"             PFO_INSSPR    Insolvency administrator\n"
"             PO            Organisation\n"
"             PO_ZAK        Organization based by law\n"
"             PO_REQ        Organization based on request\n"
"             OVM           Public authority\n"
"             OVM_EXEKUT    Executor\n"
"             OVM_NOTAR     Notary\n"
"             OVM_REQ       Public authority based on request\n"
"             OVM_FO        Private individual listed in the public authority\n"
"                           index\n"
"             OVM_PFO       Self-employed individual listed in the public\n"
"                           authority index\n"
"             OVM_PO        Organisation listed in the public authority index\n"
"  -j     identity number\n"
"\n"
"Person name options:\n"
"  -f     first name\n"
"  -m     middle name\n"
"  -l     last name\n"
"  -b     last name at birth\n"
"  -s     subject name\n"
"\n"
"Birth options:\n"
"  -d     birth date (locale or full ISO 8601 date)\n"
"  -w     birth city\n"
"  -y     birth county\n"
"  -c     birth state\n"
"\n"
"Address:\n"
"  -W     city\n"
"  -S     street\n"
"  -z     number in street\n"
"  -Z     number in municipality\n"
"  -P     ZIP code\n"
"  -C     state\n"
"\n"
"Other options:\n"
"  -n     nationality\n"
"  -e     e-mail\n"
"  -p     phone number\n"
"  -i     identifier\n"
"  -r     registry code\n"
"  -a     box status; accepted values:\n"
"             ACCESSIBLE            Accessible\n"
"             TEMP_INACCESSIBLE     Temporary inaccessible\n"
"             NOT_YET_ACCESSIBLE    Not yet accessible\n"
"             PERM_INACCESSIBLE     Permanently inaccessible\n"
"             REMOVED               Deleted\n"
"  -o     act as public authority; boolean values: 0 is false, 1 is true\n"
"  -k     receive commercial messages; boolean values\n"
"\n"
"Not all option combinations are meaningful or allowed. For example box\n"
"type is always required (except direct box ID query).\n"
"ISDS can refuse to answer to much broad query. Not all boxes are searchable\n"
"by every user.\n"
),
            command);
}


/* Allow  reassignment */
#define FILL_OR_LEAVE(variable, locale) { \
    zfree(variable); \
    (variable) = locale2utf8(locale); \
    if (!(variable)) { \
        fprintf(stderr, _("Error: Not enough memory\n")); \
        retval = -1; \
        goto leave; \
    } \
}

#define CALLOC_OR_LEAVE(structure) { \
    if (!(structure)) { \
        (structure) = calloc(1, sizeof(*(structure))); \
        if (!(structure)) { \
            fprintf(stderr, _("Error: Not enough memory\n")); \
            retval = -1; \
            goto leave; \
        } \
    } \
}

#define FILL_BOOLEAN_OR_LEAVE(variable, locale) { \
    zfree(variable); \
    (variable) = malloc(sizeof(*(variable))); \
    if (!(variable)) { \
        fprintf(stderr, _("Error: Not enough memory\n")); \
        retval = -1; \
        goto leave; \
    } \
    if (!strcmp((locale), "0")) *(variable) = 0; \
    else if (!strcmp((locale), "1")) *(variable) = 1; \
    else { \
        fprintf(stderr, _("%s: %s: Unknown boolean value\n"), \
                argv[0], (locale)); \
        retval = -1; \
        goto leave; \
    } \
}

#define FILL_LONGINT_OR_LEAVE(variable, locale) { \
    if (!(locale) || !*(locale)) { \
        fprintf(stderr, _("%s: Empty integer value\n"), argv[0]); \
        retval = -1; \
        goto leave; \
    } \
    char *endptr; \
    zfree(variable); \
    (variable) = malloc(sizeof(*(variable))); \
    if (!(variable)) { \
        fprintf(stderr, _("Error: Not enough memory\n")); \
        retval = -1; \
        goto leave; \
    } \
    (*variable) = strtol((locale), &endptr, 0); \
    if (*endptr) { \
        fprintf(stderr, _("%s: %s: Invalid integer value\n"), \
                argv[0], (locale)); \
        retval = -1; \
        goto leave; \
    } \
}

#define FILL_ULONGINT_OR_LEAVE(variable, locale) { \
    long int value; \
    if (!(locale) || !*(locale)) { \
        fprintf(stderr, _("%s: Empty integer value\n"), argv[0]); \
        retval = -1; \
        goto leave; \
    } \
    char *endptr; \
    zfree(variable); \
    (variable) = malloc(sizeof(*(variable))); \
    if (!(variable)) { \
        fprintf(stderr, _("Error: Not enough memory\n")); \
        retval = -1; \
        goto leave; \
    } \
    value = strtol((locale), &endptr, 0); \
    if (*endptr) { \
        fprintf(stderr, _("%s: %s: Invalid integer value\n"), \
                argv[0], (locale)); \
        retval = -1; \
        goto leave; \
    } \
    if (value < 0) { \
        fprintf(stderr, _("%s: %s: Negative integer value\n"), \
                argv[0], (locale)); \
        retval = -1; \
        goto leave; \
    } \
    (*variable) = value; \
}

static int shi_find_box(int argc, const char **argv) {
    int opt;
    isds_error err;
    struct isds_DbOwnerInfo *criteria = NULL;
    struct isds_list *item;
    int order = 0;
    int retval = 0;

    if (!argv || !argv[1] || !*argv[1]) {
        fprintf(stderr, _("Error: No argument supplied\n"));
        shi_find_box_usage((argv)?argv[0]:NULL);
        return -1;
    }

    criteria = calloc(1, sizeof(*criteria));
    if (!criteria) {
        fprintf(stderr, _("Error: Not enough memory\n"));
        retval = -1;
        goto leave;
    }

    /* Parse options */
    optind = 0;
    while ((opt = getopt(argc, (char * const *)argv, "t:j:s:"
                    "f:m:l:b:s:" "d:w:y:c:" "W:S:z:Z:P:C:"
                    "n:e:p:i:r:a:o:k:")) != -1) {
        switch (opt) {
            case 't':
                criteria->dbType = malloc(sizeof(*criteria->dbType));
                if (!criteria->dbType) {
                    fprintf(stderr, _("Error: Not enough memory\n"));
                    retval = -1;
                    goto leave;
                }
                if (!string2isds_DbType(criteria->dbType, optarg)) {
                    fprintf(stderr, _("%s: %s: Unknown box type\n"),
                            argv[0], optarg);
                    retval = -1;
                    goto leave;
                }
                break;

            case 'j':
                FILL_OR_LEAVE(criteria->ic, optarg);
                break;

            /* Person name */
            case 'f':
                CALLOC_OR_LEAVE(criteria->personName);
                FILL_OR_LEAVE(criteria->personName->pnFirstName, optarg);
                break;
            case 'm':
                CALLOC_OR_LEAVE(criteria->personName);
                FILL_OR_LEAVE(criteria->personName->pnMiddleName, optarg);
                break;
            case 'l':
                CALLOC_OR_LEAVE(criteria->personName);
                FILL_OR_LEAVE(criteria->personName->pnLastName, optarg);
                break;
            case 'b':
                CALLOC_OR_LEAVE(criteria->personName);
                FILL_OR_LEAVE(criteria->personName->pnLastNameAtBirth, optarg);
                break;
            case 's':
                FILL_OR_LEAVE(criteria->firmName, optarg);
                break;

            /* Birth */
            case 'd':
                CALLOC_OR_LEAVE(criteria->birthInfo);
                criteria->birthInfo->biDate = datestring2tm(optarg);
                if (!criteria->birthInfo->biDate) {
                    fprintf(stderr, _("Error: Could not parse date: %s\n"),
                            optarg);
                    retval = -1;
                    goto leave;
                }
                break;
            case 'w':
                CALLOC_OR_LEAVE(criteria->birthInfo);
                FILL_OR_LEAVE(criteria->birthInfo->biCity, optarg);
                break;
            case 'y':
                CALLOC_OR_LEAVE(criteria->birthInfo);
                FILL_OR_LEAVE(criteria->birthInfo->biCounty, optarg);
                break;
            case 'c':
                CALLOC_OR_LEAVE(criteria->birthInfo);
                FILL_OR_LEAVE(criteria->birthInfo->biState, optarg);
                break;

            /* Address */
            case 'W':
                CALLOC_OR_LEAVE(criteria->address);
                FILL_OR_LEAVE(criteria->address->adCity, optarg);
                break;
            case 'S':
                CALLOC_OR_LEAVE(criteria->address);
                FILL_OR_LEAVE(criteria->address->adStreet, optarg);
                break;
            case 'z':
                CALLOC_OR_LEAVE(criteria->address);
                FILL_OR_LEAVE(criteria->address->adNumberInStreet, optarg);
                break;
            case 'Z':
                CALLOC_OR_LEAVE(criteria->address);
                FILL_OR_LEAVE(criteria->address->adNumberInMunicipality,
                        optarg);
                break;
            case 'P':
                CALLOC_OR_LEAVE(criteria->address);
                FILL_OR_LEAVE(criteria->address->adZipCode, optarg);
                break;
            case 'C':
                CALLOC_OR_LEAVE(criteria->address);
                FILL_OR_LEAVE(criteria->address->adState, optarg);
                break;

            /* Other options */
            case 'n':
                FILL_OR_LEAVE(criteria->nationality, optarg);
                break;
            case 'e':
                FILL_OR_LEAVE(criteria->email, optarg);
                break;
            case 'p':
                FILL_OR_LEAVE(criteria->telNumber, optarg);
                break;
            case 'i':
                FILL_OR_LEAVE(criteria->identifier, optarg);
                break;
            case 'r':
                FILL_OR_LEAVE(criteria->registryCode, optarg);
                break;
            case 'a':
                criteria->dbState = malloc(sizeof(*criteria->dbState));
                if (!criteria->dbState) {
                    fprintf(stderr, _("Error: Not enough memory\n"));
                    retval = -1;
                    goto leave;
                }
                if (!strcmp(optarg, "ACCESSIBLE"))
                    *criteria->dbState = DBSTATE_ACCESSIBLE;
                else if (!strcmp(optarg, "TEMP_INACCESSIBLE"))
                    *criteria->dbState = DBSTATE_TEMP_UNACCESSIBLE;
                else if (!strcmp(optarg, "NOT_YET_ACCESSIBLE"))
                    *criteria->dbState = DBSTATE_NOT_YET_ACCESSIBLE;
                else if (!strcmp(optarg, "PERM_INACCESSIBLE"))
                    *criteria->dbState = DBSTATE_PERM_UNACCESSIBLE;
                else if (!strcmp(optarg, "REMOVED"))
                    *criteria->dbState = DBSTATE_REMOVED;
                else {
                    fprintf(stderr, _("%s: %s: Unknown box status\n"),
                            argv[0], optarg);
                    retval = -1;
                    goto leave;
                }
                break;
            case 'o':
                FILL_BOOLEAN_OR_LEAVE(criteria->dbEffectiveOVM, optarg);
                break;
            case 'k':
                FILL_BOOLEAN_OR_LEAVE(criteria->dbOpenAddressing, optarg);
                break;

            default:
                shi_find_box_usage(argv[0]);
                retval = -1;
                goto leave;
        }
    }

    /* There must be an option and all of them must be recognized, if not only
     * BOX_ID supplied */
    if (argc > 2 && optind != argc) {
        fprintf(stderr, _("Error: Superfluous argument\n"));
        shi_find_box_usage(argv[0]);
        retval = -1;
        goto leave;
    }
   
    /* If only box ID is supplied use it */
    if (argc == 2 && argv[1] && *argv[1]) {
        criteria->dbID = locale2utf8(argv[1]);
        if (!criteria->dbID) {
            fprintf(stderr, _("Error: Not enough memory\n"));
            retval = -1;
            goto leave;
        }
    }

    printf(_("Searching boxes...\n"));
    err = isds_FindDataBox(cisds, criteria, &boxes);
    finish_isds_operation(cisds, err);
    if (err) return -1;

    for(item = boxes; item; item = item->next) {
        if (!item->data) continue;
        order++;
        
        oprintf(_("\n* Result #%d:\n"), order);
        format_DbOwnerInfo(item->data);
    }

leave:
    isds_DbOwnerInfo_free(&criteria);
    return retval;
}


static void shi_search_box_usage(const char *command) {
    oprintf(_(
"Usage: %s [OPTION...] QUERY...\n"
"Get list of boxes matching a query string.\n"
"Attribute options:\n"
"  -a           search in addresses only\n"
"  -b           search in box identifiers only\n"
"  -i           search in organization identifiers only\n"
"\n"
"Box type options:\n"
"  -t TYPE      restrict to a box type; accepted values:\n"
"                   FO            Private individual\n"
"                   PFO           Self-employed individual\n"
"                   PFO_ADVOK     Lawyer\n"
"                   PFO_AUDITOR   Statutory auditor\n"
"                   PFO_DANPOR    Tax advisor\n"
"                   PFO_INSSPR    Insolvency administrator\n"
"                   PO            Organization\n"
"                   PO_ZAK        Organization based by law\n"
"                   PO_REQ        Organization based on request\n"
"                   OVM           Public authority\n"
"                   OVM_NOTAR     Notary\n"
"                   OVM_EXEKUT    Executor\n"
"                   OVM_REQ       Public authority based on request\n"
"                   OVM_FO        Private individual listed in the public\n"
"                                 authority index\n"
"                   OVM_PFO       Self-employed individual listed in the public\n"
"                                 authority index\n"
"                   OVM_PO        Organisation listed in the public authority\n"
"                                 index\n"
"\n"
"Pagination options:\n"
"  -p NUMBER    request a page with this number (defaults to the first one)\n"
"  -s NUMBER    request a page with this size\n"
"\n"
"The query arguments are concatenated by a space into one query string.\n"
"If no attribute option is specified, the query string will be searched in\n"
"all of the attributes (address, box ID, organization ID). The server splits\n"
"the query string into words and normalize them according to complex rules\n"
"(see ISDS specification for more details) before the search. Each word\n"
"matches independently, but all of them must exist in a box attributes to\n"
"return the box.\n"
),
            command);
}


static int shi_search_box(int argc, const char **argv) {
    int opt;
    isds_error err;
    isds_fulltext_target target = FULLTEXT_ALL;
    isds_DbType type = DBTYPE_SYSTEM;
    unsigned long int *page_size = NULL;
    unsigned long int *page_number = NULL;
    char *query_locale = NULL;
    char *query = NULL;
    unsigned long int *total_matching_boxes = NULL;
    unsigned long int *current_page_beginning = NULL;
    unsigned long int *current_page_size = NULL;
    _Bool *last_page = NULL;
    struct isds_list *boxes = NULL;
    struct isds_list *item;
    unsigned long int order = 0;
    int retval = 0;

    if (!argv || !argv[1] || !*argv[1]) {
        fprintf(stderr, _("Error: No argument supplied\n"));
        shi_search_box_usage((argv)?argv[0]:NULL);
        return -1;
    }

    /* Parse options */
    optind = 0;
    while ((opt = getopt(argc, (char * const *)argv, "abi" "t:" "p:s:"))
            != -1) {
        switch (opt) {
            case 'a':
                if (target != FULLTEXT_ALL) {
                    fprintf(stderr, _("%s: -%c: Another attribute option "
                                "has already been specified\n"),
                            argv[0], opt);
                    retval = -1;
                    goto leave;
                }
                target = FULLTEXT_ADDRESS;
                break;
            case 'b':
                if (target != FULLTEXT_ALL) {
                    fprintf(stderr, _("%s: -%c: Another attribute option "
                                "has already been specified\n"),
                            argv[0], opt);
                    retval = -1;
                    goto leave;
                }
                target = FULLTEXT_BOX_ID;
                break;
            case 'i':
                if (target != FULLTEXT_ALL) {
                    fprintf(stderr, _("%s: -%c: Another attribute option "
                                "has already been specified\n"),
                            argv[0], opt);
                    retval = -1;
                    goto leave;
                }
                target = FULLTEXT_IC;
                break;
            case 't':
                if (!string2isds_DbType(&type, optarg)) {
                    fprintf(stderr, _("%s: %s: Unknown box type\n"),
                            argv[0], optarg);
                    retval = -1;
                    goto leave;
                }
                break;

            case 'p':
                FILL_ULONGINT_OR_LEAVE(page_number, optarg);
                break;

            case 's':
                FILL_ULONGINT_OR_LEAVE(page_size, optarg);
                break;

            default:
                shi_search_box_usage(argv[0]);
                retval = -1;
                goto leave;
        }
    }

    /* All options are optional, there must one non-options argument, the
     * query */
    if (optind >= argc) {
        fprintf(stderr, _("Error: Missing a query argument\n"));
        retval = -1;
        goto leave;
    }

    /* If only box ID is supplied use it */
    if (NULL == argv[optind] || '\0' == *argv[optind]) {
        fprintf(stderr, _("Error: The query string must be non-empty\n"));
        retval = -1;
        goto leave;
    }

    /* Concatenate arguments into one query string */
    for (int i = optind; NULL != argv[i]; i++) {
        char *new_query_locale;
        new_query_locale = astrcat3(query_locale, " ", argv[i]);
        if (NULL == new_query_locale) {
            fprintf(stderr, _("Error: Not enough memory\n"));
            retval = -1;
            goto leave;
        }
        free(query_locale);
        query_locale = new_query_locale;
    }
    FILL_OR_LEAVE(query, query_locale);

    printf(_("Searching boxes...\n"));
    err = isds_find_box_by_fulltext(cisds,
            query, &target, &type, page_size, page_number, 0,
            &total_matching_boxes, &current_page_beginning, &current_page_size,
            &last_page, &boxes);
    finish_isds_operation(cisds, err);
    if (err) return -1;

    if (NULL != current_page_beginning) {
        order = *current_page_beginning;
    }
    print_header_ulongint(_("Total matching boxes"), total_matching_boxes);
    print_header_ulongint(_("This page size"), current_page_size);

    for(item = boxes; item; item = item->next) {
        if (!item->data) continue;
        order++;

        oprintf(_("\n* Result #%lu:\n"), order);
        format_isds_fulltext_result(item->data);
    }

    if (NULL != last_page) {
        if (NULL != boxes)
            oprintf("\n");
        if (*last_page)
            oprintf(_("This is the last page.\n"));
        else
            oprintf(_("Next pages exist.\n"));
    }

leave:
    free(page_size);
    free(page_number);
    free(query);
    free(query_locale);
    free(total_matching_boxes);
    free(current_page_beginning);
    free(current_page_size);
    free(last_page);
    isds_list_free(&boxes);
    return retval;
}


static void shi_stat_box_usage(const char *command) {
    oprintf(_(
"Usage: %s BOX_ID...\n"
"Get status of box with BOX_ID. More boxes can be specified.\n"),
            command);
}


/* Get boxes status */
static int shi_stat_box(int argc, const char **argv) {
    isds_error err;
    char *id = NULL;
    long int status;

    if (!argv || !*argv || argc < 2 || !argv[1]) {
        fprintf(stderr, _("Missing box ID\n"));
        shi_stat_box_usage((argv[0])?argv[0]:NULL);
        return -1;
    }

    for (int i = 1; i < argc; i++) {
        if (!argv[i] || !*argv[i]) continue;

        free(id);
        id = locale2utf8(argv[i]);
        if (!id) {
            fprintf(stderr, _("%s: Could not covert box ID to UTF-8\n"),
                    argv[i]);
            return -1;
        }

        printf(_("Getting status of box `%s'...\n"), argv[i]);
        err = isds_CheckDataBox(cisds, id, &status);
        finish_isds_operation(cisds, err);
        if (err) return -1;
        
        oprintf(_("Status of box `%s': %s\n"),
                argv[i], DbState2string(&status));
    }

    return 0;
}


static void shi_boxlist_usage(const char *command) {
    oprintf(_(
"Usage: %s LIST_TYPE FILE\n"
"Save latest snapshot of list of boxes of type LIST_TYPE into FILE.\n"
"\n"
"Currently recognized LIST_TYPES are:\n"
"  ALL  All boxes\n"
"  UPG  Effectively OVM boxes\n"
"  OVM  OVM gross type boxes\n"
"  OPN  Boxes allowing receiving commercial messages\n"
"\n"
"Not all types are available to all users. E.g. only `UPG' is available\n"
"to regular users.\n"
"\n"
"The format of the list is comma separate list that is packed into\n"
"ZIP archive. Name of the list file denotes time of snapshoting\n"
"the list. The snapshot is created by ISDS once a day.\n"),
            command);
}


/* Download list of boxes */
static int shi_boxlist(int argc, const char **argv) {
    isds_error err;
    const char *type_locale;
    char *type = NULL;
    void *buffer = NULL;
    size_t buffer_length;
    int retval;

    if (!argv || !*argv || argc < 3 || !argv[1] || !argv[2]) {
        fprintf(stderr, _("Bad number of arguments\n"));
        shi_boxlist_usage((argv)?argv[0]:NULL);
        return -1;
    }
    type_locale = argv[1];

    type = locale2utf8(type_locale);
    if (!type) {
        fprintf(stderr, _("%s: Could not covert list type to UTF-8\n"),
                type_locale);
        return -1;
    }

    printf(_("Getting `%s' list of boxes...\n"), type_locale);
    err = isds_get_box_list_archive(cisds, type, &buffer, &buffer_length);
    finish_isds_operation(cisds, err);
    free(type);
    if (err) {
        return -1;
    }
    
    retval = save_data_to_file(argv[2], -1, buffer, buffer_length,
            "application/zip",
            cfg_getbool(configuration, CONFIG_OVERWRITEFILES));

    free(buffer);
    return retval;
}


static void shi_delivery_usage(const char *command) {
    oprintf(_(
                "Usage: %s [MESSAGE_ID]\n"
                "Get delivery data about a message.\n"
                "If MESSAGE_ID is defined, query for that message.\n"
                "Otherwise use current message.\n"),
            command);
}


static int shi_delivery(int argc, const char **argv) {
    isds_error err;
    const char *id = NULL;
    struct isds_message *delivery_info = NULL;

    if (!argv || argc > 2) {
        shi_delivery_usage((argv)?argv[0]:NULL);
        return -1;
    }
    if (argc == 2 && argv[1] && *argv[1]) {
        id = argv[1];
    } else {
        if (!message) {
            fprintf(stderr, _("No message loaded\n"));
            return -1;
        }
        if (!message->envelope || !message->envelope->dmID) {
            fprintf(stderr, _("Current message is missing ID\n"));
            return -1;
        }
        id = message->envelope->dmID;
    }

    printf(_("Getting delivery info...\n"));
    err = isds_get_signed_delivery_info(cisds, id, &delivery_info);
    finish_isds_operation(cisds, err);
    if (err)
        return -1;

    isds_message_free(&message);
    message = delivery_info;

    format_message(message);

    if (message->envelope && message->envelope->dmID)
        set_prompt(_("%s %s"), argv[0], message->envelope->dmID);
    else 
        set_prompt("%s", argv[0]);
    select_completion(COMPL_MSG);
    return 0;
}


static int shi_dump_message(int argc, const char **argv) {
    if (!message) {
        fprintf(stderr, _("No message loaded\n"));
        return -1;
    }

    print_message(message);
    return 0;
}


static void shi_hash_usage(const char *command) {
    oprintf(_(
        "Usage: %s [MESSAGE_ID]\n"
        "Retrieve message hash stored in ISDS.\n"
        "If MESSAGE_ID is defined, query for that message.\n"
        "Otherwise use current message.\n"),
            command);
}


static int shi_hash(int argc, const char **argv) {
    isds_error err;
    const char *id = NULL;
    struct isds_hash *hash = NULL;
    char *hash_string = NULL;

    if (!argv || argc > 2) {
        shi_hash_usage((argv)?argv[0]:NULL);
        return -1;
    }
    if (argc == 2 && argv[1] && *argv[1])
        id = argv[1];
    else {
        if (!message) {
            fprintf(stderr, _("No message loaded\n"));
            return -1;
        }
        if (!message->envelope || !message->envelope->dmID) {
            fprintf(stderr, _("Current message is missing ID\n"));
            return -1;
        }
        id = message->envelope->dmID;
    }

    printf(_("Getting message hash...\n"));
    err = isds_download_message_hash(cisds, id, &hash);
    finish_isds_operation(cisds, err);
    if (err) return -1;

    hash_string = hash2string(hash);
    oprintf(_("ISDS states message with `%s' ID has following hash:\n%s\n"),
            id, hash_string);

    free(hash_string);
    isds_hash_free(&hash);
    return 0;
}


static int shi_verify(int argc, const char **argv) {
    isds_error err;
    int retval = 0;
    struct isds_hash *retrieved_hash = NULL, *stored_hash = NULL;
    char *hash_string = NULL;
    size_t width = 4;

    if (!message) {
        fprintf(stderr, _("No message loaded\n"));
        return -1;
    }
  
    if (!message->envelope) {
        fprintf(stderr, _("Current message is missing envelope\n"));
        return -1;
    }
    stored_hash = message->envelope->hash;
    message->envelope->hash = NULL;
    
    if (message->envelope->dmID) {
        /* Verify remote hash */
        oprintf(_("Remote hash check:\n"));

        printf(_("Getting message hash...\n"));
        err = isds_download_message_hash(cisds, message->envelope->dmID,
                &retrieved_hash);
        finish_isds_operation(cisds, err);

        if (retrieved_hash) {
            hash_string = hash2string(retrieved_hash);
            ohprint(_("Retrieved:"), width);
            oprintf("%s\n", hash_string);
            zfree(hash_string);
        }

        if (retrieved_hash && message->raw) {
            err = isds_compute_message_hash(cisds, message,
                    retrieved_hash->algorithm);
            finish_isds_operation(cisds, err);

            if (!err) {
                hash_string = hash2string(message->envelope->hash);
                ohprint(_("Computed:"), width);
                oprintf("%s\n", hash_string);
                zfree(hash_string);
            }
        }

        err = isds_hash_cmp(retrieved_hash, message->envelope->hash);
        switch (err) {
            case IE_SUCCESS:
                oprintf(_("Hashes match.\n")); break;
            case IE_NOTUNIQ:
                oprintf(_("Hashes do not match.\n"));
                retval = -1;
                break;
            default:
                oprintf(_("Hashes could not be compared.\n"));
                retval = -1;
                break;
        }

        free(retrieved_hash);
    }


    if (stored_hash) {
        /* Verify stored hash */
        oprintf(_("Stored hash check:\n"));

        hash_string = hash2string(stored_hash);
        ohprint(_("Stored:"), width);
        oprintf("%s\n", hash_string);
        zfree(hash_string);

        if (message->raw) {
            err = isds_compute_message_hash(cisds, message,
                    stored_hash->algorithm);
            finish_isds_operation(cisds, err);

            if (!err) {
                hash_string = hash2string(message->envelope->hash);
                ohprint(_("Computed:"), width);
                oprintf("%s\n", hash_string);
                zfree(hash_string);
            }
        }

        err = isds_hash_cmp(stored_hash, message->envelope->hash);
        switch (err) {
            case IE_SUCCESS:
                oprintf(_("Hashes match.\n")); break;
            case IE_NOTUNIQ:
                oprintf(_("Hashes do not match.\n"));
                retval = -1;
                break;
            default:
                oprintf(_("Hashes could not be compared.\n"));
                retval = -1;
                break;
        }

        isds_hash_free(&message->envelope->hash);
    }

    message->envelope->hash = stored_hash;
    return retval;
}


static int shi_authenticate(int argc, const char **argv) {
    isds_error err;
    int retval = 0;

    if (!message) {
        fprintf(stderr, _("No message loaded\n"));
        return -1;
    }
    if (!message->raw || message->raw_length == 0) {
        fprintf(stderr, _("Current message is missing raw representation\n"));
        return -1;
    }

    printf(_("Submitting message to authenticity check...\n"));
    err = isds_authenticate_message(cisds, message->raw, message->raw_length);
    finish_isds_operation(cisds, (err == IE_NOTUNIQ) ? IE_SUCCESS : err);

    switch (err) {
        case IE_SUCCESS:
            oprintf(_("Message originates in ISDS.\n")); break;
        case IE_NOTUNIQ:
            oprintf(_("Message is unknown to ISDS or has been tampered.\n"));
            retval = -1;
            break;
        default:
            retval = -1;
            break;
    }

    return retval;
}


static void shi_accept_message_usage(const char *command) {
    oprintf(_(
"Usage: %s [MESSAGE_ID...]\n"
"Accept commercial message moving its state to received.\n"
"If MESSAGE_ID is defined, accept that message. More messages can be specified.\n"
"Otherwise accept all commercial incoming messages.\n"),
            command);
}


static int shi_accept_message(int argc, const char **argv) {
    isds_error err;
    char *id = NULL;

    /* Process messages named in argv */
    for (int i = 1; i < argc; i++) {
        if (!argv[i] || !*argv[i]) continue;

        id = locale2utf8(argv[i]);
        if (!id) {
            fprintf(stderr,
                    _("Error: Could not convert message ID to UTF-8: %s\n"),
                    argv[i]);
            return -1;
        }

        printf(_("Accepting message `%s'...\n"), argv[i]);
        err = isds_mark_message_received(cisds, id);
        finish_isds_operation(cisds, err);
        if (err) {
            free(id);
            return -1;
        };

        oprintf(_("Message `%s' accepted\n"), argv[i]);
        free(id);
    }

    if (argc < 2) {
        /* TODO: list commercial not received messages and accept all of them
         * */
        fprintf(stderr,
                _("Error: No message ID supplied. Accepting all commercial "
                    "messages not implemented yet.\n"));
        return -1;
    }

    return 0;
}


static void shi_delete_message_usage(const char *command) {
    oprintf(_(
"Usage: %s {-i|-o} MESSAGE_ID...\n"
"Remove message from long term storage.\n"
"Options:\n"
"  -i  Messages are incoming\n"
"  -o  Messages are outoging\n"),
            command);
}


static int shi_delete_message(int argc, const char **argv) {
    isds_error err;
    char *id = NULL;
    _Bool incoming = 0; 
    _Bool direction_specified = 0;
    int opt;

    optind = 0;
    while ((opt = getopt(argc, (char * const *)argv, "io")) != -1) {
        switch (opt) {
            case 'i':
                incoming = 1;
                direction_specified = 1;
                break;
            case 'o':
                incoming = 0;
                direction_specified = 1;
                break;
            default:
                shi_delete_message_usage((argv)?argv[0]:NULL);
                return -1;
        }
    }
    if (optind >= argc || !argv || !argv[optind] || !*argv[optind]) {
        fprintf(stderr, _("Bad invocation\n"));
        shi_delete_message_usage((argv)?argv[0]:NULL);
        return -1;
    }
    if (!direction_specified) {
        fprintf(stderr, _("Message direction has not been specified\n"));
        shi_delete_message_usage((argv)?argv[0]:NULL);
        return -1;
    }

    /* Process messages named in argv */
    for (int i = optind; i < argc; i++) {
        if (!argv[i] || !*argv[i]) continue;

        id = locale2utf8(argv[i]);
        if (!id) {
            fprintf(stderr,
                    _("Error: Could not convert message ID to UTF-8: %s\n"),
                    argv[i]);
            return -1;
        }

        printf(_("Deleting message `%s'...\n"), argv[i]);
        err = isds_delete_message_from_storage(cisds, id, incoming);
        finish_isds_operation(cisds, err);
        if (err) {
            free(id);
            return -1;
        };

        oprintf(_("Message `%s' deleted\n"), argv[i]);
        free(id);
    }

    return 0;
}


/* Convert message ID form locale to UTF-8 or in other direction. If both
 * strings are provided, UTF-8 will take precedence. The missing string is
 * automatically allocated (but not freed before). If UTF-8 version has been
 * provided, @stastic_utf8 will become 1, otherwise 0. You can pass the
 * strings and the flags to free_message_id() to free memory properly.*/
static int convert_message_id(char **id_utf8, char **id_locale, _Bool *static_utf8) {
    if (!id_utf8 || !id_locale || !static_utf8) return -1;
    if (!*id_utf8 && !*id_locale) return -1;

    if (*id_utf8) {
        *static_utf8 = 1;
        *id_locale = utf82locale(*id_utf8);
    } else {
        *static_utf8 = 0;
        *id_utf8 = locale2utf8(*id_locale);
        if (!*id_utf8) {
            fprintf(stderr,
                    _("Error: Could not convert message ID to UTF-8: %s\n"),
                    *id_locale);
            return -1;
        }
    }

    return 0;
}


/* Free message ID strings as were allocated by convert_message_id() */
static int free_message_id(char **id_utf8, char **id_locale, _Bool static_utf8) {
    if (!id_utf8 || !id_locale) return -1;
    if (static_utf8) zfree(*id_locale);
    else zfree(*id_utf8);
    return 0;
}


/* Return static UTF-8 encoded ID of current message. In case of error NULL. */
static const char *get_current_message_id(void) { 
        if (!message) { 
            fprintf(stderr, _("No message loaded\n"));
            return NULL;
        }
        if (!message->envelope) { 
            fprintf(stderr, _("Loaded message is missing envelope\n"));
            return NULL;
        }
        if (!message->envelope->dmID || !*message->envelope->dmID) { 
            fprintf(stderr, _("Loaded message is missing ID\n"));
            return NULL;
        }
        return message->envelope->dmID;
}


static void shi_message_sender_usage(const char *command) {
    oprintf(_(
"Usage: %s [MESSAGE_ID...]\n"
"Get details about sender of a message.\n"
"If MESSAGE_ID is defined, get sender of that message. More messages can be specified.\n"
"Otherwise will get sender of current message, if any is loaded.\n"),
            command);
}


/* Get details about sender of message with given ID. At least one form must
 * be specified.
 * @message_id is UTF-8 string
 * @message_id_locale is string in locale encoding
 * @return 0 on success, -1 on failure */
static int do_message_sender(const char *message_id, const char *message_id_locale) {
    isds_sender_type *type = NULL;
    char *raw_type = NULL;
    char *name = NULL;
    isds_error err;
    _Bool static_id;

    if (convert_message_id((char **)&message_id, (char **)&message_id_locale,
                &static_id))
        return -1;
    
    printf(_("Getting sender of message `%s'...\n"), message_id_locale);
    err = isds_get_message_sender(cisds, message_id, &type, &raw_type, &name);
    finish_isds_operation(cisds, err);
    if (err) {
        free_message_id((char **)&message_id, (char **)&message_id_locale,
                static_id);
        return -1;
    }

    format_sender_info(message_id, type, raw_type, name);

    free_message_id((char **)&message_id, (char **)&message_id_locale,
            static_id);
    zfree(type);
    zfree(raw_type);
    zfree(name);
    return 0;
}


static int shi_message_sender(int argc, const char **argv) {
    if (argc < 2) {
        return do_message_sender(get_current_message_id(), NULL);
    }

    for (int i = 1; i < argc; i++) {
        if (!argv[i] || !*argv[i]) continue;
        if (do_message_sender(NULL, argv[i]))
            return -1;
    }

    return 0;
}


/* Mark message as read. At least one form of ID must be provided.
 * @id is UTF-8 encoded message ID
 * @id_locale is locale encoded message ID. @id takes preference. */
static int do_read_message(const char *id, const char *id_locale) {
    _Bool static_id;
    isds_error err;

    if ((!id || !*id) && (!id_locale || !*id_locale)) return -1;

    if (convert_message_id((char **)&id, (char **)&id_locale, &static_id)) return -1;

    printf(_("Marking message `%s' as read...\n"), id_locale);
    err = isds_mark_message_read(cisds, id);
    finish_isds_operation(cisds, err);

    if (!err)
        oprintf(_("Message `%s' marked as read\n"), id_locale);

    free_message_id((char **)&id, (char **)&id_locale, static_id);

    return (err) ? -1 : 0;
}


static void shi_read_message_usage(const char *command) {
    oprintf(_(
"Usage: %s [MESSAGE_ID...]\n"
"Mark message as read moving its state to read.\n"
"\n"
"When new incoming message is download, its state is not changed on server.\n"
"Client must mark such message as read explicitly. You can use this command\n"
"to do so, if not done automatically at download time by your client.\n"
"\n"
"If MESSAGE_ID is defined, mark that message. More messages can be specified.\n"
"Otherwise marks currently loaded message.\n"),
            command);
}


static int shi_read_message(int argc, const char **argv) {
    if (argc < 2) {
        return do_read_message(get_current_message_id(), NULL);
    }

    for (int i = 1; i < argc; i++) {
        if (!argv[i] || !*argv[i]) continue;
        if (do_read_message(NULL, argv[i]))
            return -1;
    }

    return 0;
}


static void shi_cat_message_usage(const char *command) {
    oprintf(_(
"Usage: %s\n"
"Print unformated raw representation of current message.\n"
"\n"
"This is the same content you would get into file by `save' command.\n"
"\n"
"Be ware the binary stream can screw your terminal. No new line character\n"
"will be appended to the end of the output.\n"),
            command);
}


static int shi_cat_message(int argc, const char **argv) {
    if (!message) {
        fprintf(stderr, _("No message loaded\n"));
        return -1;
    }

    if (!message->raw || !message->raw_length) {
        fprintf(stderr, _("Current message is missing raw representation\n"));
        return -1;
    }

    if (owrite(message->raw, message->raw_length) != message->raw_length) {
        fprintf(stderr, _("Error while printing message content\n"));
        return -1;
    }

    return 0;
}


static int shi_show_message(int argc, const char **argv) {
    if (!message) {
        fprintf(stderr, _("No message loaded\n"));
        return -1;
    }

    format_message(message);
    return 0;
}


static void shi_incoming_message_usage(const char *command) {
    oprintf(_(
                "Usage: %s [-r] MESSAGE_ID\n"
                "Get incoming message with MESSAGE_ID.\n"
                "Options:\n"
                "  -r  Mark mesage as read\n"),
            command);
}


static int shi_incoming_message(int argc, const char **argv) {
    isds_error err;
    const char *id;
    int opt;
    _Bool mark_as_read = 0;

    optind = 0;
    while ((opt = getopt(argc, (char * const *)argv, "r")) != -1) {
        switch (opt) {
            case 'r':
                mark_as_read = 1;
                break;
            default:
                shi_incoming_message_usage((argv)?argv[0]:NULL);
                return -1;
        }
    }
    if (optind + 1 != argc || !argv || !argv[optind] || !*argv[optind]) {
        fprintf(stderr, _("Bad invocation\n"));
        shi_incoming_message_usage((argv)?argv[0]:NULL);
        return -1;
    }
    id = argv[optind];

    printf(_("Getting incoming message...\n"));
    err = isds_get_signed_received_message(cisds, id, &message);
    finish_isds_operation(cisds, err);
    if (err) {
        set_prompt(NULL);
        select_completion(COMPL_COMMAND);
        return -1;
    }

    format_message(message);

    if (message->envelope && message->envelope->dmID)
        set_prompt(_("%s %s"), argv[0], message->envelope->dmID);
    else 
        set_prompt("%s", argv[0]);
    select_completion(COMPL_MSG);

    if (mark_as_read || cfg_getbool(configuration, CONFIG_MARKMESSAGEREAD)) {
        if (message->envelope && message->envelope->dmMessageStatus &&
                ! (*message->envelope->dmMessageStatus & MESSAGESTATE_READ))
            return do_read_message(id, NULL);
    }
    return 0;
}


static void shi_outgoing_message_usage(const char *command) {
    oprintf(_(
                "Usage: %s MESSAGE_ID\n"
                "Get outgoing message with MESSAGE_ID.\n"),
            command);
}


static int shi_outgoing_message(int argc, const char **argv) {
    isds_error err;
    const char *id;

    if (!argv || !argv[1] || !*argv[1]) {
        shi_outgoing_message_usage(argv[0]);
        return -1;
    }
    id = argv[1];

    printf(_("Getting outgoing message...\n"));
    err = isds_get_signed_sent_message(cisds, id, &message);
    finish_isds_operation(cisds, err);
    if (err) {
        set_prompt(NULL);
        select_completion(COMPL_COMMAND);
        return -1;
    }

    format_message(message);
    if (message->envelope && message->envelope->dmID)
        set_prompt(_("%s %s"), argv[0], message->envelope->dmID);
    else 
        set_prompt("%s", argv[0]);
    select_completion(COMPL_MSG);
    return 0;
}


/* Detect type (message or delivery data) and load it. And change completion
 * and show the data.
 * @buffer is memory with message or delivery data
 * @length is size of @buffer in bytes
 * @strategy defines how to fill global message variable
 * @return 0 for success, otherwise non-zero. */
static int do_load_anything(const void *buffer, size_t length,
        isds_buffer_strategy strategy) {
    isds_raw_type raw_type;
    isds_error err;
    char *type_name = NULL;

    if (NULL == buffer || 0 == length) {
        return -1;
    }

    printf(_("Detecting format...\n"));
    err = isds_guess_raw_type(cisds, &raw_type, buffer, length);
    finish_isds_operation(cisds, err);
 
    if (err) {
        if (err == IE_NOTSUP)
            fprintf(stderr, _("Unknown format.\n"));
        else 
            fprintf(stderr, _("Error while detecting format.\n"));
    } else {
        switch (raw_type) {
            case RAWTYPE_INCOMING_MESSAGE:
            case RAWTYPE_PLAIN_SIGNED_INCOMING_MESSAGE:
            case RAWTYPE_CMS_SIGNED_INCOMING_MESSAGE:
            case RAWTYPE_PLAIN_SIGNED_OUTGOING_MESSAGE:
            case RAWTYPE_CMS_SIGNED_OUTGOING_MESSAGE:
                err = isds_load_message(cisds, raw_type,
                        buffer, length, &message, strategy);
                finish_isds_operation(cisds, err);
                type_name = N_("message");
                break;

            case RAWTYPE_DELIVERYINFO:
            case RAWTYPE_PLAIN_SIGNED_DELIVERYINFO:
            case RAWTYPE_CMS_SIGNED_DELIVERYINFO:
                err = isds_load_delivery_info(cisds, raw_type,
                        buffer, length, &message, strategy);
                finish_isds_operation(cisds, err);
                type_name = N_("delivery");
                break;

            default:
                fprintf(stderr,
                        _("Unsupported format.\n"));
                err = IE_NOTSUP;
        }
    }

    if (err) {
        set_prompt(NULL);
        select_completion(COMPL_COMMAND);
        return -1;
    }

    format_message(message);

    if (message->envelope && message->envelope->dmID)
        set_prompt(_("%s %s"), _(type_name), message->envelope->dmID);
    else 
        set_prompt("%s", _(type_name));
    select_completion(COMPL_MSG);
    return 0;
}


static void shi_load_anything_usage(const char *command) {
    oprintf(_(
                "Usage: %s FILE\n"
                "Load message or message delivery details from local FILE.\n"),
            command);
}


static int shi_load_anything(int argc, const char **argv) {
    int fd;
    void *buffer = NULL;
    size_t length;
    int error;

    if (!argv || !argv[1] || !*argv[1]) {
        shi_load_anything_usage((argv)?argv[0]:NULL);
        return -1;
    }

    printf(_("Loading file `%s'...\n"), argv[1]);

    if (mmap_file(argv[1], &fd, &buffer, &length)) return -1;

    error = do_load_anything(buffer, length, BUFFER_COPY);

    munmap_file(fd, buffer, length);

    return error;
}


static void shi_save_message_usage(const char *command) {
    oprintf(_(
                "Usage: %s FILE\n"
                "Save message into local FILE.\n"),
            command);
}


static const char *raw_type2mime(isds_raw_type raw_type) {
    switch (raw_type) {
        case RAWTYPE_INCOMING_MESSAGE:
        case RAWTYPE_PLAIN_SIGNED_INCOMING_MESSAGE:
        case RAWTYPE_PLAIN_SIGNED_OUTGOING_MESSAGE:
        case RAWTYPE_DELIVERYINFO:
        case RAWTYPE_PLAIN_SIGNED_DELIVERYINFO:
            return "text/xml";

        case RAWTYPE_CMS_SIGNED_INCOMING_MESSAGE:
        case RAWTYPE_CMS_SIGNED_OUTGOING_MESSAGE:
        case RAWTYPE_CMS_SIGNED_DELIVERYINFO:
            return "application/pkcs7-mime";

        default:
            return NULL;
    }
}


static int shi_save_message(int argc, const char **argv) {
    _Bool overwrite = cfg_getbool(configuration, CONFIG_OVERWRITEFILES);

    if (!argv || !argv[1] || !*argv[1]) {
        shi_save_message_usage(argv[0]);
        return -1;
    }

    if (!message) {
        fprintf(stderr, _("No message loaded\n"));
        return -1;
    }
    if (!message->raw || message->raw_length == 0) {
        fprintf(stderr, _("Loaded message is missing raw representation\n"));
        return -1;
    }

    return save_data_to_file(argv[1], -1, message->raw, message->raw_length,
            raw_type2mime(message->raw_type), overwrite);
}


/* Return document of current message identified by ordinal number expressed
 * as string. In case of error return NULL. */
static const struct isds_document *locate_document_by_ordinal_string(
        const char *number) {
    const struct isds_list *item;
    const struct isds_document *document = NULL;
    int ordinar, i;

    if (!number) return NULL;

    ordinar = atoi(number);
    if (ordinar <= 0) {
        fprintf(stderr, _("%s: Document number must be positive number\n"),
                number);
        return NULL;
    }

    if (!message) {
        fprintf(stderr, _("No message loaded\n"));
        return NULL;
    }

    /* Find document */
    for (item = message->documents, i = 0; item; item = item->next) {
        if (!item->data) continue;
        if (++i == ordinar) {
            document = (const struct isds_document *) item->data;
            break;
        }
    }
    if (i != ordinar) {
        fprintf(stderr, _("Message does not contain document #%d\n"), ordinar);
        return NULL;
    }

    return document;
}


static void shi_cat_document_usage(const char *command) {
    oprintf(_(
        "Usage: %s NUMBER\n"
        "Print document selected with ordinal NUMBER.\n"),
            command);
}

static int shi_cat_document(int argc, const char **argv) {
    const struct isds_document *document;

    if (!argv || !argv[1] || !*argv[1] || argc > 3) {
        shi_cat_document_usage(argv[0]);
        return -1;
    }

    document = locate_document_by_ordinal_string(argv[1]);
    if (!document) return -1;

    if (document->is_xml) {
        xmlBufferPtr buffer = NULL;
        size_t written;

        if (serialize_xml_to_buffer(&buffer, document->xml_node_list))
            return -1;

        written = owrite(buffer->content, buffer->use);
        xmlBufferFree(buffer);
        if (written != buffer->use) {
            fprintf(stderr, _("Error while printing document content\n"));
            return -1;
        }
    } else {
        if (!document->data || !document->data_length) {
            fprintf(stderr, _("Document is missing raw representation\n"));
            return -1;
        }

        if (owrite(document->data, document->data_length) != document->data_length) {
            fprintf(stderr, _("Error while printing document content\n"));
            return -1;
        }
    }

    return 0;
}


static void shi_save_document_usage(const char *command) {
    oprintf(_(
"Usage: %s NUMBER [DESTINATION]\n"
"Save document having ordinal NUMBER within current message into local file.\n"
"If DESTINATION is file (or does not exist yet), document will be saved into\n"
"this file.\n"
"If DESTINATION is existing directory, file name equaled to document name\n"
"will be saved into DESTINATION.\n"
"If DESTINATION is missing, document name will be used as file name and\n"
"saved into working directory.\n"
"Be aware that document name does not embed malicious characters (slashes).\n"
"\n"
"If the document is a binary stream, image of the document will be copied\n"
"into a file. If the document is a XML document, the XML tree will be serialized\n"
"into a file. If XML document stands for one element or one text node, the node\n"
"(and its children recursively) will be serialized. If XML document compounds\n"
"more nodes or a comment or a processing instruction, parent node from ISDS name\n"
"space will be used to ensure output serialized XML well-formness.\n"
),
            command);
}


static int shi_save_document(int argc, const char **argv) {
    const struct isds_document *document;
    const char *dirname = NULL;
    char *filename = NULL, *path = NULL;
    int retval = 0;
    _Bool overwrite = cfg_getbool(configuration, CONFIG_OVERWRITEFILES);

    if (!argv || !argv[1] || !*argv[1] || argc > 3) {
        shi_save_document_usage(argv[0]);
        return -1;
    }

    document = locate_document_by_ordinal_string(argv[1]);
    if (!document) return -1;

    /* Select directory and file name */
    if (argv[2] && *argv[2]) {
        if (!is_directory(argv[2])) {
            dirname = argv[2];
        } else {
            filename = strdup(argv[2]);
            if (!filename) {
                fprintf(stderr, _("Not enough memory\n"));
                return -1;
            }
        }
    }
    if (!filename && document->dmFileDescr && *document->dmFileDescr) {
        filename = utf82locale(document->dmFileDescr);
        if (!filename) {
            fprintf(stderr, _("Not enough memory\n"));
            return -1;
        }
    }
    if (!filename) {
        fprintf(stderr,
                _("File name neither supplied, nor document name exists\n"
                    "Please, supply one.\n"));
        return -1;
    }

    /* Build path */
    if (dirname) {
        path = astrcat3(dirname, "/", filename);
        zfree(filename);
    } else {
        path = filename;
        filename = NULL;
    }
    if (!path) {
        fprintf(stderr, _("Not enough memory\n"));
        return -1;
    }

    /* Save document */ 
    if (document->is_xml)
        retval = save_xml_to_file(path, -1, document->xml_node_list,
                document->dmMimeType, overwrite);
    else
        retval = save_data_to_file(path, -1, document->data,
                document->data_length, document->dmMimeType, overwrite);
    free(path);
    return retval;
}


/* Execute program specified as NULL terminated array of arguments. argv[0] is
 * subject of PATH search variable look-up. The program is executed directly,
 * it's not a shell command. */
static int execute_system_command(char *const argv[]) {
    pid_t pid;

    if (!argv || !argv[0]) return -1;

    pid = fork();
    if (pid == -1) {
        /* Could not fork */
        fprintf(stderr, _("Could not fork\n"));
        return -1;
    } else if (pid == 0) {
        /* Child */
        execvp(argv[0], argv);
        fprintf(stderr, _("Could not execute:"));
        for (char *const *arg = argv; *arg; arg++)
            fprintf(stderr, " %s", *arg);
        fprintf(stderr, _(": %s\n"), strerror(errno));
        exit(EXIT_FAILURE);
    } else {
        /* Wait for the command */
        int retval;

        if (-1 == waitpid(pid, &retval, 0)) {
            fprintf(stderr, _("Could not wait for executed command\n"));
            return -1;
        }

        if (retval == -1)
            fprintf(stderr, _("Exit code of command could not "
                        "be determined\n"));
        else if (WIFEXITED(retval) && WEXITSTATUS(retval))
            printf(_("Command exited with code %d\n"),
                    WEXITSTATUS(retval));
        else if (WIFSIGNALED(retval))
            printf(_("Command terminated by signal "
                        "#%d\n"), WTERMSIG(retval));
        return retval;
    }
}


/* Run editor to create new text document */
static int edit_new_textual_document(struct isds_document *document) {
    char filename[14] = "shiXXXXXX.txt";
    int fd;
    char *command[] = { getenv("VISUAL"), filename, NULL };
    int retval = 0;
    struct stat file_before, file_after;

    if (batch_mode) {
        fprintf(stderr, _("Editing is forbidden in batch mode.\n"));
        return -1;
    }

    if (NULL == document) return -1;
    if (NULL == command[0]) command[0] = getenv("EDITOR");
    if (NULL == command[0]) {
        fprintf(stderr,
                _("Neither environment variable VISUAL nor EDITOR are set.\n"));
        return -1;
    }

    /* Create temporary file for the document */
    fd = create_new_file(filename, 4);
    if (fd == -1) {
        return -1;
    }
    if (fstat(fd, &file_before)) {
        fprintf(stderr,
                _("Could not retrieve modification time for `%s': %s\n"),
                filename, strerror(errno));
        retval = -1;
        goto leave;
    }

    /* Open the file with $EDITOR */
    if ((retval = execute_system_command(command))) {
        fprintf(stderr, _("Editor failed.\n"));
        retval = -1;
        goto leave;
    }

    /* Compare modification times */
    /* XXX: fstat(2) does return updated st_mtime. Bug in Linux 3.7.1? */
    if (stat(filename, &file_after)) {
        fprintf(stderr,
                _("Could not retrieve modification time for `%s': %s\n"),
                filename, strerror(errno));
        retval = -1;
        goto leave;
    }
    if (file_before.st_mtime == file_after.st_mtime) {
        fprintf(stderr, _("Edited document has not been changed.\n"));
        retval = -1;
        goto leave;
    }

    /* Load document */ 
    if (load_data_from_file(filename, &document->data,
            &document->data_length, NULL)) {
        retval = -1;
        goto leave;
    }
    
    /* Set meta-data */
    if (NULL == document->dmMimeType)
        FILL_OR_LEAVE(document->dmMimeType, "text/plain");
    /* XXX: POSIX basename() modifies argument */
    if (NULL == document->dmFileDescr)
        FILL_OR_LEAVE(document->dmFileDescr, basename(filename));

leave:
    /* Remove the file */
    unlink_file(filename);
    close(fd);
    return retval;
}


/* Append @suffix into @buffer with @size bytes prealocated at position @at.
 * Trailing '\0' of @suffix is not carried.
 * @buffer can be reallocated if @size is not suffient to fill @suffix
 * @size is original @buffer size, can change if @buffer would be reallocated
 * @at position where append @suffic to. Outputs new end after appending
 * @suffix is NULL rerminated string to append
 * @return 0 if success, -1 otherwise. Caller is resposible for freeing
 * @buffer.*/
static int append_string_at(char **buffer, size_t *size, char **at,
        const char *suffix) {

    if (!buffer || !*buffer || !size || !at || !*at) return -1;
    if (!suffix) return 0;

    while (*suffix) {
        if (*at - *buffer + 1 >= *size) {
            /* End of buffer, grow it */
            if (*size < 8) *size = 8;
            *size *= 2;
            char *new_buffer = realloc(*buffer, *size);
            if (!new_buffer) return -1;
            *at = *at - *buffer + new_buffer;
            *buffer = new_buffer;
        }

        /* Copy a character */
        *((*at)++) = *(suffix++);
    }

    return 0;
}


static char *expand_command_arg(const char *format, const char *file,
        const char *type) {
    char *buffer = NULL;
    size_t size = 0;
    const char *format_cursor;
    char *buffer_cursor;

    if (!format) return NULL;

    for (format_cursor = format, buffer_cursor = buffer; ; format_cursor++) {
        if (buffer_cursor - buffer + 1 >= size) {
            /* End of buffer, grow it */
            if (size < 8) size = 8;
            size *= 2;
            char *new_buffer = realloc(buffer, size);
            if (!new_buffer) goto error;
            buffer_cursor = buffer_cursor - buffer + new_buffer;
            buffer = new_buffer;
        }

        if (*format_cursor == '%') {
            /* Escape */
            switch (*(format_cursor+1)) {
                case 'f':
                    if (!file) {
                        fprintf(stderr, _("Could not expand `%%f' because "
                                    "file name did not exist.\n"));
                        free(buffer);
                        return NULL;
                    }
                    if (append_string_at(&buffer, &size, &buffer_cursor, file))
                        goto error;

                    format_cursor++;
                    continue;
                case 't':
                    if (!file) {
                        fprintf(stderr, _("Could not expand `%%t' because "
                                    "file type did not exist.\n"));
                        free(buffer);
                        return NULL;
                    }
                    if (append_string_at(&buffer, &size, &buffer_cursor, type))
                        goto error;

                    format_cursor++;
                    continue;
                case '%':
                    format_cursor++;
            }
        }

        /* Copy plain character */
        *(buffer_cursor++) = *format_cursor;

        if (!*format_cursor) break;
    }

    return buffer;

error:
    fprintf(stderr, _("Error: Not enough memory\n"));
    free(buffer);
    return NULL;
}


/* Expand open_command configuration option by substiting %f and %t with file
 * name and file type.
 * @file is locale encoded file name
 * @type is UTF-8 encoded MIME type 
 * @return heap allocated arrary of arguments or NULL if error occurs.
 * Arguments are coded in locale. */
static char **expand_open_command(const char *file, const char *type) {
    char **command = NULL;
    int length;
    char *type_locale = NULL;
    
    length = cfg_size(configuration, CONFIG_OPENCOMMAND);
    if (length <= 0) {
        fprintf(stderr, _("%s not set\n"), CONFIG_OPENCOMMAND);
        return NULL;
    }

    command = malloc((length + 1) * sizeof(*command));
    if (!command) {
        fprintf(stderr, _("Error: Not enough memory\n"));
        return NULL;
    }

    if (type) {
        type_locale = utf82locale(type);
        if (!type_locale) {
            printf(_("Could not convert document MIME type to locale "
                        "encoding\n"));
            free(command);
            return NULL;
        }
    }

    for (int i = 0; i < length; i++) {
        const char *value = cfg_getnstr(configuration, CONFIG_OPENCOMMAND, i);
        command[i] = expand_command_arg(value, file, type_locale);
        if (!command[i]) {
            fprintf(stderr, _("Error: Not enough memory\n"));
            for (int j = 0; j < i; j++) free(command[j]);
            free(command);
            free(type_locale);
            return NULL;
        }
    }
    command[length] = NULL;

    free(type_locale);
    return command;
}


/* Register temporary @file for removal at shigofumi exit.
 * This is done by destructor call-back in isds_list_free(). */
static int register_temporary_file(const char *file) {
    struct isds_list *temporary_file = NULL;

    if (!file) return 0;

    temporary_file = malloc(sizeof(*temporary_file));
    if (!temporary_file) {
        fprintf(stderr, _("Error: Not enough memory\n"));
        return -1;
    }

    temporary_file->data = (void *)strdup(file);
    if (!temporary_file) {
        fprintf(stderr, _("Error: Not enough memory\n"));
        free(temporary_file);
        return -1;
    }

    temporary_file->destructor = shi_unlink_temporary_file;

    if (temporary_files)
        temporary_file->next = temporary_files;
    else
        temporary_file->next = NULL;
    temporary_files = temporary_file;

    return 0;
}


static void shi_open_document_usage(const char *command) {
    oprintf(_(
"Usage: %s NUMBER\n"
"Save document having ordinal NUMBER within current message into temporal\n"
"local file, open the file by xdg-open utility and then remove the file.\n"
),
            command);
}


static int shi_open_document(int argc, const char **argv) {
    const struct isds_document *document;
    char filename[10] = "shiXXXXXX";
    int fd;
    char **command = NULL;
    int retval = 0;

    if (!argv || !argv[1] || !*argv[1] || argc > 3) {
        shi_open_document_usage(argv[0]);
        return -1;
    }

    document = locate_document_by_ordinal_string(argv[1]);
    if (!document) return -1;

    /* Create temporary file for the document */
    fd = create_new_file(filename, 0);
    if (fd == -1) {
        return -1;
    }

    /* Save document */ 
    if (document->is_xml)
        retval = save_xml_to_file(filename, fd, document->xml_node_list,
                document->dmMimeType, 0);
    else
        retval = save_data_to_file(filename, fd, document->data,
                document->data_length, document->dmMimeType, 0);

    /* Open the file with external utility */
    if (!retval) {
        /* Construct command arguments to execute */
        command = expand_open_command(filename, document->dmMimeType);
        
        if (!command)
            retval = -1;
        else { 
            /* XXX: Do not use system(3) as we cannot escape uknown shell */
            retval = execute_system_command(command);
            for (char **arg = command; *arg; arg++) free(*arg);
            free(command);
        }
    }

    /* Remove the file */
    /* XXX: We do not know when external program opens the file. We cannot
     * remove it immediately. Register the filename and remove all temporary
     * files at exit of shigofumi if requested by configuration. */
    if (cfg_getbool(configuration, CONFIG_CLEAN_TEMPORARY_FILES)) {
        if (register_temporary_file(filename))
            fprintf(stderr, _("Warning: Temporary file `%s' could not been "
                        "registered for later removal. Remove the file by "
                        "hand, please.\n"), filename);
    }
    return retval;
}


static void shi_compose_usage(const char *command) {
    oprintf(_(
"Usage: %s OPTION...\n"
"Compose and send a message to recipient defined by his box ID.\n"
"Each option requires an argument (if not stated otherwise):\n"
"  -s *   message subject\n"
"\n"
"Recipient options:\n"
"  -b *   recipient box ID\n"
"  -U     organisation unit name\n"
"  -N     organisation unit number\n"
"  -P     to hands of given person\n"
"\n"
"Sender organisation structure options:\n"
"  -I     publish user's identity\n"
"  -u     unit name\n"
"  -n     unit number\n"
"\n"
"Message identifier options:\n"
"  -r     sender reference number\n"
"  -f     sender file ID\n"
"  -R     recipient reference number\n"
"  -F     recipient file ID\n"
"\n"
"Legal title options:\n"
"  -y     year act has been issued\n"
"  -a     ordinal number of act in a year\n"
"  -e     section of the act\n"
"  -o     paragraph of the act\n"
"  -i     point of the paragraph of the act\n"
"\n"
"Delivery options:\n"
"  -p     personal delivery required\n"
"  -t     allow substitutable delivery\n"
"  -A     non-OVM sender acts as public authority\n"
"  -C     commercial type; accepted values:\n"
"             K  Commercial message paid by sender or sponsor\n"
"             I  Initiatory commercial message offering to pay response\n"
"             O  Commercial response paid by recipient\n"
"             V  Public message paid by government\n"
"         Missing option defaults to K or O (see `commercialsending' command)\n"
"         if sending to non-OVM recipient with enabled commercial receiving,\n"
"         otherwise it defaults to V.\n"
"\n"
"Document options:\n"
"  -d *   read document from local file. If `-' is specified,\n"
"         run text editor.\n"
"  -D     document name (defaults to base local file name)\n"
"  -x     transport subset of the document as a XML.\n"
"         Argument is XPath expression specifying desired node set\n"         
"  -m     override MIME type (guessed on -d)\n"
"  -g     document ID (must be unique per message)\n"
"  -G     reference to other document using its ID\n"
"  -c     document is digital signature of other document (NO argument\n"
"         allowed)\n"
"\n"
"Options marked with asterisk are mandatory, other are optional. Another soft\n"
"dependencies can emerge upon using specific option. They are not mandated by\n"
"ISDS currently, but client library or this program can force them to assure\n"
"semantically complete message. Following soft dependencies are recommended:\n"
"  -y <=> -a   act number and year must be used at the same time\n"
"  -i => -o    act point requires act paragraph\n"
"  -o => -e    act paragraph requires act section\n"
"  -e => -a    act section requires act number\n"
"  -G => -g    document with referenced ID must exist\n"
"  -c => -G    signature must refer to signed document\n"
"  -c          first document cannot be signature\n"
"  -C I => -r  sender reference number allows responder to reply to this message\n"
"  -C O -> -R  recipient reference number must match sender reference number of\n"
"              initiatory message\n"
"\n"
"More documents can be attached to a message by repeating `-d' option.\n"
"Document order will be preserved. Other document options affect immediately\n"
"preceding `-d' document only. E.g. `-d /tmp/foo.pdf -m application/pdf\n"
"-d /tmp/bar.txt -m text/plain' attaches first PDF file, then textual file.\n"
"\n"
"The same applies to recipient options that must start with box ID (-b).\n"
"If more recipients specified, each of them will get a copy of composed\n"
"message. ISDS will assign message identifier to each copy in turn.\n"
),
            command);
}


static int shi_compose(int argc, const char **argv) {
    int opt;
    isds_error err;
    int retval = 0;
    unsigned int i;
    struct isds_message *message = NULL;
    struct isds_envelope *envelope = NULL;
    struct isds_list *documents = NULL;
    struct isds_document *document = NULL;
    struct isds_list *copies = NULL, *copy_item = NULL;
    struct isds_message_copy *copy = NULL;
    char *message_id_locale = NULL, *recipient_id_locale = NULL,
         *dmStatus_locale = NULL;

    if (!argv || !argv[1] || !*argv[1]) {
        fprintf(stderr, _("Error: No argument supplied\n"));
        shi_compose_usage((argv)?argv[0]:NULL);
        return -1;
    }

    message = calloc(1, sizeof(*message));
    if (!message) {
        fprintf(stderr, _("Error: Not enough memory\n"));
        retval = -1;
        goto leave;
    }
    envelope = calloc(1, sizeof(*envelope));
    if (!envelope) {
        fprintf(stderr, _("Error: Not enough memory\n"));
        retval = -1;
        goto leave;
    }
    message->envelope = envelope;

    /* Parse options */
    optind = 0;
    while ((opt = getopt(argc, (char * const *)argv, "s:" "b:U:N:P:" "I:u:n:"
                    "r:f:R:F:" "y:a:e:o:i:" "p:t:A:C:" "d:D:x:m:g:G:c"
                    )) != -1) {
        switch (opt) {
            case 's':
                FILL_OR_LEAVE(envelope->dmAnnotation, optarg);
                break;

            /* Recipient options */
            case 'b':
                copy = NULL;
                if (!copies) {
                    /* First recipient */
                    CALLOC_OR_LEAVE(copies);
                    copies->destructor =
                        (void(*)(void **)) isds_message_copy_free;
                    copy_item = copies;
                } else {
                    /* Next recipient */
                    CALLOC_OR_LEAVE(copy_item->next);
                    copy_item->next->destructor =
                        (void(*)(void **)) isds_message_copy_free;
                    copy_item = copy_item->next;
                }
                CALLOC_OR_LEAVE(copy);
                copy_item->data = copy;

                /* Copy recipient box ID */
                FILL_OR_LEAVE(copy->dbIDRecipient, optarg);
                break;
            case 'U':
                if (!copy) {
                    fprintf(stderr,
                            _("Error: %s: Recipient box ID (-b) must precede "
                                "recipient organisation unit name (-%c)\n"),
                            optarg, opt);
                    retval = -1;
                    goto leave; 
                }
                FILL_OR_LEAVE(copy->dmRecipientOrgUnit, optarg);
                break;
            case 'N':
                if (!copy) {
                    fprintf(stderr,
                            _("Error: %s: Recipient box ID (-b) must precede "
                                "recipient organisation unit number (-%c)\n"),
                            optarg, opt);
                    retval = -1;
                    goto leave; 
                }
                FILL_LONGINT_OR_LEAVE(copy->dmRecipientOrgUnitNum, optarg);
                break;
            case 'P':
                if (!copy) {
                    fprintf(stderr,
                            _("Error: %s: Recipient box ID (-b) must precede "
                                "to-hands option (-%c)\n"), optarg, opt);
                    retval = -1;
                    goto leave; 
                }
                FILL_OR_LEAVE(copy->dmToHands, optarg);
                break;

            /* Sender organisation structure options */
            case 'I':
                FILL_BOOLEAN_OR_LEAVE(envelope->dmPublishOwnID, optarg);
                break;
            case 'u':
                FILL_OR_LEAVE(envelope->dmSenderOrgUnit, optarg);
                break;
            case 'n':
                FILL_LONGINT_OR_LEAVE(envelope->dmSenderOrgUnitNum, optarg);
                break;

            /* Message identifier options */
            case 'r':
                FILL_OR_LEAVE(envelope->dmSenderRefNumber, optarg);
                break;
            case 'f':
                FILL_OR_LEAVE(envelope->dmSenderIdent, optarg);
                break;
            case 'R':
                FILL_OR_LEAVE(envelope->dmRecipientRefNumber, optarg);
                break;
            case 'F':
                FILL_OR_LEAVE(envelope->dmRecipientIdent, optarg);
                break;

            /* Legal title options */
            case 'y':
                FILL_LONGINT_OR_LEAVE(envelope->dmLegalTitleYear, optarg);
                break;
            case 'a':
                FILL_LONGINT_OR_LEAVE(envelope->dmLegalTitleLaw, optarg);
                break;
            case 'e':
                FILL_OR_LEAVE(envelope->dmLegalTitleSect, optarg);
                break;
            case 'o':
                FILL_OR_LEAVE(envelope->dmLegalTitlePar, optarg);
                break;
            case 'i':
                FILL_OR_LEAVE(envelope->dmLegalTitlePoint, optarg);
                break;

            /* Delivery options */
            case 'p':
                FILL_BOOLEAN_OR_LEAVE(envelope->dmPersonalDelivery, optarg);
                break;
            case 't':
                FILL_BOOLEAN_OR_LEAVE(envelope->dmAllowSubstDelivery, optarg);
                break;
            case 'A':
                FILL_BOOLEAN_OR_LEAVE(envelope->dmOVM, optarg);
                break;
            case 'C':
                FILL_OR_LEAVE(envelope->dmType, optarg);
                break;

            /* Document options */
            case 'd':
                document = NULL;
                if (!documents) {
                    /* First document */
                    CALLOC_OR_LEAVE(message->documents);
                    message->documents->destructor =
                        (void(*)(void **)) isds_document_free;
                    documents = message->documents;
                    CALLOC_OR_LEAVE(document);
                    documents->data = document;
                    document->dmFileMetaType = FILEMETATYPE_MAIN;
                } else {
                    /* Next document */
                    CALLOC_OR_LEAVE(documents->next);
                    documents->next->destructor =
                        (void(*)(void **)) isds_document_free;
                    documents = documents->next;
                    CALLOC_OR_LEAVE(document);
                    documents->data = document;
                    document->dmFileMetaType = FILEMETATYPE_ENCLOSURE;
                }

                if (strcmp(optarg, "-")) {
                    /* Load file if specified. Keep editing new file after
                     * processing all arguments. */
                    if (load_data_from_file(optarg, &document->data,
                            &document->data_length, &document->dmMimeType)) {
                        retval = -1;
                        goto leave;
                    }
                    /* XXX: POSIX basename() modifies argument */
                    FILL_OR_LEAVE(document->dmFileDescr, basename(optarg));
                }
                break;
            case 'D':
                if (!document) {
                    fprintf(stderr,
                            _("Error: %s: Document file (-d) must precede "
                                "document name (-%c)\n"), optarg, opt);
                    retval = -1;
                    goto leave; 
                }
                FILL_OR_LEAVE(document->dmFileDescr, optarg);
                break;
            case 'x':
                if (!document) {
                    fprintf(stderr, 
                            _("Error: %s: Document file (-d) must precede "
                                "XPath expression (-%c)\n"), optarg, opt);
                    retval = -1;
                    goto leave; 
                }
                /* Load XML node list */
                char *xpath_expr = NULL;
                FILL_OR_LEAVE(xpath_expr, optarg);
                retval = load_xml_subtree_from_memory(
                        document->data, document->data_length,
                        &document->xml_node_list, xpath_expr);
                if (retval) {
                    free(xpath_expr);
                    goto leave;
                }
                /* Switch document type to XML */
                document->is_xml = 1;
                zfree(document->data);
                document->data_length = 0;
                documents->destructor =
                    (void(*)(void **)) free_document_with_xml_node_list;
                break;
            case 'm':
                if (!document) {
                    fprintf(stderr,
                            _("Error: %s: Document file (-d) must precede "
                                "MIME type (-%c)\n"), optarg, opt);
                    retval = -1;
                    goto leave; 
                }
                FILL_OR_LEAVE(document->dmMimeType, optarg);
                break;
            case 'g':
                if (!document) {
                    fprintf(stderr,
                            _("Error: %s: Document file (-d) must precede "
                                "document ID (-%c)\n"), optarg, opt);
                    retval = -1;
                    goto leave; 
                }
                FILL_OR_LEAVE(document->dmFileGuid, optarg);
                break;
            case 'G':
                if (!document) {
                    fprintf(stderr,
                            _("Error: %s: Document file (-d) must precede "
                                "document reference (-%c)\n"), optarg, opt);
                    retval = -1;
                    goto leave; 
                }
                FILL_OR_LEAVE(document->dmUpFileGuid, optarg);
                break;
            case 'c':
                if (!document) {
                    fprintf(stderr,
                            _("Error: Document file (-d) must precede "
                                "document signature type (-%c)\n"), opt);
                    retval = -1;
                    goto leave; 
                }
                document->dmFileMetaType = FILEMETATYPE_SIGNATURE;
                break;

            default:
                shi_compose_usage(argv[0]);
                retval = -1;
                goto leave;
        }
    }

    /* All options must be recognized */
    if (optind != argc) {
        fprintf(stderr, _("Error: Superfluous argument\n"));
        shi_compose_usage(argv[0]);
        retval = -1;
        goto leave;
    }

    if (!copies) {
        fprintf(stderr, _("Error: No recipient box ID specified\n"));
        shi_compose_usage(argv[0]);
        retval = -1;
        goto leave;
    }
   
    /* TODO: Check Legal Title soft dependencies */

    /* Compose missing documents */
    for (i = 1, documents = message->documents; NULL != documents;
            documents = documents->next, i++) {
        document = documents->data;
        if (!document->is_xml && NULL == document->data) {
            printf(_("Editing document #%u...\n"), i);
            if (edit_new_textual_document(document)) {
                fprintf(stderr, _("Composition aborted.\n"));
                retval = -1;
                goto leave;
            }
        }
    }

    /* Preview */
    oprintf(_("Following message has been composed:\n"));
    format_message(message);
    oprintf(_("Following recipients have been specified:\n"));
    format_copies(copies);
    /* TODO: correction */
    if (cfg_getbool(configuration, CONFIG_CONFIRM_SEND)) {
        if (!shi_ask_yes_no(_("Send composed message?"), 1, batch_mode)) {
            fprintf(stderr, _("Composition aborted.\n"));
            retval = -1;
            goto leave;
        }
    }

    /* Send a message */
    printf(_("Sending message...\n"));
    err = isds_send_message_to_multiple_recipients(cisds, message, copies);
    finish_isds_operation(cisds, err);
    if (err && err != IE_PARTIAL_SUCCESS) {
        retval = -1;
        goto leave;
    }

    /* Show results for each copy */
    for (copy_item = copies; copy_item; copy_item = copy_item->next) {
        if (!copy_item->data) continue;
        copy = (struct isds_message_copy *) copy_item->data;
        recipient_id_locale = utf82locale(copy->dbIDRecipient);

        if (copy->error) {
            retval = -1;
            if (copy->dmStatus) dmStatus_locale = utf82locale(copy->dmStatus);
            if (dmStatus_locale)
                oprintf(_("%s: Failed: %s: %s\n"),
                        recipient_id_locale,
                        isds_strerror(copy->error), dmStatus_locale);
            else
                oprintf(_("%s: Failed: %s\n"), recipient_id_locale,
                        isds_strerror(copy->error));
            zfree(dmStatus_locale);
        } else {
            message_id_locale = utf82locale(copy->dmID);
            oprintf(_("%s: Succeeded. Assigned message ID: %s\n"),
                    recipient_id_locale, message_id_locale);
            free(message_id_locale);
        }

        free(recipient_id_locale);
    }

leave:
    isds_message_free(&message);
    isds_list_free(&copies);
    return retval;
}


#undef FILL_LONGINT_OR_LEAVE
#undef FILL_BOOLEAN_OR_LEAVE
#undef CALLOC_OR_LEAVE
#undef FILL_OR_LEAVE


static void shi_save_stamp_usage(const char *command) {
    oprintf(_(
                "Usage: %s FILE\n"
                "Save message time stamp into local FILE.\n"),
            command);
}


static int shi_save_stamp(int argc, const char **argv) {
    _Bool overwrite = cfg_getbool(configuration, CONFIG_OVERWRITEFILES);

    if (!argv || !argv[1] || !*argv[1]) {
        shi_save_message_usage(argv[0]);
        return -1;
    }

    if (!message) {
        fprintf(stderr, _("No message loaded\n"));
        return -1;
    }
    if (!message->envelope || !message->envelope->timestamp||
            message->envelope->timestamp_length == 0) {
        fprintf(stderr, _("Loaded message is missing time stamp\n"));
        return -1;
    }

    return save_data_to_file(argv[1], -1,
            message->envelope->timestamp, message->envelope->timestamp_length,
            "application/timestamp-reply", overwrite);
}


/* Return message of current message list identified by message ID expressed
 * as string. In case of error return NULL. */
static const struct isds_message *locate_message_by_id(const char *id_locale) {
    char *id = NULL;
    const struct isds_list *item;
    const struct isds_message *message = NULL;

    if (NULL == id_locale) return NULL;

    id = locale2utf8(id_locale);
    if (id == NULL) {
        fprintf(stderr, _("Could not convert message ID `%s' to UTF-8\n"),
                id_locale);
        return NULL;
    }

    if (NULL == messages) {
        fprintf(stderr, _("No message list loaded\n"));
        return NULL;
    }

    /* Find message */
    for (item = messages; NULL != item; item = item->next) {
        if (NULL == item->data) continue;
        message = (const struct isds_message *) item->data;
        if (NULL == message->envelope || NULL == message->envelope->dmID)
            continue;
        if (!strcmp(id, message->envelope->dmID)) {
            break;
        }
    }
    if (NULL == item) {
        fprintf(stderr, _("List does not contain message `%s'\n"), id_locale);
        return NULL;
    }

    return message;
}


static void shi_show_list_usage(const char *command) {
    oprintf(_(
"Usage: %s [MESSAGE_ID]\n"
"If no MESSAGE_ID is given, show current message list.\n"
"If MESSAGE_ID is specified, show details about message with the MESSAGE_ID\n"
"on the current message list.\n"),
            command);
}


static int shi_show_list(int argc, const char **argv) {
    if (NULL == messages) {
        fprintf(stderr, _("No message list loaded\n"));
        return -1;
    }

    if (argc > 2 || (2 == argc && NULL == argv[1])) {
        shi_show_list_usage((argv[0] == NULL) ? NULL : argv[0]);
        return -1;
    }
    
    if (argc == 2) {
        /* Show details about one message */
        const struct isds_message *message = locate_message_by_id(argv[1]);
        if (NULL == message) return -1;
        format_message(message);
        return 0;
    }

    /* Show all list */
    oprintf((messages_are_outgoing) ?
            ngettext("You have %'lu outgoing message\n",
                "You have %'lu outgoing messages\n", total_messages) :
            ngettext("You have %'lu incoming message\n",
                "You have %'lu incoming messages\n", total_messages),
            total_messages);
    print_message_list(messages, messages_are_outgoing);
    return 0;
}


static void describe_message_listing_flags(void) {
    oprintf(_(
"The third column displays flags describing status of a message:\n"
"   First character is a commercial type of the message:\n"
"       P   public non-commercial message\n"
"       C   commercial message\n"
"       I   commercial message offering a paid response\n"
"       i   like I, but the offer has expired\n"
"       R   commerical message as a reply to I\n"
"   The second character is a delivery status of the message:\n"
"       >   message has been sent into the system\n"
"       t   message has been time-stamped by the system\n"
"       I   message contained an infected document\n"
"       N   message has been delivered ordinaly\n"
"       n   message has been delivered substitutingly\n"
"       O   message has been accepted by the recipient\n"
"      \" \"  message has been read\n"
"       !   message could not been delivered\n"
"       D   message content has been deleted\n"
"       S   message has been stored in the long term storage\n"
"       ?   unrecognized state\n"));
}


static void shi_list_incoming_usage(const char *command) {
    oprintf(_(
"Usage: %s\n"
"List messages received into your box.\n"
"\n"),
            command);
    describe_message_listing_flags();
}


static int shi_list_incoming(int argc, const char **argv) {
    unsigned long int count = 0;
    isds_error err;

    printf(_("Listing incoming messages...\n"));
    err = isds_get_list_of_received_messages(cisds, NULL, NULL, NULL,
            MESSAGESTATE_ANY, 0, &count, &messages);
    finish_isds_operation(cisds, err);
    total_messages = count;
    if (err) {
        set_prompt(NULL);
        select_completion(COMPL_COMMAND);
        return -1;
    }
    messages_are_outgoing = 0;

    shi_show_list(0, NULL);

    set_prompt(_("%s %'lu"), argv[0], total_messages);
    select_completion(COMPL_LIST);
    return 0;
}


static void shi_list_outgoing_usage(const char *command) {
    oprintf(_(
"Usage: %s\n"
"List messages sent from your box.\n"
"\n"),
            command);
    describe_message_listing_flags();
}


static int shi_list_outgoing(int argc, const char **argv) {
    unsigned long int count = 0;
    isds_error err;

    printf(_("Listing outgoing messages...\n"));
    err = isds_get_list_of_sent_messages(cisds, NULL, NULL, NULL,
            MESSAGESTATE_ANY, 0, &count, &messages);
    finish_isds_operation(cisds, err);
    total_messages = count;
    if (err) {
        set_prompt(NULL);
        select_completion(COMPL_COMMAND);
        return -1;
    }
    messages_are_outgoing = 1;

    shi_show_list(0, NULL);

    set_prompt(_("%s %'lu"), argv[0], total_messages);
    select_completion(COMPL_LIST);
    return 0;
}


/* Submit document for conversion and print assigned identifier */
static int do_convert(const struct isds_document *document) {
    isds_error err;
    char *id = NULL;
    struct tm *date = NULL;

    if (!document) return -1;

    printf(_("Submitting document for authorized conversion...\n"));

    err = czp_convert_document(czechpoint, document, &id, &date);
    finish_isds_operation(czechpoint, err);

    if (!err) {
        char *name_locale = utf82locale(document->dmFileDescr);
        char *date_string = tm2string(date);
        char *id_locale = utf82locale(id);
        oprintf(_(
"Document submitted for authorized conversion successfully under name\n"
"`%s' on %s.\n"
"Submit identifier assigned by Czech POINT deposit is `%s'.\n"),
                name_locale, date_string, id_locale);
        free(name_locale);
        free(date_string);
        free(id_locale);
        oprintf(_("Be ware that submitted document has restricted lifetime "
                    "(30 days).\n"));
        oprintf(_("See <%s> for more details.\n"), CZPDEPOSIT_URL);
    }

    free(id); free(date);
    return (err) ? -1 : 0;
}


static void shi_convert_file_or_message_usage(const char *command) {
    oprintf(_(
"Usage: %s [FILE [NAME]]\n"
"Submit local FILE to authorized conversion under NAME. If NAME is missing,\n"
"it will use FILE name. If FILE is missing, it will submit current message.\n"),
            command);
    oprintf(_(
"\n"
"If Czech POINT deposit accepts document, it will return document identifier\n"
"that user is supposed to provide to officer at Czech POINT contact place.\n"
"Currently only PDF 1.3 and higher version files and signed messages are\n"
"accepted.\n"));
    oprintf(_("See <%s> for more details.\n"), CZPDEPOSIT_URL);
}


static int shi_convert_file_or_message(int argc, const char **argv) {
    int fd = -1;
    struct isds_document document;
    int retval = 0;

    if (!argv || argc > 3) {
        shi_convert_file_or_message_usage((argv)?argv[0]:NULL);
        return -1;
    }

    memset(&document, 0, sizeof(document));

    if (NULL == argv[1] || !*argv[1]) {
        /* Convert current message */
        if (!message) {
            fprintf(stderr, _("No message loaded\n"));
            return -1;
        }
        if (!message->raw || !message->raw_length) {
            fprintf(stderr,
                    _("Current message is missing raw representation\n"));
            return -1;
        }
        document.dmFileDescr = astrcat(
                (NULL != message->envelope && NULL != message->envelope->dmID) ?
                    message->envelope->dmID :
                    "unknown",
                ".zfo");
        if (NULL == document.dmFileDescr) {
            printf(_("Could not build document name from message ID\n"));
            return -1;
        }
        document.data = message->raw;
        document.data_length = message->raw_length;
    } else {
        /* Convert local file */
        if (argc == 3 && argv[2] && *argv[2])
            document.dmFileDescr = locale2utf8(argv[2]);
        else
            document.dmFileDescr = locale2utf8(argv[1]);
        if (!document.dmFileDescr) {
            printf(_("Could not convert document name to UTF-8\n"));
            return -1;
        }

        printf(_("Loading document from file `%s'...\n"), argv[1]);
        if (mmap_file(argv[1], &fd, &document.data, &document.data_length)) {
            free(document.dmFileDescr);
            return -1;
        }
    }

    retval = do_convert(&document);

    if (0 <= fd) { 
        munmap_file(fd, document.data, document.data_length);
    }
    free(document.dmFileDescr);
    return retval;
}


static void shi_convert_document_usage(const char *command) {
    oprintf(_(
"Usage: %s NUMBER\n"
"Submit message document with ordinal NUMBER to authorized conversion.\n"),
            command);
    oprintf(_(
"\n"
"If Czech POINT deposit accepts document, it will return document identifier\n"
"that user is supposed to provide to officer at Czech POINT contact place.\n"
"Currently only PDF 1.3 and higher version files and signed messages are\n"
"accepted.\n"));
    oprintf(_("See <%s> for more details.\n"), CZPDEPOSIT_URL);
}


static int shi_convert_document(int argc, const char **argv) {
    const struct isds_document *document;

    if (!argv || !argv[1] || !*argv[1]) {
        shi_convert_document_usage((argv)?argv[0]:NULL);
        return -1;
    }

    document = locate_document_by_ordinal_string(argv[1]);
    if (!document) return -1;

    return do_convert(document);
}


static void shi_resign_usage(const char *command) {
    oprintf(_(
"Usage: %s [FILE]\n"
"Send message or delivery data to re-sign it and to add current time stamp.\n"
"If FILE is specified, message will be loaded from local file. Otherwise\n"
"current message will be sent.\n"),
            command);
    oprintf(_(
"\n"
"Only signed messages or delivery data without time stamp are accepted by\n"
"ISDS. Output re-signed message or delivery data will be loaded.\n"));
}


static int shi_resign(int argc, const char **argv) {
    int fd = -1;
    void *data = NULL; /* Static */
    void *resigned_data = NULL; /* Dynamic, stored into message */
    size_t data_length = 0, resigned_data_length = 0;
    struct tm *valid_to = NULL; /* Dynamic */
    isds_error err;
    int error;

    if (!argv || argc > 3) {
        shi_resign_usage((argv)?argv[0]:NULL);
        return -1;
    }

    if (NULL == argv[1] || !*argv[1]) {
        /* Use current message */
        if (!message) {
            fprintf(stderr, _("No message or delivery data loaded\n"));
            return -1;
        }
        if (!message->raw || !message->raw_length) {
            fprintf(stderr, _("Current message or delivery data "
                        "is missing raw representation\n"));
            return -1;
        }
        data = message->raw;
        data_length = message->raw_length;
    } else {
        /* Use local file */
        printf(_("Loading message or delivery data from file `%s'...\n"),
                argv[1]);
        if (mmap_file(argv[1], &fd, &data, &data_length)) {
            return -1;
        }
    }

    printf(_("Re-signing...\n"));
    err = isds_resign_message(cisds, data, data_length,
            &resigned_data, &resigned_data_length, &valid_to);
    finish_isds_operation(cisds, err);

    if (0 <= fd) { 
        munmap_file(fd, data, data_length);
    }

    if (err) {
        return -1;
    }

    print_header_tm(_("New time stamp expires"), valid_to);
    free(valid_to);

    error = do_load_anything(resigned_data, resigned_data_length, BUFFER_MOVE);
    if (error) free(resigned_data);

    return error;
}


#if ENABLE_DEBUG
static void shi_print_usage(const char *command) {
    oprintf(_(
"Usage: %s STRING LENGTH\n"
"Prints STRING into LENGTH columns. Negative LENGTH means not to cut\n"
"overflowing string.\n"
"This should be locale and terminal agnostic.\n"),
            command);
}


static int shi_print(int argc, const char **argv) {
    long int width;

    if (!argv || !argv[1] || !argv[2]) {
        shi_print_usage((argv)?argv[0]:NULL);
        return -1;
    }

    width = strtol(argv[2], NULL, 10);
    if (width < INT_MIN) {
        fprintf(stderr,
            _("Length argument must not lesser than %d.\n"), INT_MIN);
        return -1;
    }
    if (width > INT_MAX) {
        fprintf(stderr,
            _("Length argument must not be greater than %d.\n"), INT_MAX);
        return -1;
    }

    oprintf(_(">"));
    onprint(argv[1], width);
    oprintf(_("<\n"));

    return 0;
}


static int shi_tokenize(int argc, const char **argv) {

    if (!argv) return 0;

    for (int i = 0; i < argc; i++) {
        oprintf(_(">%s<\n"), argv[i]);
    }
    return 0;
}


static int shi_quote(int argc, const char **argv) {
    char *escaped, *unescaped;

    if (!argv) return 0;

    oprintf(_("Original\tQuoted\tDequoted\n"));
    for (int i = 0; i < argc; i++) {
        escaped = shi_quote_filename((char *) argv[i], 0, NULL);
        unescaped = shi_dequote_filename((char *) argv[i], 0);
        oprintf(_(">%s<\t>%s<\t>%s<\n"), argv[i], escaped, unescaped);
        free(escaped);
        free(unescaped);
    }
    return 0;
}


static int shi_ask_yesno(int argc, const char **argv) {
    _Bool answer;

    answer = shi_ask_yes_no(argv[1], 1, batch_mode);
    oprintf(_("Answer is %s.\n"), (answer) ? _("Yes") : _("No"));

    return 0;
}


#endif


/* pclose(pipe), restore ouput to stdout, show error return code */
int wait_for_shell(FILE **pipe) {
    int retval = 0;

    if (pipe && *pipe) {
        retval = pclose(*pipe);
        *pipe = NULL;
        output = stdout;

        if (retval == -1)
            fprintf(stderr, _("Exit code of shell command could not "
                        "be determined\n"));
        else if (WIFEXITED(retval) && WEXITSTATUS(retval))
            printf(_("Shell command exited with code %d\n"),
                    WEXITSTATUS(retval));
        else if (WIFSIGNALED(retval))
            printf(_("Shell command terminated by signal "
                        "#%d\n"), WTERMSIG(retval));
    }
    
    return retval;
}


/* Interactive loop */
void shi_loop(void) {
    char *command_line = NULL;
    char **command_argv = NULL;
    int command_argc;
    char *shell = NULL;
    struct command *command = NULL;
    FILE *pipe = NULL;
    int failed = 0;

    oprintf(_("Use `help' command to get list of available commands.\n"));

    select_completion(COMPL_COMMAND);
    set_prompt(NULL);

    while (1) {
        command_line = readline((prompt) ? prompt : _("shigofumi> "));
        /* Remember not parsable commands too to user be able to get back to
         * fix command */
        if (command_line && *command_line) {
            /* TODO: Omit blank lines */
            add_history(command_line);
        }

        command_argv = tokenize(command_line, &command_argc, &shell);

        if (command_argv && command_argv[0]) {
            command = find_command(command_argv[0]);

            if (!command) {
                fprintf(stderr, _("Command not understood\n"));
            } else {
                failed = 0;
                if (shell) {
                    fflush(stdout);
                    pipe = popen(shell, "w");
                    if (!pipe) {
                        fprintf(stderr, _("Could not run shell command `%s':"
                                    " %s\n"), shell, strerror(errno));
                        failed = 1;
                    }
                    output = pipe;
                }
                if (!failed) {
                    command->function(command_argc,
                            (const char **) command_argv);
                    wait_for_shell(&pipe);
                }
            }
        }

        argv_free(command_argv);
        zfree(shell);
        zfree(command_line);
    }
}


/* Non-interactive mode. Commands from @lines are processed until any command
 * lines remains or no error occurred. First failure terminates processing.
 * @lines is sequence of commands separated by '\n' or '\r'. The content is
 * modified during this call.
 * @return 0 if all command succeed, otherwise non-zero value
 */
int shi_batch(char *lines) {
    char *command_line;
    char **command_argv = NULL;
    int command_argc;
    char *shell = NULL;
    struct command *command = NULL;
    int retval = 0;
    FILE *pipe = NULL;
    int failed = 0;

    oprintf(_("Batch mode started.\n"));
    batch_mode = 1;
    select_completion(COMPL_COMMAND);

    while (!retval && (command_line = strtok(lines, "\n\r"))) {
        lines = NULL; /* strtok(3) requires it for subsequent calls */

        printf(_("Processing command: %s\n"), command_line);

        command_argv = tokenize(command_line, &command_argc, &shell);

        if (command_argv && command_argv[0]) {
            command = find_command(command_argv[0]);

            if (!command) {
                fprintf(stderr, _("Command not understood\n"));
                retval = -1;
            } else {
                failed = 0;
                if (shell) {
                    fflush(stdout);
                    pipe = popen(shell, "w");
                    if (!pipe) {
                        fprintf(stderr, _("Could not run shell command `%s':"
                                    " %s\n"), shell, strerror(errno));
                        failed = 1;
                    }
                    output = pipe;
                }
                if (!failed) {
                    retval = command->function(command_argc,
                            (const char **) command_argv);
                    if (wait_for_shell(&pipe)) retval = -1;
                }
            }
        }

        argv_free(command_argv);
        zfree(shell);
    }

    if (retval)
        fprintf(stderr, _("Command failed!\n"));
    return retval;
}


#define COMMON_COMMANDS \
    { "accept", shi_accept_message, N_("accept commercial message"), \
        shi_accept_message_usage, ARGTYPE_MSGID }, \
    { "box", shi_box, N_("show current box details"), NULL, \
        ARGTYPE_NONE }, \
    { "boxlist", shi_boxlist, N_("get list of all boxes"), shi_boxlist_usage, \
        ARGTYPE_FILE }, \
    { "cache", shi_cache, N_("show cache details"), NULL, \
        ARGTYPE_NONE }, \
    { "cd", shi_chdir, N_("change working directory"), shi_chdir_usage, \
        ARGTYPE_FILE }, \
    { "commercialcredit", shi_commercialcredit, N_("get credit details"), \
        shi_commercialcredit_usage, ARGTYPE_BOXID }, \
    { "commercialreceiving", shi_commercialreceiving, \
        N_("manipulate commercial receiving box status"), \
        shi_commercialreceiving_usage, ARGTYPE_BOXID }, \
    { "commercialsending", shi_commercialsending, \
        N_("manipulate commercial sending box status"), \
        shi_commercialsending_usage, ARGTYPE_BOXID }, \
    { "compose", shi_compose, N_("compose a message"), shi_compose_usage, \
        ARGTYPE_FILE }, \
    { "convert", shi_convert_file_or_message, \
        N_("submit local document for authorized conversion"), \
        shi_convert_file_or_message_usage, ARGTYPE_FILE }, \
    { "copying", shi_copying, N_("show this program licence excerpt"), NULL, \
        ARGTYPE_NONE }, \
    { "debug", shi_debug, N_("set debugging"), shi_debug_usage, \
        ARGTYPE_FILE }, \
    { "delete", shi_delete_message, N_("delete message from storage"), \
        shi_delete_message_usage, ARGTYPE_MSGID }, \
    { "delivery", shi_delivery, N_("get message delivery details"), \
        shi_delivery_usage, ARGTYPE_MSGID }, \
    { "findbox", shi_find_box, N_("search for a box by attributes"), \
        shi_find_box_usage, ARGTYPE_BOXID }, \
    { "hash", shi_hash, N_("query ISDS for message hash"), \
        shi_hash_usage, ARGTYPE_MSGID }, \
    { "help", shi_help, N_("describe commands"), shi_help_usage, \
        ARGTYPE_COMMAND }, \
    { "load", shi_load_anything, \
        N_("load message or message delivery details from local file"), \
        shi_load_anything_usage, ARGTYPE_FILE }, \
    { "login", shi_login, N_("log into ISDS"), shi_login_usage, \
        ARGTYPE_FILE }, \
    { "lsi", shi_list_incoming, N_("list received messages"), \
        shi_list_incoming_usage, ARGTYPE_NONE }, \
    { "lso", shi_list_outgoing, N_("list sent messages"), \
        shi_list_outgoing_usage, ARGTYPE_NONE }, \
    { "msgi", shi_incoming_message, N_("get incoming message"), \
        shi_incoming_message_usage, ARGTYPE_MSGID }, \
    { "msgo", shi_outgoing_message, N_("get outgoing message"), \
        shi_outgoing_message_usage, ARGTYPE_MSGID }, \
    { "passwd", shi_passwd, N_("manipulate user password"), shi_passwd_usage, \
        ARGTYPE_NONE }, \
    { "pwd", shi_pwd, N_("print working directory"), NULL, ARGTYPE_NONE }, \
    { "quit", shi_quit, N_("exit shigofumi"), NULL, ARGTYPE_NONE }, \
    { "read", shi_read_message, N_("mark message as read"), \
        shi_read_message_usage, ARGTYPE_MSGID }, \
    { "resign", shi_resign, N_("re-sign message or delivery data"), \
        shi_resign_usage, ARGTYPE_FILE }, \
    { "searchbox", shi_search_box, N_("search for a box by a full-text"), \
        shi_search_box_usage, ARGTYPE_BOXID }, \
    { "set", shi_settings, N_("show settings"), NULL, ARGTYPE_NONE }, \
    { "sender", shi_message_sender, N_("get message sender"), \
        shi_message_sender_usage, ARGTYPE_MSGID }, \
    { "statbox", shi_stat_box, N_("get status of a box"), shi_stat_box_usage, \
        ARGTYPE_BOXID }, \
    { "user", shi_user, N_("show current user details"), NULL, \
        ARGTYPE_NONE }, \
    { "users", shi_users, N_("show box users"), shi_users_usage, \
        ARGTYPE_NONE }, \
    { "version", shi_version, N_("show version of this program"), NULL, \
        ARGTYPE_NONE}, \
    { NULL, NULL, NULL, NULL, ARGTYPE_NONE }

struct command base_commands[] = {
#if ENABLE_DEBUG
    { "askyesno", shi_ask_yesno, N_("demonstrate yes-no question"), NULL,
        ARGTYPE_NONE },
    { "quote", shi_quote, N_("demonstrate argument escaping"), NULL,
        ARGTYPE_FILE },
    { "print", shi_print, N_("print string into given width"),
        shi_print_usage,  ARGTYPE_NONE },
    { "tokenize", shi_tokenize, N_("demonstrate arguments tokenization"), NULL,
        ARGTYPE_FILE },
#endif
    COMMON_COMMANDS
};

struct command message_commands[] = {
    { "authenticate", shi_authenticate, N_("check message authenticity"),
        NULL, ARGTYPE_NONE },
    { "cat", shi_cat_message, N_("show raw current message"),
        shi_cat_message_usage, ARGTYPE_NONE },
    { "catdoc", shi_cat_document, N_("show raw document"),
        shi_cat_document_usage, ARGTYPE_DOCID },
    { "convertdoc", shi_convert_document,
        N_("submit document of current message for authorized conversion"),
        shi_convert_document_usage, ARGTYPE_DOCID },
    { "dump", shi_dump_message, N_("dump current message structure"),
        NULL, ARGTYPE_NONE },
    { "opendoc", shi_open_document, N_("open document using external utility"),
        shi_open_document_usage, ARGTYPE_DOCID },
    { "savestamp", shi_save_stamp,
        N_("save time stamp of current message into local file"),
        shi_save_stamp_usage, ARGTYPE_FILE },
    { "savedoc", shi_save_document,
        N_("save document of current message into local file"),
        shi_save_document_usage, ARGTYPE_FILE },
    { "save", shi_save_message, N_("save current message into local file"),
        shi_save_message_usage, ARGTYPE_FILE },
    { "show", shi_show_message, N_("show current message"), NULL, 
        ARGTYPE_NONE },
    { "verify", shi_verify, N_("verify current message hash"), NULL,
        ARGTYPE_NONE },
    COMMON_COMMANDS
};

struct command list_commands[] = {
    { "show", shi_show_list, N_("show current message list or list item details"),
        shi_show_list_usage, ARGTYPE_MSGID },
    COMMON_COMMANDS
};

#undef COMMON_COMMANDS


static void main_version(void) {
    isds_init();
    show_version();
    isds_cleanup();
    printf("\n");
    shi_copying(0, NULL);
}


static void main_usage(const char *command) {
    oprintf(_(
"Usage: %s [OPTION...]\n"
"Access ISDS, process local data box messages or delivery details, submit\n"
"document to authorized conversion.\n"
"\n"
"Options:\n"
"  -c FILE      use the FILE as configuration file instead of ~/%s\n"
"  -e COMMANDS  execute COMMANDS (new line separated) and exit\n"
"  -V           show version info and exit\n"
        ),
            (command) ? command : "shigofumi",
            CONFIG_FILE);
}


int main(int argc, char **argv) {
    int opt;
    char *config_file = NULL;
    char *batch_commands = NULL;
    int retval = EXIT_SUCCESS;
    
    setlocale(LC_ALL, "");
#if ENABLE_NLS
    /* Initialize gettext */
    bindtextdomain(PACKAGE, LOCALEDIR);
    textdomain(PACKAGE);
#endif

    /* Default output */
    output = stdout;

    /* Parse arguments */
    optind = 0;
    while ((opt = getopt(argc, (char * const *)argv, "c:e:V")) != -1) {
        switch (opt) {
            case 'c':
                config_file = optarg;
                break;
            case 'e':
                batch_commands = optarg;
                break;
            case 'V':
                main_version();
                shi_exit(EXIT_SUCCESS);
                break;
            default:
                main_usage((argv[0]) ? basename(argv[0]): NULL);
                shi_exit(EXIT_FAILURE);
        }
    }


    if (shi_init(config_file)) {
        shi_exit(EXIT_FAILURE);
    }

    /*shi_login(NULL);*/

    if (batch_commands) {
        if (shi_batch(batch_commands))
            retval = EXIT_FAILURE;
    } else {
        shi_loop();
    }

    shi_exit(retval);
}

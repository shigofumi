#ifndef __SHIGOFUMI_H__
#define __SHIGOFUMI_H__

#include "config.h"
#include "utils.h"
#include <locale.h> /* Required for pgettext() from "gettext.h" */
#include "gettext.h"
#define _(x) gettext(x)
#define N_(x) (x)

/*typedef enum {
    STATE_MAIN,
    STATE_MSG
} shi_state;

extern shi_state state;*/

/* UI */
extern FILE *output;

/* Data */
extern struct isds_list *boxes;
extern struct isds_message *message;
extern _Bool messages_are_outgoing;
extern struct isds_list *messages;
extern unsigned long int total_messages;

#endif

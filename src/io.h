#ifndef __IO_H__
#define __IO_H__
#include <libxml/tree.h>    /* For xmlDoc and xmlNodePtr */
#include <libxml/xpath.h>

/* Open file and return descriptor. Return -1 in case of error. */
int open_file_for_writing(const char *file, _Bool truncate, _Bool overwrite);

/* Create new file.
 * @file is static template for file name. See mkstemps(3). It will contain
 * new file name on return.
 * @suffix_length is number of bytes of immutable file name suffix.
 * @return descriptor of opened file, or -1 in case of error. */
int create_new_file(char *file, int suffix_length);

/* Return 0, -1 in case of error */
int mmap_file(const char *file, int *fd, void **buffer, size_t *length);

/* Return 0, -1 in case of error */
int munmap_file(int fd, void *buffer, size_t length);

/* Return 0, -1 in case of error.
 * @length and @mime_type are optional. */
int load_data_from_file(const char *file, void **data, size_t *length,
        char **mime_type);

/* Save @data to file specified by descriptor @fd. If @fd is negative, @file
 * file will be opened first. Descriptor is closed at the end of this
 * function. Supply @file name even if @fd is positive, the name could be used
 * in messages. */
int save_data_to_file(const char *file, int fd, const void *data,
        const size_t length, const char *mime_type, _Bool overwrite);

/* Parse @buffer as XML document and return @node_list specified by
 * @xpath_expression.
 * @node_list is weak copy that must be non-recursively freed by caller. Caller
 * must free XML document (accessible through @node_list member) on its own.
 * In case of error @node_list value will be invalid.
 * If @node_list is returned empty, function freed parsed document already.
 * @xpat_expr is UTF-8 encoded XPath expression */ 
int load_xml_subtree_from_memory(const void *buffer, size_t length,
        xmlNodePtr *node_list, const char *xpath_expr);

/* Deallocate struct isds_document with embedded XML recursively and NULL it */
void free_document_with_xml_node_list(struct isds_document **document);

/* Serialize XML @node_list to automatically rellaocated libxml @buffer */
int serialize_xml_to_buffer(xmlBufferPtr *buffer, const xmlNodePtr node_list);

/* Save @node_list to file specified by descriptor @fd. If @fd is negative, @file
 * file will be opened first. Descriptor is closed at the end of this
 * function. Supply @file name even if @fd is positive, the name could be used
 * in messages. */
int save_xml_to_file(const char *file, int fd, const xmlNodePtr node_list,
        const char *mime_type, _Bool overwrite);

/* Return 0 if @path is directory, 1 if not, -1 if error occurred */
int is_directory(const char *path);

/* Remove a file.
 * @file to remove
 * @return 0 on success, non-zero otherwise. */
int unlink_file(const char *file);
#endif

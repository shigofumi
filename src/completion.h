#ifndef  __SHI_COMPLETITION_H__
#define __SHI_COMPLETITION_H__

typedef enum {
    ARGTYPE_NONE,
    ARGTYPE_COMMAND,
    ARGTYPE_FILE,
    ARGTYPE_MSGID,
    ARGTYPE_DOCID,
    ARGTYPE_BOXID
} arg_type;

struct command {
    char *name;                             /* Full command name */
    int (*function)(int, const char **);    /* Command implementation */
    char *description;                      /* Help text for the command */
    void (*usage)(const char*);             /* Manual for the command */
    arg_type arg;                           /* Defines argument completion */
};

extern struct command base_commands[], message_commands[], list_commands[];
extern struct command (*commands)[];
extern char *prompt;

typedef enum {
    COMPL_NONE,
    COMPL_COMMAND,
    COMPL_MSG,
    COMPL_LIST
} completion_type;

/* Switch completion function */
int select_completion(const completion_type completion);

struct command *find_command(const char *text);

/* Free list of chars recursively */
void argv_free(char **argv);

/* Decides whether character at @index offset of @text is quoted */
int shi_char_is_quoted(char *text, int index);

/* Escapes file name */
char *shi_quote_filename(char *text, int match_type, char *quote_pointer);

/* Deescapes file name */
char *shi_dequote_filename(char *text, int quote_char);


/* Split string into tokens
 * @command_line is line to parse
 * @argc outputs number of parsed arguments
 * @shell is optional automatically reallocated shell command following pipe
 * in @command_line
 * @return NULL-terminated array of tokens or NULL in case of error */
char **tokenize(const char *command_line, int *argc, char **shell);

/* Add line into history if not NULL or empty string */
void shi_add_history(const char *line);

/* Ask user for a password */
char *ask_for_password(const char *prompt);

/* Prompt user and supply default value if user does input nothing. Original
 * default value can be deallocated in this function. If *@value is NULL, use
 * as default read-only @backup_value. You can always free *@value. * */
void shi_ask_for_string(char **value, const char *prompt,
        const char *backup_value, _Bool batch_mode);

/* Prompt user for password and supply default value if user does input
 * nothing. Original default value can be deallocated in this function. If
 * *@value is NULL, use as default read-only @backup_value. You can always
 * free *@value. * */
void shi_ask_for_password(char **value, const char *prompt,
        const char *backup_value, _Bool batch_mode);

/* Ask Yes-No question.
 * @prompt is question to ask the user
 * @default_value specifies default answer if user puts nothing. True is for
 * Yes, false is for No.
 * @batch_mode is true for non-interctive mode, true for interactive
 * @return true for yes, false for no. */
_Bool shi_ask_yes_no(const char *prompt, _Bool default_value,
        _Bool batch_mode);

/* This is ISDS context network progress call back. It prints progress meter
 * and allows user to abort current network transfer. */
int shi_progressbar(double upload_total, double upload_current,
        double download_total, double download_current, void *data);

/* Finish progress meter output. */
void shi_progressbar_finish(void);

#endif

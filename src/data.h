#ifndef __DATA_H__
#define __DATA_H__

#include <isds.h>

void print_bool(const _Bool *boolean);
void print_longint(const long int *number);
void print_DbState(const long int state);
void print_date(const struct tm *date);
void print_DbOwnerInfo(const struct isds_DbOwnerInfo *info);
void print_DbUserInfo(const struct isds_DbUserInfo *info);
void print_timeval(const struct timeval *time);
void print_hash(const struct isds_hash *hash);
void print_envelope(const struct isds_envelope *envelope);
void print_message(const struct isds_message *message);
void print_copies(const struct isds_list *copies);

void compare_hashes(const struct isds_hash *hash1,
        const struct isds_hash *hash2);
int progressbar(double upload_total, double upload_current,
    double download_total, double download_current,
    void *data);

char *hash2string(const struct isds_hash *hash);
char *tm2string(const struct tm *date);

/* Convert string representation of full ISO 8601 or locale date to tm structure.
 * Return NULL if error occurs
 * XXX: Not all ISO formats are supported */
struct tm *datestring2tm(const char *string);

/* Convert string representation of box type to isds_DbType. Return false
 * if the string was not recognized. Otherwise returns true. */
int string2isds_DbType(isds_DbType *type, const char *string);

/* Return static box state string or NULL if undefined */
const char *DbState2string(const long int *state);

/* Print formatted header if locale-encoded value is defined.
 * @header is locale encoded header name
 * @value is locale encoded header value */
void print_header(const char *header, const char *value);

/* Print formatted header if boolean value is defined.
 * @header is locale encoded header name */
void print_header_bool(const char *header, const _Bool *value);

/* Print formatted header if long int value is defined.
 * @header is locale encoded header name */
void print_header_longint(const char *header, const long int *value);

/* Print formatted header if unsigned long int value is defined.
 * @header is locale encoded header name */
void print_header_ulongint(const char *header, const unsigned long int *value);

/* Print formatted header if time value is defined.
 * @header is locale encoded header name */
void print_header_timeval(const char *header, const struct timeval *time);

/* Print formatted header if date value is defined.
 * @header is locale encoded header name */
void print_header_tm(const char *header, const struct tm *date);

/* Print formatted header if UTF-8 value is defined.
 * @header is locale encoded header name
 * @value is UTF-8 encoded header value */
void print_header_utf8(const char *header, const char *value);

/* Print formatted header if long int CZK/100 currency value is defined.
 * @header is locale encoded header name
 * @value is pointer to value in hundredths of CZK */
void print_header_currency(const char *header, const long int *value);

void print_message_list(const struct isds_list *messages, _Bool outgoing);
void format_message(const struct isds_message *message);

/* Print formatted list of message copies */
void format_copies(const struct isds_list *copies);

void format_DbOwnerInfo(const struct isds_DbOwnerInfo *info);

/* Print formated details about a box found by a full-text */
void format_isds_fulltext_result(const struct isds_fulltext_result *result);

void format_DbUserInfo(const struct isds_DbUserInfo *info);

/* Print formated details about message sender */
void format_sender_info(const char *dbID, const isds_sender_type *type,
        const char *raw_type, const char *name);

/* Print formated details about credit change event */
void format_credit_event(const struct isds_credit_event *event);

void format_commercial_permission(
        const struct isds_commercial_permission *permission);

#endif

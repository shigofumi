#ifndef __ISDS_UI_H__
#define __ISDS_UI_H__

/* Convenience wrappers around *printf() functions to pass text to output
 * FILE stream.
 * TODO: Find UI patterns and make generic toolkit interface allowing to pass
 * it into GUI toolkit */

#include <stdio.h>
#include "utils.h"

#define oprintf(format, ...) fprintf(output, (format), ## __VA_ARGS__)
#define eprintf(format, ...) fprintf(stderr, (format), ## __VA_ARGS__)

#define ohprint(locale_string, width) \
    fhprint(output, (locale_string), (width));

#define onprint(locale_string, width) \
    fnprint(output, (locale_string), (width));

/* Convenience weappers around fwrite() function to pass data to output FILE
 * stream. */
#define owrite(data, length) fwrite((data), 1, (length), output)
#define ewrite(data, length) fwrite((data), 1, (length), stderr)

#endif

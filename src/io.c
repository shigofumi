#define _XOPEN_SOURCE 500
#define _SVID_SOURCE /* For mkstemps(3) up to glibc-2.19 */
#define _DEFAULT_SOURCE /* For mkstemps(3) since glibc-2.20 */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/mman.h>
#include <stdint.h>
#include <magic.h>
#include <limits.h> /* SSIZE_MAX */
#include <stdlib.h> /* mkstemps(3) */
#include <isds.h>
#include <libxml/parser.h> /* For xmlParseMemory() */
#include <libxml/xmlsave.h> /* For XML serializator */
#include <libxml/xpath.h> /* For XPath */

#include "shigofumi.h"

#if ENABLE_XATTR
#if HAVE_SYS_XATTR
#include <sys/xattr.h>
#else
#include <attr/xattr.h>
#endif
#endif


/* Guess MIME type of @file by content.
 * @type is automatically allocated. Return 0, -1 and * NULL @type in case
 * of error */
static int get_mime_type_by_magic(const char *file, char **type) {
    magic_t magic;
    const char *magic_guess;

    if (!type) return -1;
    *type = NULL;
    if (!file || !*file) return -1;

    if (!(magic = magic_open(MAGIC_MIME_TYPE))) {
        fprintf(stderr, _("Could not initialize libmagic\n"));
        return -1;
    }
    if (magic_load(magic, NULL)) {
        fprintf(stderr, _("Could not load magic database\n"));
        return -1;
    }

    if (!(magic_guess = magic_file(magic, file))) {
        fprintf(stderr, _("%s: Could not guess MIME type: %s\n"),
                file, magic_error(magic));
        magic_close(magic);
        return -1;
    }

    if (!(*type = strdup(magic_guess))) {
        fprintf(stderr, _("Not enough memory\n"));
        magic_close(magic);
        return -1;
    }

    magic_close(magic);
    return 0;
}


#if ENABLE_XATTR
#define XATTR_MIME_TYPE "user.mime_type"

/* Get MIME type from file extended attribute.
 * @fd is valid file descriptor or -1
 * @type is automatically allocated. Return 0, -1 and * NULL @type in case
 * of error */
/* XXX: This is Linux specific. There is a library, but it supports IRIX
 * in addition only. */
static int get_mime_type_by_xattr(int fd, char **type) {
    char *buffer = NULL;
    ssize_t length = 0, ret;

    if (!type) return -1;
    *type = NULL;
    if (fd < 0) return -1;

    /* Get initial size */
    length = fgetxattr(fd, XATTR_MIME_TYPE, NULL, 0);
    if (length <= 0) return -1;

    /* Grow buffer as needed */
    for (ret = -1, errno = ERANGE; ret < 0 && errno == ERANGE; length *= 2) {
        buffer = realloc(*type, length + 1);
        if (!buffer) {
            fprintf(stderr, _("Not enough memory\n"));
            zfree(*type);
            return -1;
        }
        *type = buffer;
        ret = fgetxattr(fd, XATTR_MIME_TYPE, *type, length);

        /* Break on length overflow */
        if (length >= SSIZE_MAX / 2) {
            fprintf(stderr, _("Extended attribute too long\n"));
            zfree(*type);
            return -1;
        }
    }

    if (ret <= 0) {
        zfree(*type);
        return -1;
    }

    (*type)[ret] = '\0';
    return 0;
}
#endif /* ENABLE_XATTR */


/* Get MIME type of file.
 * File can be specified by name or descriptor or both.
 * @fd is open file descriptor of the file or -1
 * @name is name of the same file or NULL
 * @type is automatically allocated.
 * Return 0; -1 and NULL @type in case of error */
static int get_mime_type(int fd, const char *name, char **type) {

    if (!type) return -1;
    *type = NULL;
    if ((!name || !*name) && fd == -1) return -1;

#ifdef ENABLE_XATTR
    if (get_mime_type_by_xattr(fd, type))
#endif
        if (get_mime_type_by_magic(name, type))
            return -1;

    return 0;
}


/* Annotate MIME type to a file
 * @fd is descriptor of the file
 * @type is mime_type in UTF-8
 * Return 0 on success, -1 on failure */
/* XXX: Linux specific */
static int save_mime_type(int fd, const char *type) {
    if (!type || !*type) return -1;

#if ENABLE_XATTR
    if (fsetxattr(fd, XATTR_MIME_TYPE, type, strlen(type), 0)) {
        fprintf(stderr,
                _("Could not save MIME type into extended attribute: %s\n"),
                    strerror(errno));
        return -1;
    }
#endif /* ENABLE_XATTR */
    return 0;
}

#undef XATTR_MIME_TYPE


/* Open file and return descriptor. Return -1 in case of error. */
int open_file_for_writing(const char *file, _Bool truncate, _Bool overwrite) {
    int fd;

    if (!file) return -1;

    fd = open(file, O_WRONLY|O_CREAT|O_APPEND |
                ((truncate) ? O_TRUNC : 0) |
                ((overwrite) ? 0 : O_EXCL),
            0666);
    if (fd == -1) {
        fprintf(stderr, _("%s: Could not open file for writing: %s\n"),
                file, strerror(errno));
    }

    return fd;
}


/* Create new file.
 * @file is static template for file name. See mkstemps(3). It will contain
 * new file name on return.
 * @suffix_length is number of bytes of immutable file name suffix.
 * @return descriptor of opened file, or -1 in case of error. */
int create_new_file(char *file, int suffix_length) {
    int fd;

    if (NULL == file) return -1;

    /* TODO: mkstemps() works in CWD. Implement something working in $TMP. */
    fd = mkstemps(file, suffix_length);
    if (fd == -1) {
        fprintf(stderr, _("Could not create temporary file `%s': %s\n"),
                file, strerror(errno));
        return -1;
    }

    return fd;
}


/* Return 0, -1 in case of error */
int mmap_file(const char *file, int *fd, void **buffer, size_t *length) {
    struct stat file_info;

    if (!file || !fd || !buffer || !length) return -1;


    *fd = open(file, O_RDONLY);
    if (*fd == -1) {
        fprintf(stderr, _("%s: Could not open file: %s\n"),
                file, strerror(errno));
        return -1;
    }

    if (-1 == fstat(*fd, &file_info)) {
        fprintf(stderr, _("%s: Could not get file size: %s\n"), file,
                strerror(errno));
        close(*fd);
        return -1;
    }
    if (file_info.st_size < 0) {
        fprintf(stderr, _("File `%s' has negative size: %jd\n"), file,
                (intmax_t) file_info.st_size);
        close(*fd);
        return -1;
    }
    *length = file_info.st_size;

    if (!*length) {
        /* Empty region cannot be mmapped */
        *buffer = NULL;
    } else {
        *buffer = mmap(NULL, *length, PROT_READ, MAP_PRIVATE, *fd, 0);
        if (*buffer == MAP_FAILED) {
            fprintf(stderr, _("%s: Could not map file to memory: %s\n"), file,
                    strerror(errno));
            close(*fd);
            return -1;
        }
    }

    return 0;
}


/* Return 0, -1 in case of error */
int munmap_file(int fd, void *buffer, size_t length) {
    int err = 0;
    long int page_size = sysconf(_SC_PAGE_SIZE);
    size_t pages = (length % page_size) ?
        ((length / page_size) + 1) * page_size:
        length;

    if (length) {
        err = munmap(buffer, pages);
        if (err) {
            fprintf(stderr,
                    _("Could not unmap memory at %p and length %zu: %s\n"),
                    buffer, pages, strerror(errno));
        }
    }

    err = close(fd);
    if (err) {
        fprintf(stderr, _("Could close file descriptor %d: %s\n"), fd,
                strerror(errno));
    }

    return err;
}


/* Return 0, -1 in case of error.
 * @length and @mime_type are optional. */
int load_data_from_file(const char *file, void **data, size_t *length,
        char **mime_type) {
    int fd;
    void *buffer;
    size_t map_length;
    
    if (!file || !data) return -1;

    if (mmap_file(file, &fd, &buffer, &map_length)) return -1;

    printf(ngettext("Reading %zu byte from file `%s'...\n",
                "Reading %zu bytes from file `%s'...\n", map_length),
            map_length, file);
    *data = malloc(map_length);
    if (!*data) {
        fprintf(stderr, _("Error: Not enough memory\n"));
        munmap_file(fd, buffer, map_length);
        return -1;
    }
    memcpy(*data, buffer, map_length);
    if (length) *length = map_length;

    if (mime_type) {
        if (get_mime_type(fd, file, mime_type))
            fprintf(stderr, _("Warning: %s: Could not determine MIME type\n"),
                    file);
    }

    munmap_file(fd, buffer, map_length);
    printf(_("Done.\n"));
    return 0;
}


/* Save @data to file specified by descriptor @fd. If @fd is negative, @file
 * file will be opened first. Descriptor is closed at the end of this
 * function. Supply @file name even if @fd is positive, the name could be used
 * in messages. */
int save_data_to_file(const char *file, int fd, const void *data,
        const size_t length, const char *mime_type, _Bool overwrite) {
    ssize_t written, left = length;
    
    if (fd < 0 && !file) return -1;
    if (length > 0 && !data) return -1;

    if (fd < 0) {
        fd = open_file_for_writing(file, 1, overwrite);
        if (fd == -1) return -1;
    }

    if (file) 
        printf(ngettext("Writing %zu byte to file `%s'...\n",
                    "Writing %zu bytes to file `%s'...\n", length),
                length, file);
    else
        printf(ngettext("Writing %zu byte to file desciptor %d...\n",
                    "Writing %zu bytes to file descriptor %d...\n", length),
                length, fd);
    while (left) {
        written = write(fd, data + length - left, left);
        if (written == -1) {
            if (file)
                fprintf(stderr, _("%s: Could not save file: %s\n"),
                        file, strerror(errno));
            else
                fprintf(stderr, _("Descriptor %d: Could not write into: %s\n"),
                        fd, strerror(errno));
            close(fd);
            return -1;
        }
        left-=written;
    }

    save_mime_type(fd, mime_type);

    if (-1 == close(fd)) {
        if (file)
            fprintf(stderr, _("%s: Closing file failed: %s\n"),
                    file, strerror(errno));
        else
            fprintf(stderr, _("Descript %d: Closing failed: %s\n"),
                    fd, strerror(errno));
        return -1;
    }

    printf(_("Done.\n"));
    return 0;
}


/* @node_list is pointer to by-function allocated weak copy of libxml node
 * pointers list. *NULL means empty list. It will be freed and NULLed in case
 * of error.
 * @xpat_expr is UTF-8 encoded XPath expression. */
static int xpath2nodelist(xmlNodePtr *node_list, xmlXPathContextPtr xpath_ctx,
        const xmlChar *xpath_expr) {
    xmlXPathObjectPtr result = NULL;
    xmlNodePtr node = NULL, prev_node = NULL, next_node;

    if (!node_list || !xpath_ctx || !xpath_expr) return -1;

    result = xmlXPathEvalExpression(xpath_expr, xpath_ctx);
    if (!result) {
        char *xpath_expr_locale = utf82locale((const char*) xpath_expr);
        fprintf(stderr, _("Error while evaluating XPath expression `%s'\n"),
                xpath_expr_locale);
        free(xpath_expr_locale);
        return -1;
    }

    if (xmlXPathNodeSetIsEmpty(result->nodesetval)) {
        /* Empty match, returning empty node list */
        *node_list = NULL;
    } else {
        /* Convert node set to list of siblings */
        for (int i = 0; i < result->nodesetval->nodeNr; i++) {
            /* Make weak copy of the node */
            node = malloc(sizeof(*node));
            if (!node) {
                fprintf(stderr, _("Not enough memory\n"));
                xmlXPathFreeObject(result);
                for (node = *node_list; node; node = next_node) {
                    next_node = node->next;
                    free(node);
                }
                *node_list = NULL;
                return -1;
            }
            memcpy(node, result->nodesetval->nodeTab[i], sizeof(*node));

            /* Add node to node_list */
            node->prev = prev_node;
            node->next = NULL;
            if (prev_node)
                prev_node->next = node;
            else
                *node_list = node;
            prev_node = node;

            /* Debug */
            /*printf("* Embeding node #%d:\n", i);
            xmlDebugDumpNode(stdout, node, 2);*/
        }
    }

    xmlXPathFreeObject(result);
    return 0;
}


/* Parse @buffer as XML document and return @node_list specified by
 * @xpath_expression.
 * @node_list is weak copy that must be non-recursively freed by caller. Caller
 * must free XML document (accessible through @node_list member) on its own.
 * In case of error @node_list value will be invalid.
 * If @node_list is returned empty, function freed parsed document already.
 * @xpat_expr is UTF-8 encoded XPath expression */ 
int load_xml_subtree_from_memory(const void *buffer, size_t length,
        xmlNodePtr *node_list, const char *xpath_expr) {
    int retval = 0;
    xmlDocPtr xml = NULL;
    xmlXPathContextPtr xpath_ctx = NULL;

    if (!buffer || !length) {
        fprintf(stderr,
                _("Empty XML document, XPath expression cannot be evaluated\n"));
        return -1;
    }
    if (!node_list || !xpath_expr)
        return -1;
    *node_list = NULL;

    /* Create XML documents */
    xml = xmlParseMemory(buffer, length);
    if (!xml) {
        fprintf(stderr, _("Error while parsing document\n"));
        return -1;
    }

    xpath_ctx = xmlXPathNewContext(xml);
    if (!xpath_ctx) {
        fprintf(stderr, _("Error while creating XPath context\n"));
        retval = -1;
        goto leave;
    }

    if (xpath2nodelist(node_list, xpath_ctx, (const xmlChar *)xpath_expr)) {
        char *xpath_expr_locale = utf82locale(xpath_expr);
        fprintf(stderr, _("Could not convert XPath result to node list: %s\n"),
                xpath_expr_locale);
        free(xpath_expr_locale);
        retval = -1;
        goto leave;
    }

leave:
    xmlXPathFreeContext(xpath_ctx);
    if (retval || !*node_list) xmlFreeDoc(xml);
    return retval;
}


/* Deallocate struct isds_document with embedded XML recursively and NULL it */
void free_document_with_xml_node_list(struct isds_document **document) {
    /* Free document xml_node_lists and associated XML document because
     * they are weak copies and isds_document_free() does not free them. */
    if (!document || !*document) return;

    if ((*document)->is_xml && (*document)->xml_node_list) {
        xmlFreeDoc((*document)->xml_node_list->doc);
        for (xmlNodePtr next_node, node = (*document)->xml_node_list; node;
                node = next_node) {
            next_node = node->next;
            free(node);
        }
    }

    isds_document_free(document);
}


/* Serialize XML @node_list to automatically rellaocated libxml @buffer */
int serialize_xml_to_buffer(xmlBufferPtr *buffer, const xmlNodePtr node_list) {
    int retval = 0;
    xmlSaveCtxtPtr save_ctx = NULL;
    xmlNodePtr element, subtree_copy = NULL;
    xmlDocPtr subtree_doc = NULL;

    if (!buffer) {
        retval = -1;
        goto leave;
    }
    if (*buffer) xmlBufferFree(*buffer);

    /* Prepare buffer to serialize into */
    *buffer = xmlBufferCreate();
    if (!*buffer) {
        fprintf(stderr,
                _("Could not create buffer to serialize XML tree into\n"));
        retval = -1;
        goto leave;
    }
    save_ctx = xmlSaveToBuffer(*buffer, "UTF-8", 0);
    if (!save_ctx) {
        fprintf(stderr, _("Could not create XML serializer\n"));
        retval = -1;
        goto leave;
    }


    /* Select node and serialize it */
    if (!node_list->next && node_list->type == XML_TEXT_NODE) {
        /* One text node becomes plain text file */
        /* TODO: Is CDATA expanded as text? Are entities expanded? */
        /* XXX: According LibXML documentation, this function does not return
         * meaningful value yet */
        xmlSaveTree(save_ctx, node_list);
    } else {
        /* Serialize element */
        if (!node_list->next && node_list->type == XML_ELEMENT_NODE) {
            /* One element becomes root */
            element = node_list;
        } else {
            /* Parent becomes root */
            if (!node_list->parent) {
                fprintf(stderr,
                        _("XML node list to serialize is missing parent node\n"));
                retval = -1;
                goto leave;
            }
            element = node_list->parent;
        }

        /* Use temporary XML document to resolve name space definitions */
        /* XXX: We can not use xmlNodeDump() nor xmlSaveTree because it dumps
         * the subtree as is. It can result in not well-formed on invalid XML
         * tree (e.g. name space prefix definition can miss.) */
        subtree_doc = xmlNewDoc(BAD_CAST "1.0");
        if (!subtree_doc) {
            fprintf(stderr, _("Could not build temporary XML document\n"));
            retval = -1;
            goto leave;
        }
        /* XXX: Copy subtree and attach the copy to document.
         * One node can not bee attached into more document at the same time. */
        subtree_copy = xmlCopyNodeList(element);
        if (!subtree_copy) {
            fprintf(stderr, _("Could not copy XML subtree\n"));
            retval = -1;
            goto leave;
        }
        xmlDocSetRootElement(subtree_doc, subtree_copy);
        /* Only this way we get name space definition as @xmlns:isds,
         * otherwise we get name space prefix without definition */
        /* FIXME: Don't overwrite original default name space */
        /*isds_ns = xmlNewNs(subtree_copy, BAD_CAST ISDS_NS, NULL);
        if(!isds_ns) {
            isds_log_message(context, _("Could not create ISDS name space"));
            err = IE_ERROR;
            goto leave;
        }
        xmlSetNs(subtree_copy, isds_ns);*/

        /* XXX: According LibXML documentation, this function does not return
         * meaningful value yet */
        xmlSaveDoc(save_ctx, subtree_doc);
    }

    /* Flush XML to buffer. After this call we are sure all data have been
     * processed successfully. */
    if (-1 == xmlSaveFlush(save_ctx)) {
        fprintf(stderr, _("Could not serialize XML tree\n"));
        retval = -1;
        goto leave;
    }
    
leave:
    xmlFreeDoc(subtree_doc); /* Frees subtree_copy */
    xmlSaveClose(save_ctx);

    if (retval) {
        xmlBufferFree(*buffer);
        *buffer = NULL;
    }

    return retval;
}


/* Save @node_list to file specified by descriptor @fd. If @fd is negative, @file
 * file will be opened first. Descriptor is closed at the end of this
 * function. Supply @file name even if @fd is positive, the name could be used
 * in messages. */
int save_xml_to_file(const char *file, int fd, const xmlNodePtr node_list,
        const char *mime_type, _Bool overwrite) {
    int retval = 0;
    xmlBufferPtr buffer = NULL;

    retval = serialize_xml_to_buffer(&buffer, node_list);

    if (!retval) {
        /* And save buffer into file */
        retval = save_data_to_file(file, fd, buffer->content, buffer->use,
                mime_type, overwrite);
        xmlBufferFree(buffer);
    }

    return retval;
}


/* Return 0 if @path is directory, 1 if not, -1 if error occurred */
int is_directory(const char *path) {
    struct stat dir_stat;

    if (!path) return -1;
    if (stat(path, &dir_stat)) return -1;
    if S_ISDIR(dir_stat.st_mode) return 0;
    else return 1;
}

/* Remove a file.
 * @file to remove
 * @return 0 on success, non-zero otherwise. */
int unlink_file(const char *file) {
    if (NULL == file || !*file) return -1;

    if (-1 == remove(file)) {
        fprintf(stderr, _("Could not remove file `%s': %s\n"),
                file, strerror(errno));
        return -1;
    }

    return 0;
}



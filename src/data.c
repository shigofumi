#define _XOPEN_SOURCE 500
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <stdint.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <isds.h>
#include <readline/readline.h>

#include "shigofumi.h"
#include "ui.h"

void print_DbState(const long int state) {
    switch(state) {
        case DBSTATE_ACCESSIBLE: oprintf("ACCESSIBLE\n"); break;
        case DBSTATE_TEMP_UNACCESSIBLE: oprintf("TEMP_UNACCESSIBLE\n"); break;
        case DBSTATE_NOT_YET_ACCESSIBLE: oprintf("NOT_YET_ACCESSIBLE\n"); break;
        case DBSTATE_PERM_UNACCESSIBLE: oprintf("PERM_UNACCESSIBLE\n"); break;
        case DBSTATE_REMOVED: oprintf("REMOVED\n"); break;
        default: oprintf("<unknown state %ld>\n", state);
    }
}

void print_DbType(const long int *type) {
    if (!type) oprintf("NULL\n");
    else
        switch(*type) {
            case DBTYPE_SYSTEM: oprintf("SYSTEM\n"); break;
            case DBTYPE_FO: oprintf("FO\n"); break;
            case DBTYPE_PFO: oprintf("PFO\n"); break;
            case DBTYPE_PFO_ADVOK: oprintf("PFO_ADVOK\n"); break;
            case DBTYPE_PFO_AUDITOR: oprintf("PFO_AUDITOR\n"); break;
            case DBTYPE_PFO_DANPOR: oprintf("PFO_DAPOR\n"); break;
            case DBTYPE_PFO_INSSPR: oprintf("PFO_INSSPR\n"); break;
            case DBTYPE_PO: oprintf("PO\n"); break;
            case DBTYPE_PO_ZAK: oprintf("PO_ZAK\n"); break;
            case DBTYPE_PO_REQ: oprintf("PO_REQ\n"); break;
            case DBTYPE_OVM: oprintf("OVM\n"); break;
            case DBTYPE_OVM_EXEKUT: oprintf("OVM_EXEKUT\n"); break;
            case DBTYPE_OVM_FO: oprintf("OVM_FO\n"); break;
            case DBTYPE_OVM_NOTAR: oprintf("OVM_NOTAR\n"); break;
            case DBTYPE_OVM_PFO: oprintf("OVM_PFO\n"); break;
            case DBTYPE_OVM_PO: oprintf("OVM_PO\n"); break;
            case DBTYPE_OVM_REQ: oprintf("OVM_REQ\n"); break;
            default: oprintf("<unknown type %ld>\n", *type);
        }
}


void print_UserType(const long int *type) {
    if (!type) oprintf("NULL\n");
    else
        switch(*type) {
            case USERTYPE_PRIMARY: oprintf("PRIMARY\n"); break;
            case USERTYPE_ENTRUSTED: oprintf("ENTRUSTED\n"); break;
            case USERTYPE_ADMINISTRATOR: oprintf("ADMINISTRATOR\n"); break;
            case USERTYPE_OFFICIAL: oprintf("OFFICIAL\n"); break;
            default: oprintf("<unknown type %ld>\n", *type);
        }
}


void print_UserPrivils(const long int *privils) {

    const char *priviledges[] = {
        "READ_NON_PERSONAL",
        "READ_ALL",
        "CREATE_DM",
        "VIEW_INFO",
        "SEARCH_DB",
        "OWNER_ADM",
        "READ_VAULT",
        "ERASE_VAULT"
    };
    const int priviledges_count = sizeof(priviledges)/sizeof(priviledges[0]);

    if (!privils) oprintf("NULL\n");
    else {
        oprintf("%ld (", *privils);

        for (int i = 0; i < priviledges_count; i++) {
            if (*privils & (1<<i)) {
                oprintf(
                        ((i + 1) == priviledges_count) ? "%s" : "%s|",
                        priviledges[i]);
            }
        }

        oprintf(")\n");
    }
}


void print_hash(const struct isds_hash *hash) {
    if (!hash) {
        oprintf("NULL\n");
        return;
    }
    
    switch(hash->algorithm) {
        case HASH_ALGORITHM_MD5: oprintf("MD5 "); break;
        case HASH_ALGORITHM_SHA_1: oprintf("SHA-1 "); break;
        case HASH_ALGORITHM_SHA_256: oprintf("SHA-256 "); break;
        case HASH_ALGORITHM_SHA_512: oprintf("SHA-512 "); break;
        default: oprintf("<Unknown hash algorithm %d> ", hash->algorithm);
                 break;
    }

    if (!hash->value) oprintf("<NULL>");
    else
        for (int i = 0; i < hash->length; i++) {
            if (i > 0) oprintf(":");
            oprintf("%02x", ((uint8_t *)(hash->value))[i]);
        }

    oprintf("\n");
}


void print_raw_type(const isds_raw_type type) {
    switch(type) {
        case RAWTYPE_INCOMING_MESSAGE:
            oprintf("INCOMING_MESSAGE\n"); break;
        case RAWTYPE_PLAIN_SIGNED_INCOMING_MESSAGE:
            oprintf("PLAIN_SIGNED_INCOMING_MESSAGE\n"); break;
        case RAWTYPE_CMS_SIGNED_INCOMING_MESSAGE:
            oprintf("CMS_SIGNED_INCOMING_MESSAGE\n"); break;
        case RAWTYPE_PLAIN_SIGNED_OUTGOING_MESSAGE:
            oprintf("PLAIN_SIGNED_OUTGOING_MESSAGE\n"); break;
        case RAWTYPE_CMS_SIGNED_OUTGOING_MESSAGE:
            oprintf("CMS_SIGNED_OUTGOING_MESSAGE\n"); break;
        case RAWTYPE_DELIVERYINFO:
            oprintf("DELIVERYINFO\n"); break;
        case RAWTYPE_PLAIN_SIGNED_DELIVERYINFO:
            oprintf("PLAIN_SIGNED_DELIVERYINFO\n"); break;
        case RAWTYPE_CMS_SIGNED_DELIVERYINFO:
            oprintf("CMS_SIGNED_DELIVERYINFO\n"); break;
        default:
            oprintf("<Unknown raw type %d> ", type);
            break;
    }
}


void print_bool(const _Bool *boolean) {
    oprintf("%s\n", (!boolean) ? "NULL" : ((*boolean)? "true" : "false") );
}


void print_longint(const long int *number) {
    if (!number) oprintf("NULL\n");
    else oprintf("%ld\n", *number);
}


void print_PersonName(const struct isds_PersonName *personName) {
    oprintf("\tpersonName = ");
    if (!personName) oprintf("NULL\n");
    else {
        oprintf("{\n");
        oprintf("\t\tpnFirstName = %s\n", personName->pnFirstName);
        oprintf("\t\tpnMiddleName = %s\n", personName->pnMiddleName);
        oprintf("\t\tpnLastName = %s\n", personName->pnLastName);
        oprintf("\t\tpnLastNameAtBirth = %s\n", personName->pnLastNameAtBirth);
        oprintf("\t}\n");
    }
}


void print_Address(const struct isds_Address *address) {
    oprintf("\taddress = ");
    if (!address) oprintf("NULL\n");
    else {
        oprintf("{\n");
        oprintf("\t\tadCity = %s\n", address->adCity);
        oprintf("\t\tadStreet = %s\n", address->adStreet);
        oprintf("\t\tadNumberInStreet = %s\n", address->adNumberInStreet);
        oprintf("\t\tadNumberInMunicipality = %s\n",
                address->adNumberInMunicipality);
        oprintf("\t\tadZipCode = %s\n", address->adZipCode);
        oprintf("\t\tadState = %s\n", address->adState);
        oprintf("\t}\n");
    }
}


void print_date(const struct tm *date) {
    if (!date) oprintf("NULL\n");
    else oprintf("%s", asctime(date));
}


void print_DbOwnerInfo(const struct isds_DbOwnerInfo *info) {
    oprintf("dbOwnerInfo = ");

    if (!info) {
        oprintf("NULL\n");
        return;
    }

    oprintf("{\n");
    oprintf("\tdbID = %s\n", info->dbID);

    oprintf("\tdbType = ");
    print_DbType((long int *) (info->dbType));
    oprintf("\tic = %s\n", info->ic);

    print_PersonName(info->personName);
        
    oprintf("\tfirmName = %s\n", info->firmName);
    
    oprintf("\tbirthInfo = ");
    if (!info->birthInfo) oprintf("NULL\n");
    else {
        oprintf("{\n");
        
        oprintf("\t\tbiDate = ");
        print_date(info->birthInfo->biDate);

        oprintf("\t\tbiCity = %s\n", info->birthInfo->biCity);
        oprintf("\t\tbiCounty = %s\n", info->birthInfo->biCounty);
        oprintf("\t\tbiState = %s\n", info->birthInfo->biState);
        oprintf("\t}\n");
    }
    
    print_Address(info->address);

    oprintf("\tnationality = %s\n", info->nationality);
    oprintf("\temail = %s\n", info->email);
    oprintf("\ttelNumber = %s\n", info->telNumber);
    oprintf("\tidentifier = %s\n", info->identifier);
    oprintf("\tregistryCode = %s\n", info->registryCode);

    oprintf("\tdbState = ");
    if (!info->dbState) oprintf("NULL\n");
    else print_DbState(*(info->dbState));
    
    oprintf("\tdbEffectiveOVM = ");
    print_bool(info->dbEffectiveOVM);

    oprintf("\tdbOpenAddressing = ");
    print_bool(info->dbOpenAddressing);

    oprintf("}\n");

}


void print_DbUserInfo(const struct isds_DbUserInfo *info) {
    oprintf("dbUserInfo = ");

    if (!info) {
        oprintf("NULL\n");
        return;
    }

    oprintf("{\n");
    oprintf("\tuserID = %s\n", info->userID);

    oprintf("\tuserType = ");
    print_UserType((long int *) (info->userType));

    oprintf("\tuserPrivils = ");
    print_UserPrivils(info->userPrivils);

    print_PersonName(info->personName);
    print_Address(info->address);

    oprintf("\tbiDate = ");
    print_date(info->biDate);
        
    oprintf("\tic = %s\n", info->ic);
    oprintf("\tfirmName = %s\n", info->firmName);
    
    oprintf("\tcaStreet = %s\n", info->caStreet);
    oprintf("\tcaCity = %s\n", info->caCity);
    oprintf("\tcaZipCode = %s\n", info->caZipCode);

    oprintf("}\n");
}


void print_timeval(const struct timeval *time) {
    struct tm broken;
    char buffer[128];

    if (!time) {
        oprintf("NULL\n");
        return;
    }
    
    if (!localtime_r(&(time->tv_sec), &broken)) goto error;
    if (!strftime(buffer, sizeof(buffer)/sizeof(char), "%c", &broken))
        goto error;
    oprintf("%s, %jd us\n", buffer, (intmax_t)time->tv_usec);
    return;

error:
    oprintf("<Error while formatting>\n>");
    return;
}


void print_event_type(const isds_event_type *type) {
    if (!type) {
        oprintf("NULL");
        return;
    }
    switch (*type) {
        case EVENT_UKNOWN: oprintf("UNKNOWN\n"); break;
        case EVENT_ENTERED_SYSTEM: printf("ENTERED_SYSTEM\n"); break;
        case EVENT_ACCEPTED_BY_RECIPIENT:
                           oprintf("ACCEPTED_BY_RECIPIENT\n"); break;
        case EVENT_ACCEPTED_BY_FICTION:
                           oprintf("DELIVERED_BY_FICTION\n"); break;
        case EVENT_UNDELIVERABLE:
                           oprintf("UNDELIVERABLE\n"); break;
        case EVENT_COMMERCIAL_ACCEPTED:
                           oprintf("COMMERCIAL_ACCEPTED\n"); break;
        case EVENT_DELIVERED:
                           oprintf("DELIVERED\n"); break;
        case EVENT_PRIMARY_LOGIN:
                           oprintf("PRIMARY_LOGIN\n"); break;
        case EVENT_ENTRUSTED_LOGIN:
                           oprintf("ENTRUSTED_LOGIN\n"); break;
        case EVENT_SYSCERT_LOGIN:
                           oprintf("SYSCERT_LOGIN\n"); break;
        default: oprintf("<unknown type %d>\n", *type);
    }
}


void print_events(const struct isds_list *events) {
    const struct isds_list *item;
    const struct isds_event *event;

    if (!events) {
        oprintf("NULL\n");
        return;
    }

    oprintf("{\n");

    for (item = events; item; item = item->next) {
        event = (struct isds_event *) item->data;
        oprintf("\t\t\tevent = ");
        if (!event) oprintf("NULL");
        else {
            oprintf("{\n");

            oprintf("\t\t\t\ttype = ");
            print_event_type(event->type);

            oprintf("\t\t\t\tdescription = %s\n", event->description);

            oprintf("\t\t\t\ttime = ");
            print_timeval(event->time);
            
            oprintf("\t\t\t}\n");
        }
    }

    oprintf("\t\t}\n");
}
    

void print_envelope(const struct isds_envelope *envelope) {
    oprintf("\tenvelope = ");

    if (!envelope) {
        oprintf("NULL\n");
        return;
    }
    oprintf("{\n");

    oprintf("\t\tdmID = %s\n", envelope->dmID);
    oprintf("\t\tdbIDSender = %s\n", envelope->dbIDSender);
    oprintf("\t\tdmSender = %s\n", envelope->dmSender);
    oprintf("\t\tdmSenderAddress = %s\n", envelope->dmSenderAddress);
    oprintf("\t\tdmSenderType = ");
    print_DbType(envelope->dmSenderType);
    oprintf("\t\tdmRecipient = %s\n", envelope->dmRecipient);
    oprintf("\t\tdmRecipientAddress = %s\n", envelope->dmRecipientAddress);
    oprintf("\t\tdmAmbiguousRecipient = ");
    print_bool(envelope->dmAmbiguousRecipient);
    oprintf("\t\tdmType = %s\n", envelope->dmType);

    oprintf("\t\tdmSenderOrgUnit = %s\n", envelope->dmSenderOrgUnit);
    oprintf("\t\tdmSenderOrgUnitNum = ");
    print_longint(envelope->dmSenderOrgUnitNum);
    oprintf("\t\tdbIDRecipient = %s\n", envelope->dbIDRecipient);
    oprintf("\t\tdmRecipientOrgUnit = %s\n", envelope->dmRecipientOrgUnit);
    oprintf("\t\tdmRecipientOrgUnitNum = ");
    print_longint(envelope->dmRecipientOrgUnitNum);
    oprintf("\t\tdmToHands = %s\n", envelope->dmToHands);
    oprintf("\t\tdmAnnotation = %s\n", envelope->dmAnnotation);
    oprintf("\t\tdmRecipientRefNumber = %s\n", envelope->dmRecipientRefNumber);
    oprintf("\t\tdmSenderRefNumber = %s\n", envelope->dmSenderRefNumber);
    oprintf("\t\tdmRecipientIdent = %s\n", envelope->dmRecipientIdent);
    oprintf("\t\tdmSenderIdent = %s\n", envelope->dmSenderIdent);

    oprintf("\t\tdmLegalTitleLaw = ");
    print_longint(envelope->dmLegalTitleLaw);
    oprintf("\t\tdmLegalTitleYear = ");
    print_longint(envelope->dmLegalTitleYear);
    oprintf("\t\tdmLegalTitleSect = %s\n", envelope->dmLegalTitleSect);
    oprintf("\t\tdmLegalTitlePar = %s\n", envelope->dmLegalTitlePar);
    oprintf("\t\tdmLegalTitlePoint = %s\n", envelope->dmLegalTitlePoint);

    oprintf("\t\tdmPersonalDelivery = ");
    print_bool(envelope->dmPersonalDelivery);
    oprintf("\t\tdmAllowSubstDelivery = ");
    print_bool(envelope->dmAllowSubstDelivery);
    oprintf("\t\tdmOVM = ");
    print_bool(envelope->dmOVM);
    oprintf("\t\tdmPublishOwnID = ");
    print_bool(envelope->dmPublishOwnID);

    oprintf("\t\tdmOrdinal = ");
    if (!envelope->dmOrdinal) oprintf("NULL\n");
    else oprintf("%lu\n", *(envelope->dmOrdinal));

    oprintf("\t\tdmMessageStatus = ");
    if (!envelope->dmMessageStatus) oprintf("NULL\n");
    else
        switch(*(envelope->dmMessageStatus)) {
            case MESSAGESTATE_SENT: oprintf("SENT\n"); break;
            case MESSAGESTATE_STAMPED: oprintf("STAMPED\n"); break;
            case MESSAGESTATE_INFECTED: oprintf("INFECTED\n"); break;
            case MESSAGESTATE_DELIVERED: oprintf("DELIVERED\n"); break;
            case MESSAGESTATE_SUBSTITUTED: oprintf("SUBSTITUTED\n"); break;
            case MESSAGESTATE_RECEIVED: oprintf("RECEIVED\n"); break;
            case MESSAGESTATE_READ: oprintf("READ\n"); break;
            case MESSAGESTATE_UNDELIVERABLE: oprintf("UNDELIVERABLE\n"); break;
            case MESSAGESTATE_REMOVED: oprintf("REMOVED\n"); break;
            case MESSAGESTATE_IN_SAFE: oprintf("IN_SAFE\n"); break;
            default: oprintf("<unknown type %d>\n",
                             *(envelope->dmMessageStatus));
        }

    oprintf("\t\tdmAttachmentSize = ");
    if (!envelope->dmAttachmentSize) oprintf("NULL\n");
    else oprintf("%lu kB\n", *(envelope->dmAttachmentSize));

    oprintf("\t\tdmDeliveryTime = ");
    print_timeval(envelope->dmDeliveryTime);

    oprintf("\t\tdmAcceptanceTime = ");
    print_timeval(envelope->dmAcceptanceTime);

    oprintf("\t\thash = ");
    print_hash(envelope->hash);

    oprintf("\t\ttimestamp = %p\n", envelope->timestamp);
    oprintf("\t\ttimestamp_length = %zu\n", envelope->timestamp_length);

    oprintf("\t\tevents = ");
    print_events(envelope->events);

    oprintf("\t}\n");
}


void print_document(const struct isds_document *document) {
    oprintf("\t\tdocument = ");

    if (!document) {
        oprintf("NULL\n");
        return;
    }
    oprintf("\{\n");

    oprintf("\t\t\tis_xml = %u\n", !!document->is_xml);
    oprintf("\t\t\txml_node_list = %p\n", document->xml_node_list);

    oprintf("\t\t\tdata = %p\n", document->data);
    oprintf("\t\t\tdata_length = %zu\n", document->data_length);
    oprintf("\t\t\tdmMimeType = %s\n", document->dmMimeType);

    oprintf("\t\t\tdmFileMetaType = ");
    switch(document->dmFileMetaType) {
        case FILEMETATYPE_MAIN: oprintf("MAIN\n"); break;
        case FILEMETATYPE_ENCLOSURE: oprintf("ENCLOSURE\n"); break;
        case FILEMETATYPE_SIGNATURE: oprintf("SIGNATURE\n"); break;
        case FILEMETATYPE_META: oprintf("META\n"); break;
        default: oprintf("<unknown type %d>\n", document->dmFileMetaType);
    }

    oprintf("\t\t\tdmFileGuid = %s\n", document->dmFileGuid);
    oprintf("\t\t\tdmUpFileGuid = %s\n", document->dmUpFileGuid);
    oprintf("\t\t\tdmFileDescr = %s\n", document->dmFileDescr);
    oprintf("\t\t\tdmFormat = %s\n", document->dmFormat);
    oprintf("\t\t}\n");
}


void print_documents(const struct isds_list *documents) {
    const struct isds_list *item;

    oprintf("\tdocuments = ");

    if (!documents) {
        oprintf("NULL\n");
        return;
    }
    oprintf("{\n");

    for (item = documents; item; item = item->next) {
        print_document((struct isds_document *) (item->data));
    }

    oprintf("\t}\n");
}


void print_message(const struct isds_message *message) {
    oprintf("message = ");

    if (!message) {
        oprintf("NULL\n");
        return;
    }

    oprintf("{\n");

    oprintf("\traw = %p\n", message->raw);
    oprintf("\traw_length = %zu\n", message->raw_length);
    oprintf("\traw_type = ");
    print_raw_type(message->raw_type);
    print_envelope(message->envelope);
    print_documents(message->documents);

    oprintf("}\n");
}

void print_copies(const struct isds_list *copies) {
    const struct isds_list *item;
    struct isds_message_copy *copy;

    oprintf("Copies = ");
    if (!copies) {
        oprintf("<NULL>\n");
        return;
    }

    oprintf("{\n");
    for (item = copies; item; item = item->next) {
        copy = (struct isds_message_copy *) item->data;
        oprintf("\tCopy = ");

        if (!copy)
            oprintf("<NULL>\n");
        else {
            oprintf("{\n");
            oprintf("\t\tdbIDRecipient = %s\n", copy->dbIDRecipient);
            oprintf("\t\tdmRecipientOrgUnit = %s\n", copy->dmRecipientOrgUnit);

            oprintf("\t\tdmRecipientOrgUnitNum = ");
            if (copy->dmRecipientOrgUnitNum)
                oprintf("%ld\n", *copy->dmRecipientOrgUnitNum);
            else
                oprintf("<NULL>\n");
            oprintf("\t\tdmToHands = %s\n", copy->dmToHands);

            oprintf("\t\terror = %s\n", isds_strerror(copy->error));
            oprintf("\t\tdmStatus = %s\n", copy->dmStatus);
            oprintf("\t\tdmID = %s\n", copy->dmID);
            oprintf("\t}\n");
        }
    }
    oprintf("}\n");
}


void compare_hashes(const struct isds_hash *hash1,
        const struct isds_hash *hash2) {
    isds_error err;

    oprintf("Comparing hashes... ");
    err = isds_hash_cmp(hash1, hash2);
    if (err == IE_SUCCESS)
        oprintf("Hashes equal\n");
    else if
        (err == IE_NOTEQUAL) oprintf("Hashes differ\n");
    else
        oprintf("isds_hash_cmp() failed: %s\n", isds_strerror(err));
}


int progressbar(double upload_total, double upload_current,
    double download_total, double download_current,
    void *data) {

    oprintf("Progress: upload %0f/%0f, download %0f/%0f, data=%p\n",
            upload_current, upload_total, download_current, download_total,
            data);
    return 0;
}


/* Print formatted header if locale-encoded value is defined.
 * @header is locale encoded header name
 * @value is locale encoded header value */
void print_header(const char *header, const char *value) {
    if (value && *value) oprintf(_("%s: %s\n"), header, value);
}


/* Print formatted header if boolean value is defined.
 * @header is locale encoded header name */
void print_header_bool(const char *header, const _Bool *value) {
    if (value) print_header(header, (*value) ? _("Yes") : _("No"));
}


/* Print formatted header if long int value is defined.
 * @header is locale encoded header name */
void print_header_longint(const char *header, const long int *value) {
    if (value) oprintf(_("%s: %ld\n"), header, *value);
}


/* Print formatted header if unsigned long int value is defined.
 * @header is locale encoded header name */
void print_header_ulongint(const char *header, const unsigned long int *value) {
    if (value) oprintf(_("%s: %lu\n"), header, *value);
}


/* Print formatted header if time value is defined.
 * @header is locale encoded header name */
void print_header_timeval(const char *header, const struct timeval *time) {
    struct tm broken;
    char buffer[128];

    if (!time) return;

    if (!localtime_r(&(time->tv_sec), &broken)) goto error;
    if (!strftime(buffer, sizeof(buffer)/sizeof(char), "%c", &broken))
        goto error;

    if (!time->tv_usec)
        oprintf(_("%s: %s\n"), header, buffer);
    else if ((time->tv_usec % 1000) == 0) 
        oprintf(_("%s: %s, %jd ms\n"), header, buffer, (intmax_t)time->tv_usec/1000);
    else 
        oprintf(_("%s: %s, %jd us\n"), header, buffer, (intmax_t)time->tv_usec);
    
    return;

error:
    oprintf(_("%s: <Error while formatting time>\n"), header);
    return;
}


/* Return formatted date as mallocated string. NULL or special error string can
 * be returned. Application must free even the error string. */
char *tm2string(const struct tm *date) {
    char *buffer;
    size_t buffer_length = 128;
    
    if (!date) return NULL;

    buffer = malloc(buffer_length);
    if (!buffer) return strdup(_("<Error while formatting date>"));

    if (0 == strftime(buffer, buffer_length, "%x", date)) {
        free(buffer);
        return strdup(_("<Error while formatting date>"));
    }
    
    return buffer;
}


/* Convert string representation of full ISO 8601 or locale date to tm structure.
 * Return NULL if error occurs
 * XXX: Not all ISO formats are supported */
struct tm *datestring2tm(const char *string) {
    struct tm *date = NULL;
    char *offset;
    if (!string) return NULL;

    date = calloc(1, sizeof(*date));
    if (!date) return NULL;

    /* xsd:date is ISO 8601 string, thus ASCII */
    offset = strptime(string, "%Y-%m-%d", date);
    if (offset && *offset == '\0')
        return date;

    offset = strptime(string, "%x", date);
    if (offset && *offset == '\0')
        return date;

    free(date);
    return NULL;
}


/* Convert string representation of box type to isds_DbType. Return false
 * if the string was not recognized. Otherwise returns true. */
int string2isds_DbType(isds_DbType *type, const char *string) {
    if (!strcmp(string, "FO"))
        *type = DBTYPE_FO;
    else if (!strcmp(string, "PFO"))
        *type = DBTYPE_PFO;
    else if (!strcmp(string, "PFO_ADVOK"))
        *type = DBTYPE_PFO_ADVOK;
    else if (!strcmp(string, "PFO_AUDITOR"))
        *type = DBTYPE_PFO_AUDITOR;
    else if (!strcmp(string, "PFO_DANPOR"))
        *type = DBTYPE_PFO_DANPOR;
    else if (!strcmp(string, "PFO_INSSPR"))
        *type = DBTYPE_PFO_INSSPR;
    else if (!strcmp(string, "PO"))
        *type = DBTYPE_PO;
    else if (!strcmp(string, "PO_ZAK"))
        *type = DBTYPE_PO_ZAK;
    else if (!strcmp(string, "PO_REQ"))
        *type = DBTYPE_PO_REQ;
    else if (!strcmp(string, "OVM"))
        *type = DBTYPE_OVM;
    else if (!strcmp(string, "OVM_NOTAR"))
        *type = DBTYPE_OVM_NOTAR;
    else if (!strcmp(string, "OVM_EXEKUT"))
        *type = DBTYPE_OVM_EXEKUT;
    else if (!strcmp(string, "OVM_REQ"))
        *type = DBTYPE_OVM_REQ;
    else if (!strcmp(string, "OVM_FO"))
        *type = DBTYPE_OVM_FO;
    else if (!strcmp(string, "OVM_PFO"))
        *type = DBTYPE_OVM_PFO;
    else if (!strcmp(string, "OVM_PO"))
        *type = DBTYPE_OVM_PO;
    else
        return 0;
    return 1;
}


/* Print formatted header if date value is defined.
 * @header is locale encoded header name */
void print_header_tm(const char *header, const struct tm *date) {
    char *string;
    
    if (NULL == date) return;
    string = tm2string(date);

    if (NULL == string)
        oprintf(_("%s: <Error while formatting date>\n"), header);
    else
        print_header(header, string);

    free(string);
}


/* Print formatted header if UTF-8 value is defined.
 * @header is locale encoded header name
 * @value is UTF-8 encoded header value */
void print_header_utf8(const char *header, const char *value) {
    char *value_locale;

    if (!value) return;

    value_locale = utf82locale(value);
    print_header(header,
            (value_locale) ?
                value_locale :
                _("<Error while converting value>"));
    free(value_locale);
}


/* Print formatted header and byte size.
 * @header is locale encoded header name */
void print_header_size(const char *header, size_t size) {
    char *buffer = NULL;

    if (size < (1<<10))
        shi_asprintf(&buffer, _("%zu B"), size);
    else if (size < (1<<20))
        shi_asprintf(&buffer, _("%0.2f KiB"), (float) size/(1<<10));
    else
        shi_asprintf(&buffer, _("%0.2f MiB"), (float) size/(1<<20));

    print_header(header, buffer);
    free(buffer);
}


/* Print formatted header if long int KB size is defined.
 * @header is locale encoded header name */
void print_header_kbsize(const char *header, const long int *size) {
    char *buffer = NULL;
    if (!size) return;

    if (*size < 0)
        shi_asprintf(&buffer, _("<Negative size>"));
    else if (*size < (1000))
        shi_asprintf(&buffer, _("%lu kB"), *size);
    else 
        shi_asprintf(&buffer, _("%0.2f MB"), *size/1000.0);

    print_header(header, buffer);
    free(buffer);
}


/* Print formatted header if long int CZK/100 currency value is defined.
 * @header is locale encoded header name
 * @value is pointer to value in hundredths of CZK */
void print_header_currency(const char *header, const long int *value) {
    char *buffer = NULL;
    unsigned long int integer;
    unsigned long int fraction;

    if (NULL == value) return;

    /* labs(3) and % with negative values are ISO C99 specific. Did I say this
     * code did not support older compilers? */
    integer = labs(*value / 100);
    fraction = labs(*value % 100);

    /* XXX: We could use strfmon(3) only if it was standardized and if it
     * supported arbitrary precision fraction (no float point types).
     * We could reimplement strfmon(3) to use locale defintion, but that would
     * be overkill. Direct localization is simpler. */
    shi_asprintf(&buffer,
           (*value < 0) ?
              _("CZK -%lu.%02lu") :
              _("CZK %lu.%02lu"),
            integer, fraction);

    print_header(header, buffer);

    free(buffer);
}


static const char *DbType2string(const long int *type) {
    if (!type) return NULL;
    switch(*type) {
        case DBTYPE_SYSTEM:         return(_("System"));
        case DBTYPE_FO:             return(_("Private individual"));
        case DBTYPE_PFO:            return(_("Self-employed individual"));
        case DBTYPE_PFO_ADVOK:      return(_("Lawyer"));
        case DBTYPE_PFO_AUDITOR:    return(_("Statutory auditor"));
        case DBTYPE_PFO_DANPOR:     return(_("Tax advisor"));
        case DBTYPE_PFO_INSSPR:     return(_("Insolvency administrator"));
        case DBTYPE_PO:             return(_("Organisation"));
        case DBTYPE_PO_ZAK:         return(_("Organization based by law"));
        case DBTYPE_PO_REQ:         return(_("Organization based on request"));
        case DBTYPE_OVM:            return(_("Public authority"));
        case DBTYPE_OVM_EXEKUT:     return(_("Executor"));
        case DBTYPE_OVM_FO:         return(_("Private individual listed in the public authority index"));
        case DBTYPE_OVM_NOTAR:      return(_("Notary"));
        case DBTYPE_OVM_PFO:        return(_("Self-employed individual listed in the public authority index"));
        case DBTYPE_OVM_PO:         return(_("Organisation listed in the public authority index"));
        case DBTYPE_OVM_REQ:        return(_("Public authority based on request"));
        default:                    return(_("<Unknown type>"));
    }
}


static const char *UserType2string(const long int *type) {
    if (!type) return NULL;
    switch(*type) {
        case USERTYPE_PRIMARY:          return(_("Primary"));
        case USERTYPE_ENTRUSTED:        return(_("Entrusted"));
        case USERTYPE_ADMINISTRATOR:    return(_("Administrator"));
        case USERTYPE_OFFICIAL:         return(_("Official"));
        default: return(_("<Unknown type>"));
    }
}


static const char *isds_sender_type2string(const isds_sender_type *type) {
    if (!type) return NULL;
    switch(*type) {
        case SENDERTYPE_PRIMARY:        return(_("Primary"));
        case SENDERTYPE_ENTRUSTED:      return(_("Entrusted"));
        case SENDERTYPE_ADMINISTRATOR:  return(_("Administrator"));
        case SENDERTYPE_OFFICIAL:       return(_("Official"));
        case SENDERTYPE_VIRTUAL:        return(_("Virtual"));
        default: return(_("<unknown type>"));
    }
}


/* Return formatted user privileges. Caller must free the string */
static char *UserPrivils2string(const long int *privils) {

    const char *priviledges[] = {
        N_("Read non-personal"),
        N_("Read all"),
        N_("Send and read sent"),
        N_("List messages and read delivery details"),
        N_("Search boxes"),
        N_("Administer her box"),
        N_("Read from safe"),
        N_("Delete from safe")
    };
    const int priviledges_count = sizeof(priviledges)/sizeof(priviledges[0]);
    char *buffer = NULL, *new_buffer = NULL;

    if (!privils) return NULL;

    /*oprintf("%ld (", *privils);*/

    for (int i = 0; i < priviledges_count; i++) {
        if (*privils & (1<<i)) {
            if ((*privils % (1<<i)))
                shi_asprintf(&new_buffer, _("%s, %s"),
                    buffer, _(priviledges[i]));
            else
                shi_asprintf(&new_buffer, "%s", _(priviledges[i]));

            if (!new_buffer) {
                free(buffer);
                return(strdup(_("<Error while formatting privileges>")));
            }

            free(buffer);
            buffer = new_buffer;
            new_buffer = NULL;
        }
    }

    if (*privils >= (1<<priviledges_count)) {
        if ((*privils % (1<<priviledges_count)))
            shi_asprintf(&new_buffer, _("%s, %s"),
                buffer, _("<Unknown privilege>"));
        else
            shi_asprintf(&new_buffer, "%s", _("<Unknown privilege>"));

        if (!new_buffer) {
            free(buffer);
            return(strdup(_("<Error while formatting privileges>")));
        }

        free(buffer);
        buffer = new_buffer;
        new_buffer = NULL;
    }

    return buffer;
}


static const char *DmType2string(const char *type) {
    if (!type) return NULL;
    if (!strcmp(type, "V"))
        return(_("Public"));
    else if (!strcmp(type, "K"))
        return(_("Commercial"));
    else if (!strcmp(type, "A"))
        return(_("Commercial (initiatory, unused, subsidized)"));
    else if (!strcmp(type, "B"))
        return(_("Commercial (initiatory, used, subsidized)"));
    else if (!strcmp(type, "C"))
        return(_("Commercial (initiatory, expired, subsidized)"));
    else if (!strcmp(type, "D"))
        return(_("Commercial (externally subsidized)"));
    else if (!strcmp(type, "E"))
        return(_("Commercial (prepaid by a stamp)"));
    else if (!strcmp(type, "G"))
        return(_("Commercial (sponsored)"));
    else if (!strcmp(type, "I"))
        return(_("Commercial (initiatory, unused, paid by sender)"));
    else if (!strcmp(type, "O"))
        return(_("Commercial (response paid by recipient)"));
    else if (!strcmp(type, "X"))
        return(_("Commercial (initiatory, expired, paid by sender)"));
    else if (!strcmp(type, "Y"))
        return(_("Commercial (initiatory, used, paid by sender)"));
    else if (!strcmp(type, "Z"))
        return(_("Commercial (limitedly subsidized)"));
    else return(_("<Unknown type>"));
}


/* Simplified commercial status printed on message listing.
 * 'P' is a public message sent by a government
 * 'C' is a commercial message
 * 'I' is a commercial message offering to pay response instead of the
 *     responder
 * 'i' is a commercial message that offered to pay reposne, but it does not
 *     anymore.
 * 'R' is a commercial response message paid by sender of the I message. */
static const char DmType2flag(const char *type) {
    if (!type) return ' ';
    if (!strcmp(type, "V")) return('P');
    else if (!strcmp(type, "K")) return('C');
    else if (!strcmp(type, "A")) return('I');
    else if (!strcmp(type, "B")) return('i');
    else if (!strcmp(type, "C")) return('i');
    else if (!strcmp(type, "D")) return('C');
    else if (!strcmp(type, "E")) return('C');
    else if (!strcmp(type, "G")) return('C');
    else if (!strcmp(type, "I")) return('I');
    else if (!strcmp(type, "O")) return('R');
    else if (!strcmp(type, "X")) return('i');
    else if (!strcmp(type, "Y")) return('i');
    else if (!strcmp(type, "Z")) return('C');
    else return('?');
}


static const char *DmMessageStatus2string(const isds_message_status *status) {
    if (!status) return NULL;
    switch(*status) {
        case MESSAGESTATE_SENT:         return(_("Sent"));
        case MESSAGESTATE_STAMPED:      return(_("Stamped"));
        case MESSAGESTATE_INFECTED:     return(_("Infected"));
        case MESSAGESTATE_DELIVERED:    return(_("Delivered ordinary"));
        case MESSAGESTATE_SUBSTITUTED:  return(_("Delivered substitutably"));
        case MESSAGESTATE_RECEIVED:     return(_("Accepted"));
        case MESSAGESTATE_READ:         return(_("Read"));
        case MESSAGESTATE_UNDELIVERABLE:    return(_("Undeliverable"));
        case MESSAGESTATE_REMOVED:      return(_("Deleted"));
        case MESSAGESTATE_IN_SAFE:      return(_("Stored in safe"));
        default:                        return(_("<Unknown state>"));
    }
}


static const char DmMessageStatus2flag(const isds_message_status *status) {
    if (!status) return ' ';
    switch(*status) {
        case MESSAGESTATE_SENT:         return('>');
        case MESSAGESTATE_STAMPED:      return('t');
        case MESSAGESTATE_INFECTED:     return('I');
        case MESSAGESTATE_DELIVERED:    return('N');
        case MESSAGESTATE_SUBSTITUTED:  return('n');
        case MESSAGESTATE_RECEIVED:     return('O');
        case MESSAGESTATE_READ:         return(' ');
        case MESSAGESTATE_UNDELIVERABLE:    return('!');
        case MESSAGESTATE_REMOVED:      return('D');
        case MESSAGESTATE_IN_SAFE:      return('S');
        default:                        return('?');
    }
}


/* Return timeval time formatted into shortest string with respect to current
 * time. Caller must free the string. */
static char *timeval2shortstring(const struct timeval *timeval) {
    struct tm broken, current_broken;
    time_t current_time;
    char *buffer = NULL;
    const size_t buffer_size = 16;

    if (!timeval) return NULL;

    buffer = malloc(buffer_size);
    if (!buffer) goto error;

    /* Get current time */
    current_time = time(NULL);
    if (current_time == (time_t) -1) goto error;
    if (!localtime_r(&current_time, &current_broken)) goto error;

    /* Get broken time */
    if (!localtime_r(&(timeval->tv_sec), &broken)) goto error;

    /* Select proper abbreviated string representation */
    if (broken.tm_year == current_broken.tm_year &&
            broken.tm_yday == current_broken.tm_yday) {
        /* Minute resolution in the same day */
        if (!strftime(buffer, buffer_size, _("%k:%M"), &broken))
            goto error;
    } else {
        /* Otherwise month and day */
        if (!strftime(buffer, buffer_size, _("%b %d"), &broken))
            goto error;
    }
    
    return buffer;

error:
    free(buffer);
    return strdup(_("<Error>"));
}


/* Formatted Law Authorization if defined. You must free it. */
static char *envelope_law2string(const struct isds_envelope *envelope) {
    char *output = NULL;
    char *year_locale = NULL, *law_locale = NULL;
    char *sect_locale = NULL, *par_locale = NULL, *point_locale = NULL;

    if (!envelope ||
            !(envelope->dmLegalTitleYear || envelope->dmLegalTitleLaw ||
                envelope->dmLegalTitleSect || envelope->dmLegalTitlePar ||
                envelope->dmLegalTitlePoint)
            ) return NULL;

    if (envelope->dmLegalTitleYear)
        shi_asprintf(&year_locale, "%ld", *envelope->dmLegalTitleYear);
    else
        year_locale = strdup(_("?"));

    if (envelope->dmLegalTitleLaw)
        shi_asprintf(&law_locale, "%ld", *envelope->dmLegalTitleLaw);
    else
        law_locale = strdup(_("?"));

    sect_locale = utf82locale(envelope->dmLegalTitleSect);
    par_locale = utf82locale(envelope->dmLegalTitlePar);
    point_locale = utf82locale(envelope->dmLegalTitlePoint);

    if (point_locale)
        shi_asprintf(&output, _("point %s, par. %s, sect. %s, %s/%s Coll."),
                point_locale, par_locale, sect_locale,
                law_locale, year_locale);
    else if (par_locale)
        shi_asprintf(&output, _("par. %s, sect. %s, %s/%s Coll."),
                par_locale, sect_locale, law_locale, year_locale);
    else if (sect_locale)
        shi_asprintf(&output, _("sect. %s, %s/%s Coll."),
                sect_locale, law_locale, year_locale);
    else 
        shi_asprintf(&output, _("%s/%s Coll."),
                law_locale, year_locale);

    free(year_locale);
    free(law_locale);
    free(sect_locale);
    free(par_locale);
    free(point_locale);
    return output;
}


static const char *isds_payment_type2string(const isds_payment_type *type) {
    if (!type) return NULL;
    switch(*type) {
        case PAYMENT_SENDER:            return(_("Payed by sender"));
        case PAYMENT_STAMP:             return(_("Stamp pre-paid by sender"));
        case PAYMENT_SPONSOR:           return(_("Sponsor pays all messages"));
        case PAYMENT_RESPONSE:          return(_("Recipient pays a response"));
        case PAYMENT_SPONSOR_LIMITED:   return(_("Limitedly subsidized"));
        case PAYMENT_SPONSOR_EXTERNAL:  return(_("Externally subsidized"));
        default: return(_("<unknown type>"));
    }
}


/* Print formatted header if time any of message ID.
 * @header is locale encoded header name */
static void print_header_messages_ids(const char *header,
        const char *ref_number, const char *ident) {
    if (ref_number || ident) {
        oprintf(_("%s:\n"), header);
        print_header_utf8(_("\tReference number"), ref_number);
        print_header_utf8(_("\tFile ID"), ident);
    }
}


/* Return formatted hash value */
char *hash2string(const struct isds_hash *hash) {
    const char *algorithm_string = NULL;
    char *buffer = NULL, *octet = NULL, *new_buffer = NULL;

    if (!hash || !hash->value) return NULL;
    
    switch(hash->algorithm) {
        case HASH_ALGORITHM_MD5: algorithm_string = (_("MD5")); break;
        case HASH_ALGORITHM_SHA_1: algorithm_string = (_("SHA-1")); break;
        case HASH_ALGORITHM_SHA_224: algorithm_string = (_("SHA-224")); break;
        case HASH_ALGORITHM_SHA_256: algorithm_string = (_("SHA-256")); break;
        case HASH_ALGORITHM_SHA_384: algorithm_string = (_("SHA-384")); break;
        case HASH_ALGORITHM_SHA_512: algorithm_string = (_("SHA-512")); break;
        default: algorithm_string = (_("<Unknown hash algorithm>")); break;
    }

    for (int i = 0; i < hash->length; i++) {
        shi_asprintf(&octet, "%02x", ((uint8_t *)(hash->value))[i]);
        if (!octet) {
            free(buffer); free(octet); return NULL;
        }

        if (i > 0)
            shi_asprintf(&new_buffer, _("%s:%s"), buffer, octet);
        else
            shi_asprintf(&new_buffer, "%s", octet);
        if (!new_buffer) {
            free(buffer); free(octet); return NULL;
        }

        buffer = new_buffer; new_buffer = NULL;
    }

    shi_asprintf(&new_buffer, _("%s %s"), algorithm_string, buffer);
    free(buffer);

    return new_buffer;
}


/* Print if any message delivery info exists. */
void print_message_delivery_info(const struct isds_envelope *envelope) {
    char *hash_string = NULL;

    if (envelope && (envelope->dmType || envelope->dmMessageStatus || 
                envelope->dmDeliveryTime ||
                envelope->dmAcceptanceTime || envelope->dmPersonalDelivery ||
                envelope->dmAllowSubstDelivery || envelope->hash ||
                envelope->dmOrdinal)) {
        oprintf(_("Delivery data:\n"));
        print_header(_("\tMessage type"), DmType2string(envelope->dmType));
        print_header(_("\tMessage status"),
                DmMessageStatus2string(envelope->dmMessageStatus));
        print_header_timeval(_("\tDelivered"), envelope->dmDeliveryTime);
        print_header_timeval(_("\tAccepted"), envelope->dmAcceptanceTime);
        print_header_bool(_("\tPersonal delivery required"),
                envelope->dmPersonalDelivery);
        print_header_bool(_("\tAllow substitutable delivery"),
                envelope->dmAllowSubstDelivery);

        hash_string = hash2string(envelope->hash);
        print_header(_("\tHash"), hash_string);
        free(hash_string);

        print_header_ulongint(_("\tOrdinal number"), envelope->dmOrdinal);
    }
}


static const char *event_type2string(const isds_event_type *type) {
    if (!type) return (_("<Undefined>"));

    switch (*type) {
        case EVENT_UKNOWN: return(_("Unknown"));
        case EVENT_ENTERED_SYSTEM:
                           return(_("Entered system"));
        case EVENT_ACCEPTED_BY_RECIPIENT:
                           return(_("Accepted by recipient"));
        case EVENT_ACCEPTED_BY_FICTION:
                           return(_("Delivered substitutably"));
        case EVENT_UNDELIVERABLE:
                           return(_("Undeliverable"));
        case EVENT_COMMERCIAL_ACCEPTED:
                           return(_("Commerical message accepted by "
                                       "recipient")); break;
        case EVENT_DELIVERED:
                           return(_("Delivered into box")); break;
        case EVENT_PRIMARY_LOGIN:
                           return(_("Primary user logged in")); break;
        case EVENT_ENTRUSTED_LOGIN:
                           return(_("Entrusted user logged in")); break;
        case EVENT_SYSCERT_LOGIN:
                           return(_("Application logged in by system "
                                       "certificate")); break;
        default: return(_("<Unknown event type>"));
    }
}


/* Print if any delivery event  exists. */
void print_delivery_events(const struct isds_list *events) {
    int counter_width = 3;
    int order = 0;
    const struct isds_list *item;
    struct isds_event *event;

    if (!events) return;

    oprintf(_("Delivery events:\n"));

    for (item = events; item; item = item->next) {
        event = (struct isds_event *) item->data;
        if (!event) continue;
        order++;

        oprintf(_("%*d %s\n"), counter_width, order,
                event_type2string(event->type));
        print_header_utf8(_("\tDescription"), event->description);
        print_header_timeval(_("\tWhen"), event->time);
    }
}


/* Print formatted message envelope */
void format_envelope(const struct isds_envelope *envelope) {
    char *law_string = NULL;

    if (!envelope) {
        oprintf(_("<Missing envelope>\n"));
        return;
    }

    print_header_utf8(_("Message ID"), envelope->dmID);

    if (envelope->dbIDSender || envelope->dmSender ||
            envelope->dmSenderOrgUnit || envelope->dmSenderOrgUnitNum ||
            envelope->dmSenderAddress || envelope->dmSenderType ||
            envelope->dmOVM || envelope->dmPublishOwnID) {
        oprintf(_("Sender:\n"));
        print_header_utf8(_("\tID"), envelope->dbIDSender);
        print_header_utf8(_("\tName"), envelope->dmSender);
        print_header_utf8(_("\tUnit"), envelope->dmSenderOrgUnit);
        print_header_longint(_("\tUnit number"), envelope->dmSenderOrgUnitNum);
        print_header_utf8(_("\tAddress"), envelope->dmSenderAddress);
        print_header(_("\tType"), DbType2string(envelope->dmSenderType));
        print_header_bool(_("\tAs public authority"), envelope->dmOVM);
        print_header_bool(_("\tPublish user's identity"),
                envelope->dmPublishOwnID);
    }

    if (envelope->dbIDRecipient || envelope->dmRecipient ||
            envelope->dmRecipientOrgUnit || envelope->dmRecipientOrgUnitNum ||
            envelope->dmToHands || envelope->dmRecipientAddress ||
            envelope->dmAmbiguousRecipient) {
        oprintf(_("Recipient:\n"));
        print_header_utf8(_("\tID"), envelope->dbIDRecipient);
        print_header_utf8(_("\tName"), envelope->dmRecipient);
        print_header_utf8(_("\tUnit"), envelope->dmRecipientOrgUnit);
        print_header_longint(_("\tUnit number"),
                envelope->dmRecipientOrgUnitNum);
        print_header_utf8(_("To hands"), envelope->dmToHands);
        print_header_utf8(_("\tAddress"), envelope->dmRecipientAddress);
        print_header_bool(_("\tAs public authority"),
                envelope->dmAmbiguousRecipient);
    }

    print_header_utf8(_("Subject"), envelope->dmAnnotation);

    print_header_messages_ids(_("Sender message IDs"),
            envelope->dmSenderRefNumber,  envelope->dmSenderIdent);
    print_header_messages_ids(_("Recipient message IDs"),
            envelope->dmRecipientRefNumber, envelope->dmRecipientIdent);

    law_string = envelope_law2string(envelope);
    print_header(_("Law authorization"), law_string);
    free(law_string);

    print_message_delivery_info(envelope);

    print_header_kbsize(_("Document total size"), envelope->dmAttachmentSize);

    /*oprintf("\t\ttimestamp = %p\n", envelope->timestamp);
    oprintf("\t\ttimestamp_length = %zu\n", envelope->timestamp_length);*/
    print_delivery_events(envelope->events);

}

const char *DmFileMetaType2string(isds_FileMetaType type) {
    switch(type) {
        case FILEMETATYPE_MAIN:         return(_("Main document"));
        case FILEMETATYPE_ENCLOSURE:    return(_("Enclosure"));
        case FILEMETATYPE_SIGNATURE:    return(_("Signature"));
        case FILEMETATYPE_META:         return(_("Meta document"));
        default:                        return(_("<unknown document type>"));
    }
}


/* Computes ordinal number of document identified by GUID
 * @documents is list of documents where to search
 * @id is UTF-8 encoded document ID reference
 * Return allocated array of ordinal numbers terminated by -1. Normally only
 * one document with @id exists. However ISDS does not check this higher
 * requirements and can transport message with duplicate document identifiers.
 * Therefore this function returns array of ordinals. Return NULL if error
 * occurs (e.g. memory insufficiency). */
static int *dmFileGuid2ordinar(const struct isds_list *documents,
                const char *id) {
    const struct isds_list *item;
    struct isds_document *document;
    size_t ordinars_length = 64, offset = 0;
    int *ordinars = NULL, *new_ordinars;
    int ordinar = 0;

    if (!documents || !id || !*id) return NULL;
    ordinars = malloc(ordinars_length * sizeof(*ordinars));
    if (!ordinars) return NULL;

    for (item = documents; item; item = item->next) {
        if (!item->data) continue;
        ordinar++;
        document = (struct isds_document *) (item->data);

        if (document->dmFileGuid && !strcmp(document->dmFileGuid, id)) {
            if (offset == ordinars_length) {
                /* Grow ordinals array */
                ordinars_length *= 2;
                new_ordinars = realloc(ordinars,
                        ordinars_length * sizeof(*ordinars));
                if (!new_ordinars) {
                    free(ordinars);
                    return NULL;
                }
                ordinars = new_ordinars;

            }
            ordinars[offset++] = ordinar;
        }
    }

    ordinars[offset] = -1;
    return ordinars;
}


/* @id is UTF-8 encoded GUID of referred document
 * @refernces is array of ordinal numbers if exist terminated by -1. Can be
 * NULL to signal error. */
static void format_document_reference(char *id, int *references) {
    char *buffer = NULL, *new_buffer = NULL;
    if (!id) return;

    if (references && *references != -1) {
        for (; *references > 0; references++) {
            if (!buffer) {
                shi_asprintf(&new_buffer, _("%d"), *references);
            } else {
                shi_asprintf(&new_buffer, _("%s, %d"), buffer, *references);
            }
            if (!new_buffer) {
                zfree(buffer);
                break;
            }
            buffer = new_buffer;
            new_buffer = NULL;
        }
    } else {
        char *id_locale = utf82locale(id);
        shi_asprintf(&buffer,
                _("<Reference to non-existing document ID `%s'>"), id_locale);
        free(id_locale);
    }

    if (buffer)
        print_header(_("\tRefers to"), buffer);
    else
        print_header(_("\tRefers to"), _("<Error while formatting reference>"));
    free(buffer);
}


/* Print formatted document
 * @references is ordinal number of referred document. Formally it's array
 * terminated by -1 because non-well-formed message can have more documents
 * with equal IDs */
void format_document(const struct isds_document *document, int order,
        int *references) {
    int counter_width = 3;
    char *filename_locale = NULL;

    if (!document) return;

    oprintf(_("%*d "), counter_width, order);

    if (document->dmFileDescr) {
        filename_locale = utf82locale(document->dmFileDescr);
        oprintf("%s\n", filename_locale);
        free(filename_locale);
    } else {
        oprintf(_("<Unknown file name>\n"));
    }

    if (document->is_xml) {
        char *message  =NULL;
        int nodes = 0;

        for (xmlNodePtr node = document->xml_node_list; node;
                node = node->next) nodes++; 
        shi_asprintf(&message, ngettext("%d node", "%d nodes", nodes), nodes);
        if (message) {
            print_header(_("\tXML document"), message);
            free(message);
        }
    } else {
        print_header_size(_("\tSize"), document->data_length);
    }
    print_header(_("\tMIME type"), document->dmMimeType);

    print_header(_("\tType"), DmFileMetaType2string(document->dmFileMetaType));
    print_header_utf8(_("\tMeta format"), document->dmFormat);

    print_header_utf8(_("\tID"), document->dmFileGuid);
    format_document_reference(document->dmUpFileGuid, references);
}


/* Print formatted message documents */
void format_documents(const struct isds_list *documents) {
    const struct isds_list *item;
    const struct isds_document *document;
    int i = 0;
    int *references;
    if (!documents) return;

    oprintf(_("Documents:\n"));

    for (item = documents; item; item = item->next) {
        if (!item->data) continue;
        document = (struct isds_document *) item->data;
        references = dmFileGuid2ordinar(documents, document->dmUpFileGuid);
        format_document(document, ++i, references);
        free(references);
    }
}


/* Print formatted message */
void format_message(const struct isds_message *message) {
    if (!message) return;
    format_envelope(message->envelope);
    format_documents(message->documents);
}


/* Print formatted list of message copies */
void format_copies(const struct isds_list *copies) {
    const struct isds_list *item;
    const struct isds_message_copy *copy;
    int i = 0;
    int counter_width = 3;
    if (!copies) return;

    for (item = copies; item; item = item->next) {
        if (!item->data) continue;
        copy = (struct isds_message_copy *) item->data;
        i++;

        oprintf(_("%*d Recipient:\n"), counter_width, i);
        print_header_utf8(_("\tID"), copy->dbIDRecipient);
        print_header_utf8(_("\tUnit"), copy->dmRecipientOrgUnit);
        print_header_longint(_("\tUnit number"), copy->dmRecipientOrgUnitNum);
        print_header_utf8(_("\tTo hands"), copy->dmToHands);
    }
}


void print_message_list(const struct isds_list *messages, _Bool outgoing) {
    const struct isds_list *item;
    struct isds_envelope *envelope;
    unsigned long int counter = 0;

    int counter_max, id_max = 0,  name_max = 0, subject_max = 0;
    int counter_width, id_width = 0, flags_width = 2, time_width = 6,
        name_width = 0, subject_width = 0;

    char *id_locale = NULL, flags_locale[3], *time_locale = NULL,
         *name_locale = NULL, *subject_locale = NULL;
    int screen_cols;
    int width;


    /* Compute column widths */
    rl_get_screen_size(NULL, &screen_cols);

    /* Get real maximal widths */
    for (counter = 0, item = messages; item; item = item->next) {
        if (!item->data || !((struct isds_message *)item->data)->envelope)
            continue;
        envelope = ((struct isds_message *) item->data)->envelope;
        counter++;

        width = utf8width(envelope->dmID);
        if (width > id_max) id_max = width;

        width = utf8width((outgoing) ?
                envelope->dmRecipient : envelope->dmSender);
        if (width > name_max) name_max = width;

        width = utf8width(envelope->dmAnnotation);
        if (width > subject_max) subject_max = width;
    }
    counter_max = numberwidth(counter);

    /* Correct widths to fit into window */
    if (counter_max < 0) counter_width = -3; else counter_width = counter_max;
    if (id_max < 0) id_width = -6; else id_width = id_max;
    if (name_max < 0) name_width = -20; else name_width = name_max;
    if (subject_max < 0) subject_width = -32; else subject_width = subject_max;
  
    width = abs(counter_width) + 1 + abs(id_width) + 1 + abs(flags_width) + 1 + 
            abs(time_width) + 1 + abs(name_width) + 1 + abs(subject_width);
    if (width > screen_cols) {
        width -= abs(name_width) + abs(subject_width);

        name_width = subject_width = 0;
        while (width + name_width + subject_width < screen_cols) {
            if (name_width < abs(name_max)) name_width++;
            if (subject_width < abs(subject_max)) subject_width++;
        }
        if (width + name_width + subject_width > screen_cols) subject_width--;
    }

    /* Print header */
    /* TRANSLATORS: "No" is abbreviation for "Number" in listing header. */
    onprint(pgettext("list header", "No"), counter_width);
    oprintf(" ");
    onprint(_("ID"), id_width);
    oprintf(" ");
    onprint(_("Flags"), flags_width);
    oprintf(" ");
    onprint(_("Delivered"), time_width);
    oprintf(" ");
    onprint((outgoing) ? _("To") : _("From"), name_width);
    oprintf(" ");
    onprint(_("Subject"), subject_width);
    oprintf("\n");
    for (int i = 0; i < screen_cols; i++) oprintf(_("-"));
    oprintf("\n");

    /* Print the list */
    for (counter = 0, item = messages; item; item = item->next) {
        envelope = ((struct isds_message *) item->data)->envelope;
        counter++;

        oprintf(_("%*lu "), counter_width, counter);

        if (!envelope) {
            oprintf(_("<Missing envelope>\n"));
            continue;
        }

        id_locale = utf82locale(envelope->dmID);
        name_locale = utf82locale((outgoing) ?
                envelope->dmRecipient: envelope->dmSender);
        flags_locale[0] = DmType2flag(envelope->dmType);
        flags_locale[1] = DmMessageStatus2flag(envelope->dmMessageStatus);
        flags_locale[2] = '\0';
        time_locale = timeval2shortstring(envelope->dmDeliveryTime);
        subject_locale = utf82locale(envelope->dmAnnotation);

        onprint(id_locale, id_width);
        oprintf(" ");
        onprint(flags_locale, flags_width);
        oprintf(" ");
        onprint(time_locale, time_width);
        oprintf(" ");
        onprint(name_locale, name_width);
        oprintf(" ");
        onprint(subject_locale, subject_width);
        oprintf("\n");

        zfree(id_locale);
        zfree(time_locale);
        zfree(name_locale);
        zfree(subject_locale);
    }
}


static void format_PersonName(const struct isds_PersonName *personName) {
    if (!personName) return;

    oprintf(_("Person name:\n"));
    print_header_utf8(_("\tFirst"), personName->pnFirstName);
    print_header_utf8(_("\tMiddle"), personName->pnMiddleName);
    print_header_utf8(_("\tLast"), personName->pnLastName);
    print_header_utf8(_("\tLast at birth"),
            personName->pnLastNameAtBirth);
}


static void format_BirthInfo(const struct isds_BirthInfo *birth) {
    if (!birth || !(birth->biDate || birth->biCity || birth->biCounty
                || birth->biState)) return;

    oprintf(_("Birth details:\n"));
    
    print_header_tm(_("\tDate"), birth->biDate);
    print_header_utf8(_("\tCity"), birth->biCity);
    print_header_utf8(_("\tCounty"), birth->biCounty);
    print_header_utf8(_("\tState"), birth->biState);
}


static void format_Address(const struct isds_Address *address) {
    if (!address || !(address->adCity || address->adStreet ||
                address->adNumberInStreet || address->adNumberInMunicipality ||
                address->adZipCode || address->adState)) return;

    oprintf(_("Address:\n"));
    print_header_utf8(_("\tCity"), address->adCity);
    print_header_utf8(_("\tStreet"), address->adStreet);
    print_header_utf8(_("\tNumber in street"), address->adNumberInStreet);
    print_header_utf8(_("\tNumber in municipality"),
            address->adNumberInMunicipality);
    print_header_utf8(_("\tZIP code"), address->adZipCode);
    print_header_utf8(_("\tState"), address->adState);
}


/* Return static box state string or NULL if undefined */
const char *DbState2string(const long int *state) {
    if (!state) return NULL;

    switch(*state) {
        case DBSTATE_ACCESSIBLE:        return(_("Accessible"));
        case DBSTATE_TEMP_UNACCESSIBLE: return(_("Temporary inaccessible"));
        case DBSTATE_NOT_YET_ACCESSIBLE:    return(_("Not yet accessible"));
        case DBSTATE_PERM_UNACCESSIBLE: return(_("Permanently inaccessible"));
        case DBSTATE_REMOVED:           return(_("Deleted"));
        default:                        return(_("<unknown state>"));
    }
}


void format_DbOwnerInfo(const struct isds_DbOwnerInfo *info) {
    if (!info) return;

    print_header_utf8(_("Box ID"), info->dbID);
    print_header(_("Type"), DbType2string((long int *) (info->dbType)));
    print_header_utf8(_("Subject name"), info->firmName);
    print_header_utf8(_("Identity number"), info->ic);

    format_PersonName(info->personName);
    format_BirthInfo(info->birthInfo);
    
    format_Address(info->address);

    print_header_utf8(_("Nationality"), info->nationality);
    print_header_utf8(_("E-mail"), info->email);
    print_header_utf8(_("Phone"), info->telNumber);

    print_header_utf8(_("Identifier"), info->identifier);
    print_header_utf8(_("Registry code"), info->registryCode);

    print_header(_("State"), DbState2string(info->dbState));
    print_header_bool(_("Act as public authority"), info->dbEffectiveOVM);
    print_header_bool(_("Receive commercial messages"),
            info->dbOpenAddressing);
}


/* Print formated details about a box found by a full-text */
void format_isds_fulltext_result(const struct isds_fulltext_result *info) {
    if (NULL == info) return;

    print_header_utf8(_("Box ID"), info->dbID);
    print_header(_("Type"), DbType2string((long int *)&(info->dbType)));
    print_header_utf8(_("Subject name"), info->name);
    print_header_utf8(_("Identity number"), info->ic);
    print_header_utf8(_("Address"), info->address);
    print_header_tm(_("Birth date"), info->biDate);

    print_header_bool(_("Act as public authority"), &info->dbEffectiveOVM);
    print_header_bool(_("Active"), &info->active);
    print_header_bool(_("Non-commercial message can be sent"),
            &info->public_sending);
    print_header_bool(_("Commercial message can be sent"),
            &info->commercial_sending);

}


static void format_supervising_firm(const char *ic, const char *firmName) {
    if (!ic && !firmName) return;

    oprintf(_("Supervising subject:\n"));
    print_header_utf8(_("\tIdentity number"), ic);
    print_header_utf8(_("\tName"), firmName);
}


static void format_contact_address(const char *caStreet, const char *caCity,
        const char *caZipCode, const char *caState) {
    if (!caStreet && !caCity && !caZipCode) return;

    oprintf(_("Contact address:\n"));
    print_header_utf8(_("\tStreet"), caStreet);
    print_header_utf8(_("\tCity"), caCity);
    print_header_utf8(_("\tZIP code"), caZipCode);
    print_header_utf8(_("\tState"), caState);
}


void format_DbUserInfo(const struct isds_DbUserInfo *info) {
    char *buffer;

    if (!info) return;

    print_header_utf8(_("User ID"), info->userID);
    print_header(_("Type"), UserType2string((long int *) (info->userType)));

    buffer = UserPrivils2string(info->userPrivils);
    print_header(_("Privileges"), buffer);
    zfree(buffer);

    format_PersonName(info->personName);
    format_Address(info->address);

    print_header_tm(_("Birth date"), info->biDate);
    
    format_supervising_firm(info->ic, info->firmName);
    format_contact_address(info->caStreet, info->caCity, info->caZipCode,
            info->caState);
}


/* Print formated details about message sender */
void format_sender_info(const char *dbID, const isds_sender_type *type,
        const char *raw_type, const char *name) {
    const char *type_string;

    if (!dbID && !type && !raw_type && !name) return;

    oprintf(_("Message sender:\n"));
    print_header_utf8(_("\tMessage ID"), dbID);

    type_string = isds_sender_type2string(type);
    if (type_string)
        print_header(_("\tType"), type_string);
    else
        print_header_utf8(_("\tRaw type"), raw_type);

    print_header_utf8(_("\tName"), name);
}


static const char *credit_event_type2string(
        const isds_credit_event_type type) {
    switch (type) {
        case ISDS_CREDIT_CHARGED: return(_("Credit charged"));
        case ISDS_CREDIT_DISCHARGED: return(_("Credit discharged"));
        case ISDS_CREDIT_MESSAGE_SENT: return(_("Message sent"));
        case ISDS_CREDIT_STORAGE_SET: return(_("Long-term storage set"));
        case ISDS_CREDIT_EXPIRED: return(_("Credit expired"));
        default: return(_("<Unknown credit event type>"));
    }
}


/* Print formated details about credit change event */
void format_credit_event(const struct isds_credit_event *event) {
    if (NULL == event) return;

    print_header_timeval(_("When"), event->time);
    print_header_currency(_("Credit change"), &event->credit_change);
    print_header_currency(_("Total credit"), &event->new_credit);
    print_header(_("Type"), credit_event_type2string(event->type));

    switch (event->type) {
        case ISDS_CREDIT_CHARGED:
            print_header(_("Transation ID"),
                    event->details.charged.transaction);
            break;
        case ISDS_CREDIT_DISCHARGED:
            print_header(_("Transation ID"),
                    event->details.discharged.transaction);
            break;
        case ISDS_CREDIT_MESSAGE_SENT:
            print_header(_("Message ID"),
                    event->details.message_sent.message_id);
            print_header(_("Message recipient"),
                    event->details.message_sent.recipient);
            break;
        case ISDS_CREDIT_STORAGE_SET:
            if (NULL != event->details.storage_set.old_capacity) {
                oprintf(_("Old setting:\n"));
                oprintf(ngettext("%s: %ld message\n", "%s: %ld messages\n",
                            *event->details.storage_set.old_capacity),
                        _("\tCapacity"),
                        *event->details.storage_set.old_capacity);
                print_header_tm(_("\tValid from"),
                        event->details.storage_set.old_valid_from);
                print_header_tm(_("\tValid to"),
                        event->details.storage_set.old_valid_to);
            }
            oprintf(_("New setting:\n"));
            oprintf(ngettext("%s: %ld message\n", "%s: %ld messages",
                        event->details.storage_set.new_capacity),
                    _("\tCapacity"),
                    event->details.storage_set.new_capacity);
            print_header_tm(_("\tValid from"),
                    event->details.storage_set.new_valid_from);
            print_header_tm(_("\tValid to"),
                    event->details.storage_set.new_valid_to);
            print_header_utf8(_("Initiator"),
                    event->details.storage_set.initiator);
            break;
        case ISDS_CREDIT_EXPIRED:
            break;
        default:
            oprintf(_("Details can be missing due to unkown type.\n"));
    }
}


void format_commercial_permission(
        const struct isds_commercial_permission *permission) {
    if (NULL == permission) return;

    print_header(_("Payment type"),
            isds_payment_type2string(&permission->type));
    print_header_utf8(_("Allowed recipient box ID"), permission->recipient);
    print_header_utf8(_("Payed by owner of box ID"), permission->payer);
    print_header_timeval(_("Permission expires"), permission->expiration);
    print_header_ulongint(_("Remaining messages"), permission->count);
    print_header_utf8(_("Reference to request"), permission->reply_identifier);
}

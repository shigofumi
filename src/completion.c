#define _XOPEN_SOURCE 500
#include <stdio.h>
#include <string.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <termios.h>
#include <signal.h>
#include <langinfo.h>   /* nl_langinfo(3) */
#include <sys/types.h> /* regcomp(3) */
#include <regex.h>  /* regcomp(3) */

#include "shigofumi.h"
#include <isds.h>
#include "completion.h"

static _Bool progress_started = 0;
static _Bool progress_abort_requested = 0;
static char *upload_current_formated = NULL;
static char *upload_total_formated = NULL;
static char *download_current_formated = NULL;
static char *download_total_formated = NULL;

/* Generates possible completions for base commands set
 * @text is partial user input word
 * @state is 0 for first completion attempt, non-zero otherwise
 * @return next suggested complete word or NULL if no possibility */
static char *shi_command_generator(const char *text, int state) {
    static size_t text_length, index;
    char *command_name;

    if (!state) {
        text_length = strlen(text);
        index = 0;
    }

    if (!commands) return NULL;

    while ((command_name = (*commands)[index++].name)) {
        if (!strncmp(command_name, text, text_length))
            return strdup(command_name);
    }

    return NULL;
}


/* Generates possible message ID completions
 * @text_locale is partial user input word in locale encoding
 * @state is 0 for first completion attempt, non-zero otherwise
 * @return next suggested complete word or NULL if no possibility */
static char *shi_msgid_generator(const char *text_locale, int state) {
    static char *text = NULL;
    static size_t text_length;
    static struct isds_list *item;
    char *id;

    if (!state) {
        free(text);
        text = locale2utf8(text_locale);
        if (text) text_length = strlen(text);
        item = messages;
    }

    if (text) {
        for (; item; item = item->next) {
            if (item->data && ((struct isds_message *)item->data)->envelope) {
                id = ((struct isds_message *)item->data)->envelope->dmID;
                if (id  && !strncmp(id, text, text_length)) {
                    item = item->next;
                    return utf82locale(id);
                }
            }
        }
    }

    return NULL;
}


/* Generates possible document ID completions
 * @text is partial user input word
 * @state is 0 for first completion attempt, non-zero otherwise
 * @return next suggested complete word or NULL if no possibility
 * TODO: Implement match on string representation */
static char *shi_docid_generator(const char *text, int state) {
    static struct isds_list *item;
    static int order;
    char *document_id;

    if (!state) {
        if (message)
            item = message->documents;
        else
            item = NULL;
        order = 0;
    }

    for (; item; item = item->next) {
        if (item->data) {
            item = item->next;
            order++;
            document_id = NULL;
            shi_asprintf(&document_id, "%d", order);
            return document_id;
        }
    }

    return NULL;
}


/* Generates possible box ID completions
 * @text_locale is partial user input word in locale encoding
 * @state is 0 for first completion attempt, non-zero otherwise
 * @return next suggested complete word or NULL if no possibility */
static char *shi_boxid_generator(const char *text_locale, int state) {
    static char *text = NULL;
    static size_t text_length;
    static struct isds_list *item;
    char *id;

    if (!state) {
        free(text);
        text = locale2utf8(text_locale);
        if (text) text_length = strlen(text);
        item = boxes;
    }

    if (text) {
        for (; item; item = item->next) {
            if (item->data && ((struct isds_DbOwnerInfo *)item->data)->dbID) {
                id = ((struct isds_DbOwnerInfo *)item->data)->dbID;
                if (id  && !strncmp(id, text, text_length)) {
                    item = item->next;
                    return utf82locale(id);
                }
            }
        }
    }

    return NULL;
}


struct command *find_command(const char *text) {
    int index = 0;
    if (!text || !commands) return NULL;

    for (index = 0; (*commands)[index].name; index++) {
        if (!strcmp((*commands)[index].name, text))
            return &(*commands)[index];
    }

    return NULL;
}


arg_type find_arg_type(const char *line) {
    int index;
    size_t length;

    if (!line || !commands) return ARGTYPE_NONE;

    for (index = 0; (*commands)[index].name; index++) {
        length = strlen((*commands)[index].name);
        if (rl_point >= length &&
                !strncmp(line, (*commands)[index].name, length) &&
                isspace(line[length]))
            return (*commands)[index].arg;
    }

    return ARGTYPE_NONE;
}


static char **shi_completion_none(const char *text, int start, int end) {
    rl_attempted_completion_over = 1;
    return NULL;
}

/* Readline completion hook
 * @text is a word to complete
 * @start is index of begining of @text in rl_line_buffer (counts from 0)
 * @end is index of cursor in rl_line_buffer (character after end of @text)
 * @return dynamicly allocated array of possible completions or NULL if none
 * @side-effect set rl_attempted_completion_over to disable (0) file name
 * completion */
static char **shi_completion_command(const char *text, int start, int end) {
    rl_attempted_completion_over = 1;

#if ENABLE_DEBUG
    fprintf(stderr, "DEBUG: shi_completion_command(): "
            "text=<%s>, start=%d, end=%d, rl_line_buffer=<%s>, "
            "rl_completion_found_quote=%d, rl_completion_quote_character=<%c> "
            "rl_filename_quoting_desired=%d, rl_filename_quoting_function=%p "
            "(shi_quote_filename=%p)\n",
            text, start, end, rl_line_buffer,
            rl_completion_found_quote, rl_completion_quote_character,
            rl_filename_quoting_desired, rl_filename_quoting_function,
            shi_quote_filename);
#endif

    if (!start)
        /* Command */
        return rl_completion_matches(text, shi_command_generator);

    /* Command argument */
    switch (find_arg_type(rl_line_buffer)) {
        case ARGTYPE_COMMAND:
            return rl_completion_matches(text, shi_command_generator);
        case ARGTYPE_FILE:
            rl_attempted_completion_over = 0;
            break;
        case ARGTYPE_MSGID:
            return rl_completion_matches(text, shi_msgid_generator);
        case ARGTYPE_DOCID:
            return rl_completion_matches(text, shi_docid_generator);
        case ARGTYPE_BOXID:
            return rl_completion_matches(text, shi_boxid_generator);
        default:
            rl_attempted_completion_over = 1;
    }
    return NULL;
}


/* Compares two command names */
int commandcmp(const void *a, const void *b) {
    if (!a) return 1;
    return strcoll(((struct command *)a)->name, ((struct command *)b)->name);
}


/* Make list of currently available commands */
static int build_command_list(const struct command unsorted_commands[]) {
    int count, i;

    for (count = 0; unsorted_commands[count].name; count++);

    zfree(commands);
    commands = calloc(count + 1, sizeof(struct command));
    if (!commands) {
        fprintf(stderr,
                _("Fatal error: Non enough memory to sort commands\n"));
        return -1;
    }

    for (i = 0; i < count; i++) {
        (*commands)[i].name = unsorted_commands[i].name;
        (*commands)[i].function = unsorted_commands[i].function;
        (*commands)[i].description = unsorted_commands[i].description;
        (*commands)[i].usage = unsorted_commands[i].usage;
        (*commands)[i].arg = unsorted_commands[i].arg;
    }

    qsort(*commands, count, sizeof(struct command), commandcmp);

    return 0;
}


/* Switch completion function */
int select_completion(const completion_type completion) {
    switch (completion) {
        case COMPL_NONE:
            rl_attempted_completion_function = shi_completion_none;
            break;
        case COMPL_COMMAND:
            if (build_command_list(base_commands)) return -1;
            rl_attempted_completion_function = shi_completion_command;
            break;
        case COMPL_MSG:
            if (build_command_list(message_commands)) return -1;
            rl_attempted_completion_function = shi_completion_command;
            break;
        case COMPL_LIST:
            if (build_command_list(list_commands)) return -1;
            rl_attempted_completion_function = shi_completion_command;
            break;
    }
    return 0;
}


/* Free list of chars recursively */
void argv_free(char **argv) {
    if (argv) {
        for (char **arg = argv; *arg; arg++)
            free(*arg);
        free(argv);
    }
}


#define ESCAPER '\\'
/* Decides whether character at @index offset of @text is quoted */
int shi_char_is_quoted(char *text, int index) {
    int i;
    _Bool escaped = 0;

#if ENABLE_DEBUG
    fprintf(stderr, "shi_char_is_quoted(text=%s, index=%d)\n", text, index);
#endif
    if (!text || index < 0) return 0;

    for (i = 0; i <= index; i++) {
        if (text[i] == ESCAPER && !escaped) {
            escaped = 1;
            continue;
        }
        escaped = 0;
    }

    return escaped;
}


/* Escapes file name */
char *shi_quote_filename(char *text, int match_type, char *quote_pointer) {
    char *quoted_text = NULL;
    int i;

#if ENABLE_DEBUG
    fprintf(stderr,
            "shi_quote_filename(text=%s, match_type=%d, quote_pointer='%c')\n",
            text, match_type, (quote_pointer)?*quote_pointer:0);
#endif
    if (!text) return NULL;

    quoted_text = malloc(strlen(text) * 2 + 1);
    if (!quoted_text) return strdup(text);

    for (i = 0; *text; text++, i++) {
        if (*text == ESCAPER) quoted_text[i++] = ESCAPER;
        if (isspace(*text)) quoted_text[i++] = ESCAPER;
        quoted_text[i] = *text;
    }
    quoted_text[i] = '\0';
    
    return quoted_text;
}


/* Deescapes file name */
char *shi_dequote_filename(char *text, int quote_char) {
    _Bool escaped;
    char *unquoted_text = NULL;
    int i;

#if ENABLE_DEBUG
    fprintf(stderr,
            "shi_dequote_filename(text=%s, quote_char=%d)\n",
            text, quote_char);
#endif
    if (!text) return NULL;

    unquoted_text = malloc(strlen(text) + 1);
    if (!unquoted_text) return strdup(text);

    for (escaped = 0, i = 0; *text; text++) {
        if (*text == ESCAPER && !escaped) {
            escaped = 1;
            continue;
        }
        escaped = 0;

        unquoted_text[i++] = *text;
    }
    unquoted_text[i] = '\0';

    return unquoted_text;
}


#define MAX_TOKENS 256
#define PIPE '|'
#define QUOTE '"'
/* Split string into tokens. Backslash escapes following space or double quote.
 * Double quote escapes white spaces until next double quote.
 * @command_line is line to parse
 * @argc outputs number of parsed arguments
 * @shell is optional automatically reallocated shell command following pipe
 * in @command_line
 * @return NULL-terminated array of tokens or NULL in case of error */
char **tokenize(const char *command_line, int *argc, char **shell) {
    char **argv = NULL;
    const char *start, *end;
    _Bool escaped = 0, quoted = 0;
    char *target;
    char *home = getenv("HOME");

    if (!argc) return NULL;
    *argc = 0;

    if (!command_line) return NULL;

    if (!shell) return NULL;
    zfree(*shell);

    argv = calloc(MAX_TOKENS, sizeof(*argv));
    if (!argv) {
        fprintf(stderr, _("Not enough memory to tokenize command line\n"));
        goto error;
    }

    for (start = command_line; *start; start++) {
        if (isspace(*start)) continue;
        if (*start == PIPE) break;

        if (*argc >= MAX_TOKENS - 1) {
            fprintf(stderr, _("To much arguments\n"));
            goto error;
        }

        /* Locate token boundaries */
        escaped = quoted = 0;
        for (end = start; *end; end++) {
            if (*end == ESCAPER && !escaped) {
                escaped = 1;
                continue;
            }
            if (*end == QUOTE && !escaped) {
                quoted = !quoted;
                continue;
            }
            if ((isspace(*end) || *end == PIPE ) && !escaped && !quoted) break;
            if (escaped) escaped = 0;
        }

        /* Allocate memory and expand first tilde to $HOME */
        if (*start == '~' && home) {
            size_t home_length = strlen(home);
            start++;
            argv[*argc] = malloc(home_length + end-start + 1);
            if (argv[*argc]) {
                strcpy(argv[*argc], home);
                target = argv[*argc] + home_length;
            }
        } else {
            argv[*argc] = malloc(end-start + 1);
            target = argv[*argc];
        }
        if (!argv[*argc]) {
            fprintf(stderr, _("Not enough memory to tokenize command line\n"));
            goto error;
        }

        /* Copy unquoted token */
        for (escaped = quoted = 0; start < end; start++) {
            if (*start == ESCAPER && !escaped) {
                escaped = 1;
                continue;
            }
            if (*start == QUOTE && !escaped) {
                quoted = !quoted;
                continue;
            }
            escaped = 0;
            *target++ = *start;
        }
        *target = '\0';

        (*argc)++;
        start = end;
        if (!*start || *start == PIPE) break;
    }

    /* Copy shell command */
    if (*start == PIPE) {
        start++;
        size_t length = strlen(start);
        
        if (length > 0) {
            *shell = malloc(length + 1);
            if (!*shell) goto error;
            strcpy(*shell, start);
        }
    }

    return argv;

error:
    argv_free(argv);
    zfree(*shell);
    return NULL;
}

#undef MAX_TOKENS
#undef PIPE
#undef QUOTE
#undef ESCAPER

/* Add line into history if not NULL or empty string */
void shi_add_history(const char *line) {
    if (line && *line) add_history(line);
}


/* Ask user for a password */
char *ask_for_password(const char *prompt) {
    struct termios terminal;
    int tcerr;
    char *password = NULL;

    tcerr = tcgetattr(fileno(stdin), &terminal);
    if (tcerr) {
        fprintf(stderr, "Could not get terminal characteristics\n");
    } else {
        /* TODO: handle SIGINT to restore echo */
        terminal.c_lflag &= ~ECHO;
        tcerr = tcsetattr(fileno(stdin), TCSAFLUSH, &terminal);
        if (tcerr) {
            fprintf(stderr, "Could not switch off local echo\n");
        }
    }
    if (tcerr) {
        fprintf(stderr, "Password will be visible on your terminal\n");
    }

    password = readline(prompt);

    if (!tcerr) {
        terminal.c_lflag |= ECHO;
        tcerr = tcsetattr(fileno(stdin), TCSAFLUSH, &terminal);
        if (tcerr) {
            fprintf(stderr, "Could not switch on local echo\n");
        }
        printf("\n");
    }

    return password;
}


/* Prompt user and supply default value if user does input nothing. Original
 * default value can be deallocated in this function. If *@value is NULL, use
 * as default read-only @backup_value. You can always free *@value. * */
void shi_ask_for_string(char **value, const char *prompt,
        const char *backup_value, _Bool batch_mode) {
    char *answer = NULL;

    if (!value) return;

    shi_add_history(backup_value);
    shi_add_history(*value);

    if (!*value && backup_value) *value = strdup(backup_value);

    if (batch_mode) {
        if (prompt) printf(_("%s%s\n"), prompt,
                (*value) ? *value : _("<Empty value>"));
    } else {
        if (*value) printf(_("Default value: %s\n"), *value);
        answer = readline(prompt);
        if (answer && answer[0] == '\0') zfree(answer);

        if (answer) {
            zfree(*value);
            *value = answer;
            return;
        }
        if (*value) printf(_("Using default value `%s'.\n"), *value);
    }
    return;
}


/* Prompt user for password and supply default value if user does input
 * nothing. Original default value can be deallocated in this function. If
 * *@value is NULL, use as default read-only @backup_value. You can always
 * free *@value. * */
void shi_ask_for_password(char **value, const char *prompt,
        const char *backup_value, _Bool batch_mode) {
    char *answer = NULL;

    if (!value) return;

    if (!*value && backup_value) *value = strdup(backup_value);

    if (batch_mode) {
        if (prompt) printf(_("%s%s\n"), prompt,
                (*value) ? _("<Password provided>") : _("<Empty value>"));
    } else {
        if (*value) printf(_("Default password exists\n"));
        answer = ask_for_password(prompt);
        if (answer && answer[0] == '\0') zfree(answer);

        if (answer) {
            zfree(*value);
            *value = answer;
            return;
        }
        if (*value) printf(_("Using default value.\n"));
    }
    return;
}


/* Ask Yes-No question.
 * @question to ask the user
 * @default_value specifies default answer if user puts nothing. True is for
 * Yes, false is for No.
 * @batch_mode is true for non-interctive mode, true for interactive
 * @return true for yes, false for no. */
_Bool shi_ask_yes_no(const char *question, _Bool default_value,
        _Bool batch_mode) {
    _Bool answer = default_value;
    const char *choices = (default_value) ? _("Y/n") : _("y/N");
    const char *yes_expr, *no_expr;
    regex_t yes_compiled, no_compiled;
    char *input = NULL;
    char *prompt = NULL;
    int retval;

    if (-1 == shi_asprintf(&prompt,
            (question == NULL)? _("[%2$s]> ") : _("%1$s [%2$s]> "),
            question, choices)) {
        fprintf(stderr, _("Could not format yes-no prompt\n"));
        goto leave;
    }

    if (batch_mode) {
        printf("%s%s\n", prompt, (answer) ? _("Yes") : _("No") );
        goto leave;
    }
    
    yes_expr = nl_langinfo(YESEXPR);
    no_expr = nl_langinfo(NOEXPR);
    if (NULL == yes_expr || !strcmp(yes_expr, "")) yes_expr = "^[yY].*";
    if (NULL == no_expr || !strcmp(no_expr, "")) no_expr = "^[nN].*";
    memset(&yes_compiled, 0, sizeof(yes_compiled));
    memset(&no_compiled, 0, sizeof(no_compiled));
    if (regcomp(&yes_compiled, yes_expr, REG_EXTENDED|REG_NOSUB)) {
        fprintf(stderr, _("Error while compiling regular expression for "
                    "affirmation\n"));
        goto refree;
    }
    if (regcomp(&no_compiled, no_expr, REG_EXTENDED|REG_NOSUB)) {
        fprintf(stderr, _("Error while compiling regular expression for "
                    "negation\n"));
        goto refree;
    }

    while (1) {
        input = readline(prompt);
        if (input == NULL || input[0] == '\0') break;

        retval = regexec(&yes_compiled, input, 0, NULL, 0);
        if (0 == retval) {
            answer = 1;
            break;
        } else if (REG_NOMATCH != retval) {
            fprintf(stderr, _("Error while matching answer for affirmation\n"));
            break;
        }
        retval = regexec(&no_compiled, input, 0, NULL, 0);
        if (0 == retval) {
            answer = 0;
            break;
        } else if (REG_NOMATCH != retval) {
            fprintf(stderr, _("Error while matching answer for negation\n"));
            break;
        }
        printf(_("Unrecognized answer. Affirmation must match `%s', "
                    "negation must match `%s'.\n"), yes_expr, no_expr);
        free(input);
    }
    free(input);

refree:
    regfree(&yes_compiled);
    regfree(&no_compiled);

leave:
    free(prompt);
    return answer;
}


static void shi_format_size(char **buffer, double value) {
    if (value <= 0) {
        shi_asprintf(buffer, _("---"));
    } else if (value < (1<<10)) {
        shi_asprintf(buffer, _("%0.2f B"), value);
    } else if (value < (1<<20)) {
        shi_asprintf(buffer, _("%0.2f KiB"), value/(1<<10));
    } else {
        shi_asprintf(buffer, _("%0.2f MiB"), value/(1<<20));
    }
}


/* Signal handler aborting ISDS transfer */
static void shi_abort_transfer(int signo) {
    if (progress_started) progress_abort_requested = 1;
}


/* This is ISDS context network progress call back. It prints progress meter
 * and allows user to abort current network transfer. */
int shi_progressbar(double upload_total, double upload_current,
        double download_total, double download_current, void *data) {
    if (!progress_started) {
        progress_started = 1;
        progress_abort_requested = 0;
        signal(SIGINT, shi_abort_transfer);
    }
    
    shi_format_size(&upload_current_formated, upload_current);
    shi_format_size(&upload_total_formated, upload_total);
    shi_format_size(&download_current_formated, download_current);
    shi_format_size(&download_total_formated, download_total);

    printf("\r");
    printf(_("Progress: uploaded %s/%s, downloaded %s/%s"),
         upload_current_formated, upload_total_formated,
         download_current_formated, download_total_formated);

    return progress_abort_requested;
}


/* Finish progress meter output. */
void shi_progressbar_finish(void) {
    if (progress_started) {
        progress_started = 0;
        /* Remove progress bar info */
        /* FIXME: Progress bar can be longer than summary. We must gather
         * longest tainted column in shi_progressbar() and blank only that
         * necessary width. */
        printf("\r                                            "
                "                                   ");
        printf("\r");
        printf(_("Transfer summary: uploaded %s, downloaded %s\n"),
             upload_current_formated, download_current_formated);
    }
}

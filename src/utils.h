#ifndef __ISDS_UTILS_H__
#define __ISDS_UTILS_H__

#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>

/* _hidden macro marks library private symbols. GCC can exclude them from global
 * symbols table */
#if defined(__GNUC__) && (__GNUC__ >= 4)
#define _hidden __attribute__((visibility("hidden")))
#else
#define _hidden
#endif

/* PANIC macro aborts current process without any clean up.
 * Use it as last resort fatal error solution */
#define PANIC(message) { \
    if (stderr != NULL ) fprintf(stderr, \
            "LIBISDS PANIC (%s:%d): %s\n", __FILE__, __LINE__, (message)); \
    abort(); \
}

/* Concatenate two strings into newly allocated buffer.
 * You must free() them, when you don't need it anymore.
 * Any of the arguments can be NULL meaning empty string.
 * In case of error returns NULL.
 * Empty string is always returned as allocated empty string. */
char *astrcat(const char *first, const char *second);

/* Concatenate three strings into newly allocated buffer.
 * You must free() them, when you don't need it anymore.
 * Any of the arguments can be NULL meaning empty string.
 * In case of error returns NULL.
 * Empty string is always returned as allocated empty string. */
char *astrcat3(const char *first, const char *second,
        const char *third);

/* Print formatted string into automatically reallocated @buffer.
 * @buffer automatically reallocated buffer. Must be &NULL or preallocated
 * memory.
 * @format format string as for printf(3)
 * @ap list of variadic arguments, after call will be in undefined state
 * @Returns number of bytes printed. In case of error, -1 and NULL @buffer*/
int shi_vasprintf(char **buffer, const char *format, va_list ap);

/* Print formatted string into automatically reallocated @buffer.
 * @buffer automatically reallocated buffer. Must be &NULL or preallocated
 * memory.
 * @format format string as for printf(3)
 * @... variadic arguments
 * @Returns number of bytes printed. In case of error, -1 and NULL @buffer*/
int shi_asprintf(char **buffer, const char *format, ...);

/* Converts UTF8 string into locale encoded string.
 * @utf string int UTF-8 terminated by zero byte
 * @return allocated string encoded in locale specific encoding. You must free
 * it. In case of error or NULL @utf returns NULL. */
char *utf82locale(const char *utf);

/* Converts locale encoded string into UTF8.
 * @locale string in locale encoded string terminated by zero byte
 * @return allocated string encoded in UTF-8 encoding. You must free
 * it. In case of error or NULL @locale returns NULL. */
char *locale2utf8(const char *locale);

/* Determine how many columns occupies given UTF-8 encoded string.
 * Return number of columns or -1 */
int utf8width(const char *string);

/* Determine how many columns occupies given number.
 * Return number of columns or -1 */
int numberwidth(const size_t number);

/* Print locale-encoded string occupying exactly given terminal width */
void fnprint(FILE *stream, const char *locale_string, int width);

/* Print locale-encoded string occupying at lest given terminal width.
 * If the string is shorter, it will pad the string with spaces.
 * If the string is wider, it will move cursor to next line on column with
 * given width. */
void fhprint(FILE *stream, const char *locale_string, size_t width);

/* Switches time zone to UTC.
 * XXX: This is not reentrant and not thread-safe */
void switch_tz_to_utc(void);

/* Switches time zone to original value.
 * XXX: This is not reentrant and not thread-safe */
void switch_tz_to_native(void);

/* Free() and set to NULL pointed memory */
#define zfree(memory) do { \
    free(memory); \
    (memory) = NULL; \
} while (0)

#endif

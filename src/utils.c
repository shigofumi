#define _XOPEN_SOURCE 500 /* For strdup(3) */
#define _POSIX_C_SOURCE 200112L /* For setenv() */
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <iconv.h>
#include <langinfo.h>
#include <time.h>
#include <wchar.h>
#include <errno.h>

#include "utils.h"

char *tz_orig; /* Copy of original TZ variable */

/* Concatenate two strings into newly allocated buffer.
 * You must free() them, when you don't need it anymore.
 * Any of the arguments can be NULL meaning empty string.
 * In case of error returns NULL.
 * Empty string is always returned as allocated empty string. */
_hidden char *astrcat(const char *first, const char *second) {
    size_t first_len, second_len;
    char *buf;
    
    first_len = (first) ? strlen(first) : 0;
    second_len = (second) ? strlen(second) : 0;
    buf = malloc(1 + first_len + second_len);
    if (buf) {
        buf[0] = '\0';
        if (first) strcpy(buf, first);
        if (second) strcpy(buf + first_len, second);
    }
    return buf;
}


/* Concatenate three strings into newly allocated buffer.
 * You must free() them, when you don't need it anymore.
 * Any of the arguments can be NULL meaning empty string.
 * In case of error returns NULL.
 * Empty string is always returned as allocated empty string. */
_hidden char *astrcat3(const char *first, const char *second,
        const char *third) {
    size_t first_len, second_len, third_len;
    char *buf, *next;
    
    first_len = (first) ? strlen(first) : 0;
    second_len = (second) ? strlen(second) : 0;
    third_len = (third) ? strlen(third) : 0;
    buf = malloc(1 + first_len + second_len + third_len);
    if (buf) {
        buf[0] = '\0';
        next = buf;
        if (first) {
            strcpy(next, first);
            next += first_len;
        }
        if (second) {
            strcpy(next, second);
            next += second_len;
        }
        if (third) {
            strcpy(next, third);
        }
    }
    return buf;
}


/* Print formatted string into automatically reallocated @buffer.
 * @buffer automatically reallocated buffer. Must be &NULL or preallocated
 * memory.
 * @format format string as for printf(3)
 * @ap list of variadic arguments, after call will be in undefined state
 * @Returns number of bytes printed. In case of error, -1 and NULL @buffer*/
_hidden int shi_vasprintf(char **buffer, const char *format, va_list ap) {
    va_list aq;
    int length, new_length;
    char *new_buffer;

    if (!buffer || !format) {
        if (buffer) {
            free(*buffer);
            *buffer = NULL;
        }
        return -1;
    }

    va_copy(aq, ap);
    length = vsnprintf(NULL, 0, format, aq) + 1;
    va_end(aq);
    if (length <= 0) {
        free(*buffer);
        *buffer = NULL;
        return -1;
    }

    new_buffer = realloc(*buffer, length);
    if (!new_buffer) {
        free(*buffer);
        *buffer = NULL;
        return -1;
    }
    *buffer = new_buffer;

    new_length = vsnprintf(*buffer, length, format, ap);
    if (new_length >= length) {
        free(*buffer);
        *buffer = NULL;
        return -1;
    }

    return new_length;
}


/* Print formatted string into automatically reallocated @buffer.
 * @buffer automatically reallocated buffer. Must be &NULL or preallocated
 * memory.
 * @format format string as for printf(3)
 * @... variadic arguments
 * @Returns number of bytes printed. In case of error, -1 and NULL @buffer*/
_hidden int shi_asprintf(char **buffer, const char *format, ...) {
    int ret;
    va_list ap;
    va_start(ap, format);
    ret = shi_vasprintf(buffer, format, ap);
    va_end(ap);
    return ret;
}


/* Converts UTF8 string into locale encoded string.
 * @utf string in UTF-8 terminated by zero byte
 * @return allocated string encoded in locale specific encoding. You must free
 * it. In case of error or NULL @utf returns NULL. */
_hidden char *utf82locale(const char *utf) {
    iconv_t state;
    size_t utf_length;
    char *buffer = NULL, *new_buffer;
    size_t buffer_length = 0, buffer_used = 0;
    char *inbuf, *outbuf;
    size_t inleft, outleft;

    if (!utf) return NULL;

    /* nl_langinfo() is not thread-safe */
    state = iconv_open(nl_langinfo(CODESET), "UTF-8");
    if (state == (iconv_t) -1) return NULL;

    /* Get the initial output buffer length */
    utf_length = strlen(utf);
    buffer_length = utf_length + 1;

    inbuf = (char *) utf;
    inleft = utf_length + 1;

    while (inleft > 0) {
        /* Extend buffer */
        new_buffer = realloc(buffer, buffer_length);
        if (!new_buffer) {
            free(buffer);
            buffer = NULL;
            goto leave;
        }
        buffer = new_buffer;

        /* FIXME */
        outbuf = buffer + buffer_used;
        outleft = buffer_length - buffer_used;
        
        /* Convert chunk of data */
        if ((size_t) -1 == iconv(state, &inbuf, &inleft, &outbuf, &outleft) &&
                errno != E2BIG) {
            free(buffer);
            buffer = NULL;
            goto leave;
        }

        /* Update positions */
        buffer_length += 1024;
        buffer_used = outbuf - buffer;
    }

leave:
    iconv_close(state);
    return buffer;
}


/* Converts locale encoded string into UTF8.
 * @locale string in locale encoded string terminated by zero byte
 * @return allocated string encoded in UTF-8 encoding. You must free
 * it. In case of error or NULL @locale returns NULL. */
_hidden char *locale2utf8(const char *locale) {
    iconv_t state;
    size_t locale_length;
    char *buffer = NULL, *new_buffer;
    size_t buffer_length = 0, buffer_used = 0;
    char *inbuf, *outbuf;
    size_t inleft, outleft;

    if (!locale) return NULL;

    /* nl_langinfo() is not thread-safe */
    state = iconv_open("UTF-8", nl_langinfo(CODESET));
    if (state == (iconv_t) -1) return NULL;

    /* Get the initial output buffer length */
    locale_length = strlen(locale);
    buffer_length = locale_length + 1;

    inbuf = (char *) locale;
    inleft = locale_length + 1;

    while (inleft > 0) {
        /* Extend buffer */
        new_buffer = realloc(buffer, buffer_length);
        if (!new_buffer) {
            free(buffer);
            buffer = NULL;
            goto leave;
        }
        buffer = new_buffer;

        /* FIXME */
        outbuf = buffer + buffer_used;
        outleft = buffer_length - buffer_used;
        
        /* Convert chunk of data */
        if ((size_t) -1 == iconv(state, &inbuf, &inleft, &outbuf, &outleft) &&
                errno != E2BIG) {
            zfree(buffer);
            goto leave;
        }

        /* Update positions */
        buffer_length += 1024;
        buffer_used = outbuf - buffer;
    }

leave:
    iconv_close(state);
    return buffer;
}


/* Determine how many columns occupies given locale encoded string.
 * Return number of columns or -1 */
static int localewidth(const char *string) {
    mbstate_t mbstate;
    wchar_t wide_char;
    int wchar_width;
    int offset = 0, columns = 0;
    size_t wc_size;
    size_t length;

    if (!string || !*string) return 0;

    memset(&mbstate, 0, sizeof(mbstate));
    length = strlen(string);

    /* Step on each multibyte character */
    for (offset = 0; offset < length;) {
        wc_size = mbrtowc(&wide_char, string + offset, length, &mbstate);
        if (wc_size > 0) {
            wchar_width = wcwidth(wide_char);
            if (wchar_width < 0) break; /* Non-printable character? */ 

            columns += wchar_width;
            offset += wc_size;
        } else break; /* L'\0' or incomplete multibyte character */
    }

    return columns;
}


/* Determine how many columns occupies given UTF-8 encoded string.
 * Return number of columns or -1 */
int utf8width(const char *string) {
    char *locale_string;
    size_t columns = 0;

    if (!string || !*string) return 0;

    locale_string = utf82locale(string);
    if (!locale_string) return -1;

    columns = localewidth(locale_string);

    free(locale_string);
    return columns;
}


/* Determine how many columns occupies given number.
 * Return number of columns or -1 */
int numberwidth(const size_t number) {
    int columns;
    char *buffer = NULL;
   
    shi_asprintf(&buffer, "%zu", number);
    if (!buffer) return -1;

    columns = localewidth(buffer);

    free(buffer);
    return columns;
}


/* Print locale-encoded string occupying exactly given terminal width.
 * @width is desired column number. If negative,  do not limit the width, but
 * fill to spaces to absolute width if string is shorter */
void fnprint(FILE *stream, const char *locale_string, int width) {
    int abs_width = abs(width);
    mbstate_t mbstate;
    wchar_t wide_char;
    int wchar_width;
    int offset = 0, columns = 0;
    size_t wc_size;
    size_t locale_length;

    if (!locale_string) {
        /* FIXME: a space can occupy more than one column */
        for (columns = 0; columns < abs_width; columns++) fprintf(stream, " ");
        return;
    }

    memset(&mbstate, 0, sizeof(mbstate));
    locale_length = strlen(locale_string);

    /* Step on each multibyte character */
    for (offset = 0; offset < locale_length;) {
        wc_size = mbrtowc(&wide_char, locale_string + offset, locale_length,
                &mbstate);
        if (wc_size > 0) {
            wchar_width = wcwidth(wide_char);
            if (wchar_width < 0) break; /* Non-printable character? */ 
            
            if (columns + wchar_width > abs_width)
                /* This character overflows desired display width */
                break;

            columns += wchar_width;
            offset += wc_size;
        } else break; /* L'\0' or incomplete multibyte character */
    }

    /* Print fitting prefix of locale_string */
    if (width >= 0) 
        fprintf(stream, "%.*s", offset, locale_string);
    else
        fprintf(stream, "%s", locale_string);

    /* And pad to width spaces  */
    /* FIXME: a space can occupy more than one column */
    while (columns < abs_width) {
        fprintf(stream, " ");
        columns++;
    }
}


/* Print locale-encoded string occupying at lest given terminal width.
 * If the string is shorter, it will pad the string with spaces.
 * If the string is wider, it will move cursor to next line on column with
 * given width. */
void fhprint(FILE *stream, const char *locale_string, size_t width) {
    mbstate_t mbstate;
    wchar_t wide_char;
    int wchar_width;
    size_t offset = 0, columns = 0;
    size_t wc_size;
    size_t locale_length;

    if (!locale_string) return;

    memset(&mbstate, 0, sizeof(mbstate));
    locale_length = strlen(locale_string);

    /* Step on each multibyte character */
    for (offset = 0; offset < locale_length;) {
        wc_size = mbrtowc(&wide_char, locale_string + offset, locale_length,
                &mbstate);
        if (wc_size > 0) {
            wchar_width = wcwidth(wide_char);
            if (wchar_width < 0) break; /* Non-printable character? */ 
            
            if (columns + wchar_width > width)
                /* This character overflows desired display width */
                break;

            columns += wchar_width;
            offset += wc_size;
        } else break; /* L'\0' or incomplete multibyte character */
    }

    /* Print fitting prefix of locale_string */
    fprintf(stream, "%s", locale_string);

    /* FIXME: a space can occupy more than one column */
    if (columns < width) {
        /* And pad with spaces to width */
        while (columns < width) {
            fprintf(stream, " ");
            columns++;
        }
    } else {
        /* Shift to next line */
        fprintf(stream, "\n");
        for (columns = 0; columns < width; columns++) {
            fprintf(stream, " ");
        }
    }
}


/* Switches time zone to UTC.
 * XXX: This is not reentrant and not thread-safe */
_hidden void switch_tz_to_utc(void) {
    char *tz;

    tz = getenv("TZ");
    if (tz) {
        tz_orig = strdup(tz);
        if (!tz_orig) 
            PANIC("Can not back original time zone up");
    } else {
        tz_orig = NULL;
    }

    if (setenv("TZ", "", 1))
            PANIC("Can not change time zone to UTC temporarily");

    tzset();
}


/* Switches time zone to original value.
 * XXX: This is not reentrant and not thread-safe */
_hidden void switch_tz_to_native(void) {
    if (tz_orig) {
        if (setenv("TZ", tz_orig, 1))
            PANIC("Can not restore time zone by setting TZ variable");
        free(tz_orig);
        tz_orig = NULL;
    } else {
        if(unsetenv("TZ"))
            PANIC("Can not restore time zone by unsetting TZ variable");
    }
    tzset();
}

#include "test.h"
#include <completion.h>
#include <isds.h>
#include <string.h>

/* Copy from shigofumi.c until sources will be split properly */
struct command base_commands[] = {
    { NULL, NULL, NULL, NULL, ARGTYPE_NONE }
};
struct command message_commands[] ={
    { NULL, NULL, NULL, NULL, ARGTYPE_NONE }
};
struct command list_commands[] = {
    { NULL, NULL, NULL, NULL, ARGTYPE_NONE }
};
struct command (*commands)[] = NULL;
char *prompt = NULL;
struct isds_list *boxes = NULL;
struct isds_message *message = NULL;
struct isds_list *messages = NULL;


static int test_completion(const void *line, const char **correct_argv,
        int correct_argc, const char *correct_shell) {
    char *shell = NULL;
    char **argv;
    int argc;

    argv = tokenize(line, &argc, &shell); 

    if ((correct_argv != NULL && argv == NULL) ||
            (correct_argv == NULL && argv != NULL)) {
        FAIL_TEST("Wrong returned value");
    }
    if (argc != correct_argc) {
        FAIL_TEST("Wrong argc: expected=%d, returned=%d", correct_argc, argc);
    }
    TEST_STRING_DUPLICITY(correct_shell, shell);
    for (int i = 0; i != argc; i++) {
        TEST_STRING_DUPLICITY(correct_argv[i], argv[i]);
    }

    free(shell);
    PASS_TEST;
}

int main(int argc, char **argv) {
    const char *line[] = {
        "a",
        "a b",
        "a\\ b",
        "a \\ b",
        "a \\ b\\ ",
        "\\ ",
        "a|b",
        "a\\|b",
        "\"a b\"",
        "\"\\\"\"",
        "\\ \"a \"",
        "\"a | b\"",
        "\"a | b\"|c",
    };
    const char *arg_values[][3] = {
        { "a", NULL },
        { "a", "b", NULL },
        { "a b", NULL },
        { "a", " b", NULL },
        { "a", " b ", NULL },
        { " ", NULL },
        { "a", NULL },
        { "a|b", NULL },
        { "a b", NULL },
        { "\"", NULL },
        { " a ", NULL },
        { "a | b", NULL},
        { "a | b", NULL},
    };
    int arg_counter[] = {
        1,
        2,
        1,
        2,
        2,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
    };
    const char *shell[] = {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        "b",
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        "c"
    };

    INIT_TEST("completion");

    for (int i = 0; i < sizeof(line)/sizeof(line[0]); i++) {
        TEST(line[i], test_completion,
                line[i], arg_values[i], arg_counter[i], shell[i]);
    }

    SUM_TEST();
}

PARAMS = --stringparam "man.justify" 1
PARAMS += --stringparam "man.hyphenate" 1
STYLE = @DOCBOOK_STYLE@/manpages/docbook.xsl
.PHONY: checkspell man-clean validate install-man1 install-man5

man_SRC = shigofumi.xml
man_LLMANS = shigofumi.1 shigofumirc.5
EXTRA_DIST = $(man_SRC) $(man_LLMANS)

if BUILD_NLS
validate: $(man_SRC)
	xmllint --valid --noout $<

checkspell: $(man_SRC)
	aspell --lang=$(LL) --mode=sgml list < $< | sort -u > badlist

all: $(man_LLMANS)

install: install-man1 install-man5

install-man1: shigofumi.1
	install -m 755 -d $(DESTDIR)$(mandir)/$(LL)/man1
	install -m 644 -t $(DESTDIR)$(mandir)/$(LL)/man1 $<

install-man5: shigofumirc.5
	install -m 755 -d $(DESTDIR)$(mandir)/$(LL)/man5
	install -m 644 -t $(DESTDIR)$(mandir)/$(LL)/man5 $<

uninstall: uninstall-man1 uninstall-man5

uninstall-man1: shigofumi.1
	cd $(DESTDIR)$(mandir)/$(LL)/man1/ && rm -- $<

uninstall-man5: shigofumirc.5
	cd $(DESTDIR)$(mandir)/$(LL)/man5 && rm -- $<

clean-local:
	@rm -- $(man_LLMANS) $(man_SRC)

if BUILD_MAN
$(man_LLMANS): $(man_SRC)
	xsltproc $(PARAMS) $(STYLE) $<

man-clean:
	-rm -f $(man_MANS)
endif
endif
